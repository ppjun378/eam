# 资产管理系统

#### 介绍

##### 1、 任务概述

&nbsp;&nbsp;&nbsp;&nbsp;本系统主要为了更好地提高企业资产管理的效率，科学的管理企业的资产，极大地提高企业的办事效率，为企业节省人力。给资产管理员以及用户提供一个井然有序的管理平台，防止手工管理混乱，提高效率和服务质量。为企业的良好运行铺设前进道路。

> 这个系统需具备以下功能：

-   1.资产分类（办公用品、通讯设备、办公设备、财务专用设备）
-   2.资产入库、出库、调拨、盘点（多人盘点，数据汇总）
-   3.资产变更、领用、借用归还、维修信息登记、清理报废
-   4.分析报表（分类汇总、部门汇总、使用汇总、资产折旧、员工资产汇总等）
-   5.安全库存等
<<<<<<< HEAD
    > 后台：详见功能说明表
=======

> 后台：详见功能说明表
>>>>>>> bfdc3a04bad47002c3dd4dc335fca3cca9992ee0

#### 软件架构

#### 项目截图

<div align="center">

<img src="https://images.gitee.com/uploads/images/2019/0228/110011_8ce8c19b_916358.jpeg" height="330" width="190" >

<img src="https://images.gitee.com/uploads/images/2019/0228/110053_0acafbf8_916358.jpeg" height="330" width="190" >

<img src="https://images.gitee.com/uploads/images/2019/0228/110122_64aca843_916358.jpeg" height="330" width="190" >

<img src="https://images.gitee.com/uploads/images/2019/0228/110208_dc60f9f5_916358.jpeg" height="330" width="190">

 </div>

#### 后台截图

#### 环境要求

-   Nginx/Apache/IIS
-   PHP7.0+
-   MySQL5.1+

建议使用环境：Linux + Nginx1.14 + PHP7 + MySQL5.6

#### 安全&缺陷

如果你发现了一个安全漏洞，请发送邮件到 ppjun378@qq.com。所有的安全漏洞都将及时得到解决。
