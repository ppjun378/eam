<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReceivesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('receives', function (Blueprint $table) {
            $table->increments('id');
            $table->string('borrow_number')->comment('领用单号');
            $table->integer('borrow_user')->commit('领用人');
            $table->string('borrow_date')->commit('领用日期');
            $table->integer('use_company_id')->commit('领用后使用公司');
            $table->integer('use_department_id')->default('0')->commit('领用后使用部门');
            $table->string('address_type_id')->commit('领用后区域');
            $table->string('address')->commit('领用后存放地点');
            $table->string('expect_revert_date')->default('0')->commit('预计退库日期');
            $table->integer('borrow_operator')->default('1')->commit('制单人');
            $table->string('comment')->commit('备注');
            $table->string('select_assets')->default('')->commit('领用资产');
            $table->integer('status')->default(1)->commit('状态');
            $table->text('signature_images')->default('')->commit('签字图片');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('receives');
    }
}
