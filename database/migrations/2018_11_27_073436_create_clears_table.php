<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClearsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clears', function (Blueprint $table) {
            $table->increments('id');
            $table->string('clear_number')->commit('报废单号');
            $table->string('content')->commit('清理说明');
            $table->integer('borrow_operator')->commit('清理人');
            $table->string('clear_date')->commit('清理日期');
            $table->string('select_assets')->commit('报废资产');
            $table->integer('status')->default(0)->commit('报废状态');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clears');
    }
}
