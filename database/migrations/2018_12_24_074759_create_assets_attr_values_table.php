<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAssetsAttrValuesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('assets_attr_values', function (Blueprint $table) {
            $table->increments('id');
            $table->string('attr_value')->commit('属性值');
            $table->integer('attr_id')->commit('属性名ID');
            $table->integer('asset_id')->commit('资产ID');
            $table->timestamps();
            $table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('assets_attr_values');
    }
}
