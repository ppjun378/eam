<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBorrowsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('borrows', function (Blueprint $table) {
            $table->increments('id');
            $table->string('borrow_number')->commit('借用单号');
            $table->integer('borrow_user')->commit('借用人');
            $table->string('borrow_date')->commit('借用日期');
            $table->string('expect_revert_date')->default(0)->commit('预计归还日期');
            $table->string('actual_revert_date')->default(0)->commit('实际归还日期');
            $table->integer('borrow_operator')->default('1')->commit('制单人');
            $table->integer('is_borrow')->default('0')->commit('是否归还：1 借用 2:归还');
            $table->string('comment')->coment('备注');
            $table->string('select_assets')->default('0')->commit('领用资产');
            $table->integer('status')->default('1')->commit('状态');
            $table->text('signature_images')->commit('签字图片');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('borrows');
    }
}
