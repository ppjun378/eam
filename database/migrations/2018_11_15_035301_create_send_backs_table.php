<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSendBacksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('send_backs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('borrow_number')->default('')->commit('退库单号');
            $table->string('actual_revert_date')->commit('实际退库日期');
            $table->integer('use_company_id')->default(0)->commit('退库后使用公司');
            $table->integer('address_type_id')->default('')->commit('退库后区域');
            $table->string('address')->default('')->commit('退库后存放地点');
            $table->integer('borrow_operator')->commit('制单人');
            $table->string('comment')->default('')->commit('退库备注');
            $table->text('signature_images')->default('')->commit('签字图片');
            $table->integer('status')->default(1)->commit('状态');
            $table->string('select_assets')->commit('退库资产');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('send_backs');
    }
}
