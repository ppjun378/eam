<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCompanysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('companys', function (Blueprint $table) {
            $table->increments('id');
            $table->string('company_number')->default('')->comment('公司编号');
            $table->string('company_name')->default('')->comment('公司名称');
            $table->integer('parent_id')->default(0)->comment('父类id');
            $table->integer('sort')->default(1)->comment('排序');
            $table->tinyInteger('is_company')->default(1)->comment('是否为公司1.是0。否');
            $table->tinyInteger('is_disabled')->default(1)->comment('是否启用1.是0.否');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('companys');
    }
}
