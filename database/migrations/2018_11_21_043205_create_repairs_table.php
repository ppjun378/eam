<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRepairsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('repairs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('repair_number')->commit('维修单号');
            $table->string('content')->default('')->commit('维修内容');
            $table->string('money')->default(0)->commit('维修花费');
            $table->integer('repair_man')->default(1)->commit('保修人');
            $table->integer('status')->default(0)->commit('维修状态');
            $table->integer('borrow_operator')->commit('处理人');
            $table->string('bussiness_creation_date')->default('')->commit('业务创建日期');
            $table->string('business_update_date')->default('')->commit('报修处理中时间');
            $table->string('business_end_date')->default('')->commit('完成修改时间');
            $table->string('commit')->commit('备注信息');
            $table->string('select_assets')->commit('维修资产');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('repairs');
    }
}
