<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAssetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('assets', function (Blueprint $table) {
            $table->increments('id');
            $table->string('coding')->default('')->commit('资产编号');
            $table->integer('category_id')->commit('资产类别');
            $table->string('name')->default('')->commit('资产名称');
            $table->string('specification')->default('')->commit('规格型号');
            $table->string('sn')->default('')->commit('SN号');
            $table->string('unit')->default('')->commit('计量单位');
            $table->decimal('money', 8, 2)->commit('金额');
            $table->integer('use_the_company_id')->commit('使用公司');
            $table->integer('department_id')->commit('使用部门');
            $table->string('purchase_date')->commit('购入日期');
            $table->string('user_name')->commit('使用人');
            $table->string('administrator')->commit('管理员');
            $table->integer('company_id')->commit('所属公司');
            $table->string('area')->commit('区域');
            $table->string('location')->default('')->commit('存放地点');
            $table->string('use_period')->default('')->commit('使用期限');
            $table->string('supplier')->default('')->commit('供应商');
            $table->string('source')->default('')->commit('来源');
            $table->string('note')->default('')->commit('备注');
            $table->integer('sort')->default(0)->commit('排序');
            $table->string('image')->default('')->commit('照片');
            $tbale->integer('status')->default(2)->commit('资产状态');
            $table->timestamps();
            $table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('assets');
    }
}
