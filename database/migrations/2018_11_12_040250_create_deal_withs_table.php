<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDealWithsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('deal_withs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('asset_id')->comment('资产编号');
            $table->integer('deal_with_date')->comment('处理日期');
            $table->integer('deal_with_man_id')->comment('处理人编号');
            $table->string('deal_with_content')->default('')->comment('处理内容');
            $table->tinyInteger('deal_with_status')->default(1)->comment('处理时资金的状态');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('deal_withs');
    }
}
