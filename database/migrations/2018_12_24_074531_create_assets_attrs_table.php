<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAssetsAttrsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('assets_attrs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('attr_name')->commit('属性名称');
            $table->integer('status')->default(0)->commit('是否必选');
            $table->integer('attr_on')->default(3)->commit('1.所有资产包含该属性2.仅已选资产分类下的固定资产包含该属性3.仅当前资产包含该属性');
            $table->integer('attr_type_id')->default(0)->commit('属性类型ID：0即为全部');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('assets_attrs');
    }
}
