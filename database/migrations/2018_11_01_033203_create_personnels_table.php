<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePersonnelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('personnels', function (Blueprint $table) {
            $table->increments('id');
            $table->string('personnel_number')->comment('员工编号');
            $table->string('personnel_name')->comment('员工名称');
            $table->integer('company_id')->default('')->comment('公司编号');
            $table->integer('department_id')->default('')->comment('部门编号');
            $table->char('mobile_phone', 11)->default('')->comment('手机');
            $table->string('email')->default('')->comment('邮箱');
            $table->tinyInteger('status')->default(1)->comment('在职状态0.离职 1.在职');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('personnels');
    }
}
