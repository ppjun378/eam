<?php

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;

class AssetsTableSeeder extends Seeder
{
    /**
     * Run the database seeds
     *
     * @return void
     */
    public function run()
    {
        // App\Model\Assets::truncate();
        factory(App\Model\Assets::class, 10)->create();
    }
}
?>
