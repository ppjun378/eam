<?php

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;

class CategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds
     *
     * @return void
     */
    public function run()
    {
        App\Model\Category::truncate();
        DB::table('Category')->insert([
            [
                'name' => '土地、房屋及建筑物',
                'keywords' => '',
                'description' => '',
                'sort' => '1',
                'pid' => '0',
                'status' => '1',
            ], [
                'name' => '通用设备',
                'keywords' => '',
                'description' => '',
                'sort' => '1',
                'pid' => '0',
                'status' => '1',
            ], [
                'name' => '专用设备',
                'keywords' => '',
                'description' => '',
                'sort' => '1',
                'pid' => '0',
                'status' => '1',
            ], [
                'name' => '交通运输设备',
                'keywords' => '',
                'description' => '',
                'sort' => '1',
                'pid' => '0',
                'status' => '1',
            ], [
                'name' => '电气设备',
                'keywords' => '',
                'description' => '',
                'sort' => '1',
                'pid' => '0',
                'status' => '1',
            ], [
                'name' => '电子产品及通信设备',
                'keywords' => '',
                'description' => '',
                'sort' => '1',
                'pid' => '0',
                'status' => '1',
            ], [
                'name' => '仪器仪表、计量标准器具及量具、衡器',
                'keywords' => '',
                'description' => '',
                'sort' => '1',
                'pid' => '0',
                'status' => '1',
            ], [
                'name' => '文艺体育设备',
                'keywords' => '',
                'description' => '',
                'sort' => '1',
                'pid' => '0',
                'status' => '1',
            ], [
                'name' => '图书文物及陈列品',
                'keywords' => '',
                'description' => '',
                'sort' => '1',
                'pid' => '0',
                'status' => '1',
            ], [
                'name' => '家具用具及其他类',
                'keywords' => '',
                'description' => '',
                'sort' => '1',
                'pid' => '0',
                'status' => '1',
            ],
        ]);
    }
}
