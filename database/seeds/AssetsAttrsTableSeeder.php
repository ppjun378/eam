<?php
use Illuminate\Database\Seeder;

class AssetsAttrsTableSeeder extends Seeder
{
    /**
     * Run the database seeds
     *
     * @return void
     */
    public function run()
    {
        App\Model\AssetsAttrs::truncate();

        DB::table('assets_attrs')->insert([
            [
                'attr_name' => '吊灯(厅)',
                'status' => 0,
                'attr_on' => 2,
                'attr_type_id' => 10,
            ], [
                'attr_name' => '沙发(厅)',
                'status' => '0',
                'attr_on' => '2',
                'attr_type_id' => '10',
            ], [
                'attr_name' => '电视(厅)',
                'status' => '0',
                'attr_on' => '2',
                'attr_type_id' => '10',
            ], [
                'attr_name' => '电视柜(厅)',
                'status' => '0',
                'attr_on' => '2',
                'attr_type_id' => '10',
            ], [
                'attr_name' => '空调(厅)',
                'status' => '0',
                'attr_on' => '2',
                'attr_type_id' => '10',
            ], [
                'attr_name' => '落地灯(厅)',
                'status' => '0',
                'attr_on' => '2',
                'attr_type_id' => '10',
            ], [
                'attr_name' => '地毯(厅)',
                'status' => '0',
                'attr_on' => '2',
                'attr_type_id' => '10',
            ], [
                'attr_name' => '厅装饰画(厅)',
                'status' => '0',
                'attr_on' => '2',
                'attr_type_id' => '10',
            ], [
                'attr_name' => '茶几(厅)',
                'status' => '0',
                'attr_on' => '2',
                'attr_type_id' => '10',
            ], [
                'attr_name' => '客厅边几(厅)',
                'status' => '0',
                'attr_on' => '2',
                'attr_type_id' => '10',
            ], [
                'attr_name' => '餐灯(餐灯)',
                'status' => '0',
                'attr_on' => '2',
                'attr_type_id' => '10',
            ], [
                'attr_name' => '餐桌(餐灯)',
                'status' => '0',
                'attr_on' => '2',
                'attr_type_id' => '10',
            ], [
                'attr_name' => '餐椅(餐灯)',
                'status' => '0',
                'attr_on' => '2',
                'attr_type_id' => '10',
            ], [
                'attr_name' => '冰箱(厨房)',
                'status' => '0',
                'attr_on' => '2',
                'attr_type_id' => '10',
            ], [
                'attr_name' => '橱柜(厨房)',
                'status' => '0',
                'attr_on' => '2',
                'attr_type_id' => '10',
            ], [
                'attr_name' => '燃气炉(厨房)',
                'status' => '0',
                'attr_on' => '2',
                'attr_type_id' => '10',
            ], [
                'attr_name' => '抽油烟机(厨房)',
                'status' => '0',
                'attr_on' => '2',
                'attr_type_id' => '10',
            ], [
                'attr_name' => '消毒碗柜(厨房)',
                'status' => '0',
                'attr_on' => '2',
                'attr_type_id' => '10',
            ], [
                'attr_name' => '床/床垫(房间)',
                'status' => '0',
                'attr_on' => '2',
                'attr_type_id' => '10',
            ], [
                'attr_name' => '衣柜(房间)',
                'status' => '0',
                'attr_on' => '2',
                'attr_type_id' => '10',
            ], [
                'attr_name' => '房间画(房间)',
                'status' => '0',
                'attr_on' => '2',
                'attr_type_id' => '10',
            ], [
                'attr_name' => '房间灯(房间)',
                'status' => '0',
                'attr_on' => '2',
                'attr_type_id' => '10',
            ], [
                'attr_name' => '空调(房间)',
                'status' => '0',
                'attr_on' => '2',
                'attr_type_id' => '10',
            ], [
                'attr_name' => '橱柜(洗手间)',
                'status' => '0',
                'attr_on' => '2',
                'attr_type_id' => '10',
            ], [
                'attr_name' => '热水器(洗手间)',
                'status' => '0',
                'attr_on' => '2',
                'attr_type_id' => '10',
            ], [
                'attr_name' => '马桶(洗手间)',
                'status' => '0',
                'attr_on' => '2',
                'attr_type_id' => '10',
            ], [
                'attr_name' => '洗衣机(其他)',
                'status' => '0',
                'attr_on' => '2',
                'attr_type_id' => '10',
            ], [
                'attr_name' => '窗帘(其他)',
                'status' => '0',
                'attr_on' => '2',
                'attr_type_id' => '10',
            ], [
                'attr_name' => '电子锁(其他)',
                'status' => '0',
                'attr_on' => '2',
                'attr_type_id' => '10',
            ], [
                'attr_name' => '锁匙(物品交接)',
                'status' => '0',
                'attr_on' => '2',
                'attr_type_id' => '10',
            ], [
                'attr_name' => '门禁卡(物品交接)',
                'status' => '0',
                'attr_on' => '2',
                'attr_type_id' => '10',
            ],
        ]);

    }

}
