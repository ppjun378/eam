<?php

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;

class CompanysTableSeeder extends Seeder
{
    /**
     * Run the database seeds
     *
     * @return void
     */
    public function run()
    {
        // App\Model\Assets::truncate();
        factory(App\Model\Compays::class, 10)->create();
    }
}
