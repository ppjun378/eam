<?php

use Faker\Generator as Faker;

$factory->define(App\Model\Assets::class, function (Faker $faker) {
    return [
        'coding' => 'SK1232', //资产编号
        'category_id' => '1', //资产类别
        'name' => '笔记本电脑', //资产名称
        'specification' => ' ', //规格
        'sn' => ' ', //SN号
        'unit' => '台', //计量单位
        'money' => 10, //金额
        'use_the_company_id' => '1', //使用公司
        'department_id' => '1', //使用部门
        'purchase_date' => time(), //购入日期
        'user_name' => '1', //使用人
        'administrator' => '管理员', //管理员
        'company_id' => '1', //所属公司
        'area' => '本地', //区域
        'location' => '本地-1', //存放地点
        'use_period' => '1', //使用期限
        'supplier' => '微软公司', //供应商
        'source' => '购买', //来源
        'note' => '', //备注
        'image' => 'https: //images.gitee.com/uploads/58/916358_ppjun378.png?1535425020', //照片
        'sort' => '1', //排序
    ];
});
