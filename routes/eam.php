<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
 */

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

use Illuminate\Support\Facades\Route;
// 列表界面
// Route::get('api/assets/show', 'AssetsController@show');

/**
 * 数据接口
 */
Route::prefix('api')->middleware(['api'])->group(function () {
    // // 中间件验证是否已经登陆
    // Route::middleware(['admin.login'])->group(function () {
    //     // 登陆界面
    //     Route::get('login', 'LoginController@index');
    //     // 登陆验证
    //     Route::post('login', 'LoginController@login');
    //     // 退出登陆
    //     Route::get('logout', 'LoginController@logout');
    //     // 注册账号
    //     Route::post('regis', 'LoginController@regis');
    // });

    /**
     * 资产管理
     */
    Route::prefix('assets')->group(function () {
        // 列表界面
        Route::get('show', 'AssetsController@show');
        // 列表数据
        Route::get('getinfo', 'AssetsController@getinfo');
        // 添加页面
        Route::get('add', 'AssetsController@add');
        // 编辑页面
        Route::get('get_detail', 'AssetsController@edit');
        // 添加/保存
        Route::post('store', 'AssetsController@store');
        // 更新
        Route::post('save', 'AssetsController@save');
        // 删除
        Route::post('destroy', 'AssetsController@destory');
        // 恢复删除
        Route::post('restore', 'AssetsController@restore');
        // 处理记录
        Route::get('getinfo_deal_with', 'DealWithsController@getinfo');
        // 上传图片
        Route::post('upload_image', 'AssetsController@uploadImage');

        // 导入excel表
        Route::get('excel/import', 'AssetsController@import_excel');

        Route::get('getlist', 'CategoryController@getlist');

    });

    /**
     * 公司管理
     */
    Route::prefix('company')->group(function () {
        // 列表界面
        Route::get('show', 'CompanysController@show');
        // 列表数据
        Route::get('getinfo', 'CompanysController@getinfo');
        // 添加页面
        Route::get('add', 'CompanysController@add');
        // 编辑页面
        Route::get('get_detail', 'CompanysController@edit');
        // 添加/保存
        Route::post('store', 'CompanysController@store');
        // 更新
        Route::post('save', 'CompanysController@save');
        // 删除
        Route::post('destroy', 'CompanysController@destory');
        // 恢复删除
        Route::post('restore', 'CompanysController@restore');
    });

    /**
     * 部门
     */

    /**
     * 员工
     */
    Route::prefix('personnel')->group(function () {
        // 列表界面
        Route::get('show', 'PersonnelsController@show');
        // 列表数据
        Route::get('getinfo', 'PersonnelsController@getinfo');
        // 添加页面
        Route::get('add', 'PersonnelsController@add');
        // 编辑页面
        Route::get('get_detail', 'PersonnelsController@edit');
        // 添加/保存
        Route::post('store', 'PersonnelsController@store');
        // 更新
        Route::post('save', 'PersonnelsController@save');
        // 删除
        Route::post('destroy', 'PersonnelsController@destory');
        // 恢复删除
        Route::post('restore', 'PersonnelsController@restore');
    });

    /**
     * 分类
     */
    Route::prefix('category')->group(function () {
        // 列表界面
        Route::get('show', 'CategoryController@show');
        // 列表数据
        Route::get('getinfo', 'CategoryController@getinfo');
        // 添加页面
        Route::get('add', 'CategoryController@add');
        // 编辑页面
        Route::get('get_detail', 'CategoryController@edit');
        // 添加/保存
        Route::post('store', 'CategoryController@store');
        // 更新
        Route::post('save', 'CategoryController@save');
        // 删除
        Route::post('destroy', 'CategoryController@destory');
        // 恢复删除
        Route::post('restore', 'CategoryController@restore');
    });

    /**
     * 资产领用
     */
    Route::prefix('receive')->group(function () {
        // 列表界面
        Route::get('show', 'ReceivesController@show');
        // 列表数据
        Route::get('getinfo', 'ReceivesController@getinfo');
        // 添加页面
        Route::get('add', 'ReceivesController@add');
        // 编辑页面
        Route::get('get_detail', 'ReceivesController@edit');
        // 添加/保存
        Route::post('store', 'ReceivesController@store');
        // 更新
        Route::post('save', 'ReceivesController@save');
        // 删除
        Route::post('destroy', 'ReceivesController@destory');
        // 恢复删除
        Route::post('restore', 'ReceivesController@restore');

    });

    /**
     * 资产退库
     */
    Route::prefix('sendback')->group(function () {
        // 列表界面
        Route::get('show', 'SendBacksController@show');
        // 列表数据
        Route::get('getinfo', 'SendBacksController@getinfo');
        // 添加页面
        Route::get('add', 'SendBacksController@add');
        // 编辑页面
        Route::get('get_detail', 'SendBacksController@edit');
        // 添加/保存
        Route::post('store', 'SendBacksController@store');
        // 更新
        Route::post('save', 'SendBacksController@save');
        // 删除
        Route::post('destroy', 'SendBacksController@destory');
        // 恢复删除
        Route::post('restore', 'SendBacksController@restore');

    });

    /**
     * 资产借用
     */
    Route::prefix('borrow')->group(function () {
        // 列表界面
        Route::get('show', 'BorrowsController@show');
        // 列表数据
        Route::get('getinfo', 'BorrowsController@getinfo');
        // 添加页面
        Route::get('add', 'BorrowsController@add');
        // 编辑页面
        Route::get('get_detail', 'BorrowsController@edit');
        // 添加/保存
        Route::post('store', 'BorrowsController@store');
        // 更新
        Route::post('save', 'BorrowsController@save');
        // 删除
        Route::post('destroy', 'BorrowsController@destory');
        // 恢复删除
        Route::post('restore', 'BorrowsController@restore');

    });

    /**
     * 区域
     */
    Route::prefix('address')->group(function () {
        // 列表界面
        Route::get('show', 'AddressController@show');
        // 列表数据
        Route::get('getinfo', 'AddressController@getinfo');
        // 添加页面
        Route::get('add', 'AddressController@add');
        // 编辑页面
        Route::get('get_detail', 'AddressController@edit');
        // 添加/保存
        Route::post('store', 'AddressController@store');
        // 更新
        Route::post('save', 'AddressController@save');
        // 删除
        Route::post('destroy', 'AddressController@destory');
        // 恢复删除
        Route::post('restore', 'AddressController@restore');

    });

    /**
     * 资产维修
     */
    Route::prefix('repair')->group(function () {
        // 列表界面
        Route::get('show', 'RepairsController@show');
        // 列表数据
        Route::get('getinfo', 'RepairsController@getinfo');
        // 添加页面
        Route::get('add', 'RepairsController@add');
        // 编辑页面
        Route::get('get_detail', 'RepairsController@edit');
        // 添加/保存
        Route::post('store', 'RepairsController@store');
        // 更新
        Route::post('save', 'RepairsController@save');
        // 删除
        Route::post('destroy', 'RepairsController@destory');
        // 恢复删除
        Route::post('restore', 'RepairsController@restore');

    });

    /**
     * 资产报废
     */
    Route::prefix('clear')->group(function () {
        // 列表界面
        Route::get('show', 'ClearsController@show');
        // 列表数据
        Route::get('getinfo', 'ClearsController@getinfo');
        // 添加页面
        Route::get('add', 'ClearsController@add');
        // 编辑页面
        Route::get('get_detail', 'ClearsController@edit');
        // 添加/保存
        Route::post('store', 'ClearsController@store');
        // 更新
        Route::post('save', 'ClearsController@save');
        // 删除
        Route::post('destroy', 'ClearsController@destory');
        // 恢复删除
        Route::post('restore', 'ClearsController@restore');

    });

    /**
     * 管理员
     */

    /**
     * 公共函数
     */
    Route::prefix('common')->group(function () {
        // 上载封面
        Route::post('upload/cover', 'CommonController@uploadCover');
    });

    Route::prefix('user_assets')->group(function () {
        // 列表数据
        Route::get('getinfo', 'Home\AssetsController@getinfo');
        // 添加页面
        Route::get('add', 'Home\AssetsController@add');
        // 添加/保存
        Route::post('select_use_assets', 'Home\AssetsController@select_use_assets');
    });

    /**
     * 资产领用
     */
    // Route::prefix('user_receive')->group(function () {
    //     // 列表数据
    //     Route::get('getinfo', 'Home\ReceivesController@getinfo');
    //     // 添加/保存
    //     Route::post('store', 'Home\ReceivesController@store');
    // });

    /**
     * 资产退库
     */
    Route::prefix('user_sendback')->group(function () {
        // 列表数据
        Route::get('getinfo', 'Home\SendBacksController@getinfo');
        // 添加/保存
        Route::post('store', 'Home\SendBacksController@store');
    });

    /**
     * 资产借用
     */
    Route::prefix('user_borrow')->group(function () {
        // 列表数据
        Route::get('getinfo', 'Home\BorrowsController@getinfo');
        // 添加/保存
        Route::post('store', 'Home\BorrowsController@store');
    });

});
