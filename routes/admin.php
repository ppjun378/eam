<?php
/**
 * User: Administrator
 * Date: 2018/12/07
 * Time: 12:00
 */

use Illuminate\Support\Facades\Route;
/**
 * 后台
 */
Route::prefix('admina')->middleware(['blog'])->group(function () {
    //登陆界面
    Route::get('login', 'LoginController@index');
    // 登陆弹窗
    // Route::get('popup', 'LoginController@popup');
    // 登陆验证
    // Route::post('login', 'LoginController@login');
    // 退出登录
    // Route::get('logout', 'LoginController@logout');
    // 注册账号
    // Route::post('regis', 'LoginController@regis');

    // 中间件判断是否登录
    // Route::middleware(['admin.login', 'auth:blog'])->group(function () {
    // 后台首页
    // Route::get('index', 'IndexController@adminIndex');
    // 后台欢迎页
    // Route::get('welcome', 'IndexController@welcome');
    // exit;
    /**
     * 文章管理
     */
    /*
    Route::prefix('assets')->group(function () {
    // 列表界面
    Route::get('show', 'AssetsController@show');
    // 列表数据
    Route::get('getinfo', 'AssetsController@getinfo');
    // 添加页面
    Route::get('add', 'AssetsController@add');
    // 编辑页面
    Route::get('edit/{id}', 'AssetsController@edit');
    // 添加/保存
    Route::post('store', 'AssetsController@store');
    // 更新
    Route::post('save', 'AssetsController@save');
    // 软删除
    Route::post('destroy', 'AssetsController@destroy');
    // 恢复删除
    Route::post('restore', 'AssetsController@restore');
    // 删除（谨慎操作!）
    Route::post('delete', 'Assetscontroller@delete');
    // 文章分类管理
    Route::prefix('category')->group(function ($api) {
    // 获取分类列表
    $api->get('getlist', 'CategoryController@getlist');
    // 分类管理页面
    $api->get('show', 'CategoryController@listPage');
    // 分类列表数据
    $api->get('getinfo', 'CategoryController@getInfo');
    // 分类添加页面
    $api->get('add', 'CategoryController@addPage');
    // 分类新增
    $api->post('store', 'CategoryController@storeInfo');
    // 分类(软)删除
    $api->post('destroy', 'CategoryController@destroyInfo');
    // 分类状态修改
    $api->post('alteration', 'CategoryController@alteration');
    });
    // 文章标签管理
    Route::prefix('tag')->group(function ($api) {
    // 获取标签列表
    $api->get('list', 'TagController@getList');
    // 标签管理页面
    $api->get('show', 'TagController@listPage');
    // 标签列表数据
    $api->get('all', 'TagController@getAll');
    // 标签添加页面
    $api->get('add', 'TagController@addPage');
    // 标签新增
    $api->post('store', 'TagController@store');
    // 标签删除
    $api->post('destroy', 'TagController@destroy');
    // 标签状态修改
    $api->post('alteration', 'TagController@alteration');
    });
    });
     */

    // });
    // 后台首页

});
