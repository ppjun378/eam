@extends('common.baseAdmin')
@section('title',$title)
@section('css')
    <style>
        body {
            padding:20px;
        }
        .layui-btn-small {
            padding: 0 15px;
        }

        tr td:not(:nth-child(0)),
        tr th:not(:nth-child(0)) {
            text-align: center;
        }
    </style>
@stop
@section('content')
    <table class="layui-table" lay-even="">
        <colgroup>
            <col width="200">
            <col width="150">
            <col>
            <col width="180">
        </colgroup>
        <thead>
        <tr>
            <th>回复时间</th>
            <th>回复人</th>
            <th>内容</th>
            <th>操作</th>
        </tr>
        </thead>
        <tbody>
        @foreach($data as $item)
        <tr>
            <td>{{ $item->created_at }}</td>
            <td>{{ $item->OauthUser->nickname }}</td>
            <td>{{ $item->content }}</td>
            <td>
                @if($item->deleted_at)
                    <button title="还原删除" class="layui-btn layui-btn-sm" onclick="layui.guestbookList.restoreData(this,{{ $item->id }})">
                        <i class="layui-icon">&#xe669;</i>
                    </button>
                    <button title="永久删除" class="layui-btn layui-btn-sm layui-btn-danger" onclick="layui.guestbookList.deleteData(this,{{ $item->id }})">
                        <i class="layui-icon">&#x1006;</i>
                    </button>
                @else
                    <button title="删除" class="layui-btn layui-btn-sm layui-btn-warm" onclick="layui.guestbookList.softDelete(this,{{ $item->id }})">
                        <i class="layui-icon">&#xe640;</i>
                    </button>
                @endif
            </td>
        </tr>
        @endforeach
        </tbody>
    </table>
@stop
@section('js')
    <!-- layui规范化用法 -->
    <script type="text/javascript">
        layui.config({
            base: '/static/layerjs/'
        }).use('guestbookList');
    </script>
@stop