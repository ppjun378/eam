@extends('common.baseAdmin')
@section('title',$title)
@section('css')
@stop
@section('content')
    <fieldset id="dataList" class="layui-elem-field layui-field-title sys-list-field" >
        <legend style="text-align: center;">留言列表</legend>
        <div class="layui-field-box">
            <div id="dataContent" class="">
                <!--内容区域 ajax获取-->
            </div>
        </div>
    </fieldset>
@stop
@section('js')
    <!-- layui规范化用法 -->
    <script type="text/javascript">
        layui.config({
            base: '/static/layerjs/'
        }).use('guestbookList');
    </script>
@stop