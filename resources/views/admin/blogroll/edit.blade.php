@extends('common.baseAdmin')
@section('title',$title)
@section('css')
    <!--cropper-->
    <link href="{{ asset('static/plugins/cropper/cropper.min.css') }}" rel="stylesheet">
    <link href="{{ asset('static/plugins/cropper/ImgCropping.css') }}" rel="stylesheet">
@stop
@section('content')
    <body>
    <div style="height: 50px; display: block;"></div>
    <!-- 内容 -->
    <div class="layui-form form-main">
        <div class="layui-form-item layui-form-text">
            <label class="layui-form-label">名称</label>
            <div class="layui-input-inline" style="width:300px">
                <input type="text" name="title" placeholder="请输入名称" autocomplete="off" class="layui-input"
                       value="{{ $data->title }}"></div>
        </div>
        <div class="layui-form-item layui-form-text">
            <label class="layui-form-label">地址</label>
            <div class="layui-input-inline" style="width:300px">
                <input type="text" name="domain" placeholder="请输入地址" autocomplete="off" class="layui-input"
                       value="{{ $data->domain }}" lay-verify="linkurl">
            </div>
        </div>
        <div class="layui-form-item" style="position: relative;">
            <label class="layui-form-label">LOGO</label>
            <div class="layui-input-block">
                <div class="layui-upload-list" style="padding:10px 0px">
                    <img width="150px" height="150px" @if($data->logo) src="/{{ $data->logo }}" @else src="/static/images/dwl.jpg" @endif id="srcimgurl" class="layui-upload-img">
                </div>
            </div>
            <div class="layui-input-inline layui-btn-container" style="width: auto;">
                <label class="layui-form-label"></label>
                <a class="layui-btn" href="javascript:;" id="replaceImg" onclick="layui.blogrollAdd.tailorContainer()"><i
                            class="layui-icon">&#xe67c;</i>修改图片</a>
                <!--隐藏域 用来存储上传后的图片地址-->
                <input type="hidden" name="avatar" id="logoPic" value="">
            </div>
            <div class="layui-form-mid layui-word-aux">LOGO的尺寸限定150x150px,大小在50kb左右</div>
        </div>
        <div class="layui-form-item">
            <div class="layui-input-block">
                <button class="layui-btn" onclick="layui.blogrollAdd.saveBlogRoll({{ $data->id }})">立即保存</button>
                <a id="articleBack" href="#" onclick="layui.blogrollAdd.closeIframe()" type="button" class="layui-btn layui-btn-primary">返回列表</a>
            </div>
        </div>
    </div>

    <!--图片裁剪器的工作页面-->
    <div style="display: none" class="tailoring-container">
        <div class="black-cloth" onClick="layui.blogrollAdd.closeTailor(this)"></div>
        <div class="tailoring-content">
            <div class="tailoring-content-one">
                <label title="上传图片" for="chooseImg" class="l-btn choose-btn">
                    <input type="file" accept="image/jpg,image/jpeg,image/png" name="file" id="chooseImg" class="hidden"
                           onChange="layui.blogrollAdd.selectImg(this)">选择图片</label>
                <div class="close-tailoring" onclick="layui.blogrollAdd.closeTailor(this)">×</div>
            </div>
            <div class="tailoring-content-two">
                <div class="tailoring-box-parcel">
                    <img id="tailoringImg"></div>
                <div class="preview-box-parcel">
                    <p>图片预览：</p>
                    <div class="square previewImg"></div>
                    <div class="circular previewImg"></div>
                </div>
            </div>
            <div class="tailoring-content-three">
                <span class="l-btn" style="cursor: pointer;" onclick="layui.blogrollAdd.resetImg()">复位</span>
                <span class="l-btn" style="cursor: pointer;" onclick="layui.blogrollAdd.rotateImg()">旋转</span>
                <span class="l-btn" style="cursor: pointer;" onclick="layui.blogrollAdd.cropperScaleX()">换向</span>
                <span class="l-btn sureCut" id="sureCut" style=" cursor: pointer;"
                      onclick="layui.blogrollAdd.sureCut()">确定</span>
            </div>
        </div>
    </div>
    </body>
@stop
@section('js')
    <script src="{{ asset('static/plugins/cropper/cropper.min.js') }}"></script>
    <!-- layui规范化用法 -->
    <script type="text/javascript">
        layui.config({
            base: '/static/layerjs/'
        }).use('blogrollAdd');
    </script>
@stop