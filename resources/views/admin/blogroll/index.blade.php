@extends('common.baseAdmin')
@section('title',$title)
@section('css')
    <link rel="stylesheet" href="{{ asset('static/layercss/mystyle.css') }}">
    <style>
        legend {
            text-align: center;
        }
    </style>
@stop
@section('content')
    <body>
    <fieldset id="friendlinkConsole" class="layui-elem-field layui-field-title" style="display:block;text-align:center;">
        <legend>控制台</legend>
        <div class="layui-field-box">
            <div id="friendlinkIndexTop">
                <div class="layui-form layui-form-pane" style="text-align: center;">
                    <div class="layui-form-item" style="margin:0;margin-top:15px;">
                        <div class="layui-inline">
                            <div class="layui-inline">
                                <a href="#" onclick="layui.blogrollList.addDataPage()" class="layui-btn layui-btn-normal">新增友链</a>
                            </div>
                            <div class="layui-inline" style="width:auto">
                                <button class="layui-btn layui-btn layui-icon" onclick="location.reload(this);">&#xe669;</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </fieldset>
    <fieldset id="friendlinkList" class="layui-elem-field layui-field-title" style="display:block">
        <legend id="friendlinkBoxTitle">友链列表</legend>
        <div class="layui-field-box" style="position: fixed;">
            <div id="friendlinkIndex">
            </div>
        </div>
    </fieldset>
    </body>
@stop
@section('js')
    <!-- layui规范化用法 -->
    <script type="text/javascript">
        layui.config({
            base: '/static/layerjs/'
        }).use('blogrollList');
    </script>
@stop