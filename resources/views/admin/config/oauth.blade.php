@extends('common.baseAdmin')
@section('title',$title)
@section('css')
    <style>
        table {
            border: 1px solid #C2CCD1;
            border-color: #C2CCD1;
        }

        th {
            text-align: left;
            color: #74879C;
            padding: 10px;
        }

        td {
            padding: 10px;
        }
        .title {
            color: #74879C;
            margin: 20px  20px 0px;
        }
    </style>
@stop
@section('content')
    <body>
    <div class="title">
        <h3>第三方登录置项</h3>
    </div>
    @inject('ConfigsPresenter','App\Presenters\ConfigsPresenter')
    <form style="padding: 0 20px; position: relative;" class="layui-form">
        <table border="1" width="100%">
            <tr>
                <div class="layui-form-item">
                    <th>
                        <label class="">QQ登录APP ID：</label>
                    </th>
                    <td>
                        <div class="layui-input-inline">
                            <input type="text" name="qq_client_id"
                                   value="{{ $ConfigsPresenter->getValueWithName('qq_client_id') }}"
                                   class="layui-input">
                        </div>
                    </td>
                </div>
            </tr>
            <tr>
                <div class="layui-form-item">
                    <th>
                        <label class="">QQ登录APP KEY：</label>
                    </th>
                    <td>
                        <div class="layui-input-inline" style="min-width: 300px;">
                            <input type="text" name="qq_client_key"
                                   value="{{ $ConfigsPresenter->getValueWithName('qq_client_key') }}"
                                   class="layui-input">
                        </div>
                    </td>
                </div>
            </tr>
            <tr>
                <div class="layui-form-item">
                    <th>
                        <label class="">微信登录APP ID：</label>
                    </th>
                    <td>
                        <div class="layui-input-inline">
                            <input type="text" name="weixin_client_id"
                                   value="{{ $ConfigsPresenter->getValueWithName('weixin_client_id') }}"
                                   class="layui-input">
                        </div>
                    </td>
                </div>
            </tr>
            <tr>
                <div class="layui-form-item">
                    <th>
                        <label class="">微信登录APP KEY：</label>
                    </th>
                    <td>
                        <div class="layui-input-inline" style="min-width: 300px;">
                            <input type="text" name="weixin_client_key"
                                   value="{{ $ConfigsPresenter->getValueWithName('weixin_client_key') }}"
                                   class="layui-input">
                        </div>
                    </td>
                </div>
            </tr>
            <tr>
                <div class="layui-form-item">
                    <th>
                        <label class="">微博登录APP ID：</label>
                    </th>
                    <td>
                        <div class="layui-input-inline">
                            <input type="text" name="weibo_client_id"
                                   value="{{ $ConfigsPresenter->getValueWithName('weibo_client_id') }}"
                                   class="layui-input">
                        </div>
                    </td>
                </div>
            </tr>
            <tr>
                <div class="layui-form-item">
                    <th>
                        <label class="">微博登录APP KEY：</label>
                    </th>
                    <td>
                        <div class="layui-input-inline" style="min-width: 300px;">
                            <input type="text" name="weibo_client_key"
                                   value="{{ $ConfigsPresenter->getValueWithName('weibo_client_key') }}"
                                   class="layui-input">
                        </div>
                    </td>
                </div>
            </tr>
            <tr>
                <div class="layui-form-item">
                    <th>
                        <label class="layui-form-label"></label>
                    </th>
                    <td>
                        <div class="layui-input-inline">
                            <button class="layui-btn" lay-submit lay-filter="saveConfigs">保存</button>
                        </div>
                    </td>
                </div>
            </tr>
        </table>
    </form>
    </body>
@stop
@section('js')
    <!-- layui规范化用法 -->
    <script type="text/javascript">
        layui.config({
            base: '/static/layerjs/'
        }).use('configsEdit');
    </script>
@stop