@extends('common.baseAdmin')
@section('title',$title)
@section('css')
    <style>
        table {
            border: 1px solid #C2CCD1;
            border-color: #C2CCD1;
        }

        th {
            text-align: left;
            color: #74879C;
            padding: 10px;
        }

        td {
            padding: 10px;
        }
        .title {
            color: #74879C;
            margin: 20px  20px 0px;
        }
    </style>
@stop
@section('content')
    <body>
    <div class="title">
        <h3>邮箱配置项</h3>
    </div>
    @inject('ConfigsPresenter','App\Presenters\ConfigsPresenter')
    <form style="padding: 0 20px; position: relative;" class="layui-form">
        <table border="1" width="100%">
            <tr>
                <div class="layui-form-item">
                    <th>
                        <label class="">邮箱服务器类型：</label>
                    </th>
                    <td>
                        <div class="layui-input-inline">
                            <input type="text" name="mail_driver"
                                   value="{{ $ConfigsPresenter->getValueWithName('mail_driver') }}"
                                   class="layui-input">
                        </div>
                    </td>
                </div>
            </tr>
            <tr>
                <div class="layui-form-item">
                    <th>
                        <label class="">加密方式：</label>
                    </th>
                    <td>
                        <div class="layui-input-inline">
                            <input type="text" name="mail_encryption"
                                   value="{{ $ConfigsPresenter->getValueWithName('mail_encryption') }}"
                                   class="layui-input">
                        </div>
                    </td>
                </div>
            </tr>
            <tr>
                <div class="layui-form-item">
                    <th>
                        <label class="">邮箱服务器端口：</label>
                    </th>
                    <td>
                        <div class="layui-input-inline">
                            <input type="text" name="mail_port" value="{{ $ConfigsPresenter->getValueWithName('mail_port') }}"
                                   class="layui-input">
                        </div>
                    </td>
                </div>
            </tr>
            <tr>
                <div class="layui-form-item">
                    <th>
                        <label class="">邮箱服务器地址：</label>
                    </th>
                    <td>
                        <div class="layui-input-inline">
                            <input type="text" name="mail_host"
                                   value="{{ $ConfigsPresenter->getValueWithName('mail_host') }}"
                                   class="layui-input">
                        </div>
                    </td>
                </div>
            </tr>
            <tr>
                <div class="layui-form-item">
                    <th>
                        <label class="">邮箱账号：</label>
                    </th>
                    <td>
                        <div class="layui-input-inline">
                            <input type="text" name="mail_username"
                                   value="{{ $ConfigsPresenter->getValueWithName('mail_username') }}"
                                   class="layui-input">
                        </div>
                    </td>
                </div>
            </tr>
            <tr>
                <div class="layui-form-item">
                    <th>
                        <label class="">邮箱密码：</label>
                    </th>
                    <td>
                        <div class="layui-input-inline">
                            <input type="text" name="mail_password"
                                   value="{{ $ConfigsPresenter->getValueWithName('mail_password') }}"
                                   class="layui-input">
                        </div>
                    </td>
                </div>
            </tr>
            <tr>
                <div class="layui-form-item">
                    <th>
                        <label class="">发件人名称：</label>
                    </th>
                    <td>
                        <div class="layui-input-inline">
                            <input type="text" name="mail_from_name"
                                   value="{{ $ConfigsPresenter->getValueWithName('mail_from_name') }}"
                                   class="layui-input">
                        </div>
                    </td>
                </div>
            </tr>
            <tr>
                <div class="layui-form-item">
                    <th>
                        <label class="">发件人邮箱：</label>
                    </th>
                    <td>
                        <div class="layui-input-inline">
                            <input type="text" name="mail_from_address"
                                   value="{{ $ConfigsPresenter->getValueWithName('mail_from_address') }}"
                                   class="layui-input">
                        </div>
                    </td>
                </div>
            </tr>
            <tr>
                <div class="layui-form-item">
                    <th>
                        <label class="layui-form-label"></label>
                    </th>
                    <td>
                        <div class="layui-input-inline">
                            <button class="layui-btn" lay-submit lay-filter="saveConfigs">保存</button>
                        </div>
                    </td>
                </div>
            </tr>
        </table>
    </form>
    </body>
@stop
@section('js')
    <!-- layui规范化用法 -->
    <script type="text/javascript">
        layui.config({
            base: '/static/layerjs/'
        }).use('configsEdit');
    </script>
@stop