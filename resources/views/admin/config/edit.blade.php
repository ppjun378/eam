@extends('common.baseAdmin')
@section('title',$title)
@section('css')
    <style>
        table {
            border: 1px solid #C2CCD1;
            border-color: #C2CCD1;
        }

        th {
            text-align: left;
            color: #74879C;
            padding: 10px;
        }

        td {
            padding: 10px;
        }
    </style>
@stop
@section('content')
    <body>
    @inject('ConfigsPresenter','App\Presenters\ConfigsPresenter')
    <form style="padding: 20px; position: relative;" class="layui-form">
        <table border="1" width="100%">
            <tr>
                <div class="layui-form-item">
                    <th>
                        <label class="">备案号：</label>
                    </th>
                    <td>
                        <div class="layui-input-inline">
                            <input type="text" name="web_icp_number"
                                   value="{{ $ConfigsPresenter->getValueWithName('web_icp_number') }}"
                                   class="layui-input">
                        </div>
                    </td>
                </div>
            </tr>
            <tr>
                <div class="layui-form-item">
                    <th>
                        <label class="">网站标题：</label>
                    </th>
                    <td>
                        <div class="layui-input-inline">
                            <input type="text" name="web_name"
                                   value="{{ $ConfigsPresenter->getValueWithName('web_name') }}"
                                   class="layui-input">
                        </div>
                    </td>
                </div>
            </tr>
            <tr>
                <div class="layui-form-item">
                    <th>
                        <label class="">网站关键字：</label>
                    </th>
                    <td>
                        <div class="layui-input-inline">
                <textarea name="web_keywords" placeholder=""
                          class="layui-textarea">{{ $ConfigsPresenter->getValueWithName('web_keywords') }}</textarea>
                        </div>
                    </td>
                </div>
            </tr>
            <tr>
                <div class="layui-form-item">
                    <th>
                        <label class="">网站描述：</label>
                    </th>
                    <td>
                        <div class="layui-input-inline">
                <textarea name="web_description" placeholder=""
                          class="layui-textarea">{{ $ConfigsPresenter->getValueWithName('web_description') }}</textarea>
                        </div>
                    </td>
                </div>
            </tr>
            <tr>
                <div class="layui-form-item">
                    <th>
                        <label class="">默认作者：</label>
                    </th>
                    <td>
                        <div class="layui-input-inline">
                            <input type="text" name="author" value="{{ $ConfigsPresenter->getValueWithName('author') }}"
                                   class="layui-input">
                        </div>
                    </td>
                </div>
            </tr>
            <tr>
                <div class="layui-form-item">
                    <th>
                        <label class="">百度推送site提交链接：</label>
                    </th>
                    <td>
                        <div class="layui-input-inline">
                            <input type="text" name="baidu_site_url"
                                   value="{{ $ConfigsPresenter->getValueWithName('baidu_site_url') }}"
                                   class="layui-input">
                        </div>
                    </td>
                </div>
            </tr>
            <tr>
                <div class="layui-form-item">
                    <th>
                        <label class="">第三方统计代码：</label>
                    </th>
                    <td>
                        <div class="layui-input-inline">
                <textarea name="web_statistics" placeholder=""
                          class="layui-textarea">{{ $ConfigsPresenter->getValueWithName('web_statistics') }}</textarea>
                        </div>
                    </td>
                </div>
            </tr>
            <tr>
                <div class="layui-form-item">
                    <th>
                        <label class="">站长邮箱：</label>
                    </th>
                    <td>
                        <div class="layui-input-inline">
                            <input type="text" name="admin_email"
                                   value="{{ $ConfigsPresenter->getValueWithName('admin_email') }}"
                                   class="layui-input">
                        </div>
                    </td>
                </div>
            </tr>
            <tr>
                <div class="layui-form-item">
                    <th>
                        <label class="layui-form-label"></label>
                    </th>
                    <td>
                        <div class="layui-input-inline">
                            <button class="layui-btn" lay-submit lay-filter="saveConfigs">保存</button>
                        </div>
                    </td>
                </div>
            </tr>
        </table>
    </form>
    </body>
@stop
@section('js')
    <!-- layui规范化用法 -->
    <script type="text/javascript">
        layui.config({
            base: '/static/layerjs/'
        }).use('configsEdit');
    </script>@stop
