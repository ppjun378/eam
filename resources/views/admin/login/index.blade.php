@extends('common.baseAdmin')
@section('title',$title)
@section('css')
    <link rel="stylesheet" href="{{ asset('static/layercss/admin_login.css') }}">
@stop
@section('content')
<body>
    <div class="mask"></div>
    <div class="main">
        <h1><span style="font-size: 64px;">时空阁</span><span style="font-size:30px;"></span></h1>
        <p id="time"></p>
        <div onclick="layui.adminLogin.loginImport()" class="enter">
            Please&nbsp;&nbsp;Click&nbsp;&nbsp;Enter
        </div>
    </div>
</body>
@stop
@section('js')
    <!-- layui规范化用法 -->
    <script type="text/javascript">
        layui.config({
            base: '/static/layerjs/'
        }).use('adminLogin');
    </script>
    <script type="text/javascript">
        function systemTime() {
            //获取系统时间。
            var dateTime = new Date();
            var year = dateTime.getFullYear();
            var month = dateTime.getMonth() + 1;
            var day = dateTime.getDate();
            var hh = dateTime.getHours();
            var mm = dateTime.getMinutes();
            var ss = dateTime.getSeconds();

            //分秒时间是一位数字，在数字前补0。
            mm = extra(mm);
            ss = extra(ss);

            //将时间显示到ID为time的位置，时间格式形如：19:18:02
            document.getElementById("time").innerHTML = year + "-" + month + "-" + day + " " + hh + ":" + mm + ":" + ss;
            //每隔1000ms执行方法systemTime()。
            setTimeout("systemTime()", 1000);
        }

        //补位函数。
        function extra(x) {
            //如果传入数字小于10，数字前补一位0。
            if (x < 10) {
                return "0" + x;
            }
            else {
                return x;
            }
        }
        systemTime();
    </script>@stop
