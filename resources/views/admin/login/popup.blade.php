<form class="layui-form" action="">
    <div class="layui-form-item">
        <label class="layui-form-label">账号</label>
        <div class="layui-input-inline pm-login-input">
            <input type="text" name="account" lay-verify="account" autofocus placeholder="请输入账号" value="" autocomplete="off" class="layui-input">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">密码</label>
        <div class="layui-input-inline pm-login-input">
            <input type="password" name="password" lay-verify="passWord" autofocus placeholder="请输入密码" value="" autocomplete="off" class="layui-input">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">人机验证</label>
        <div class="layui-input-block">
            <div class="l-captcha" data-width="280px" data-site-key="{{ $siteKey }}"  lay-verify="result_response"></div>
        </div>
    </div>
    <div class="layui-form-item" style="margin-top:25px;margin-bottom:0;">
        <div class="layui-input-block">
            <button class="layui-btn" style="width:230px;" lay-submit="" lay-filter="login">立即登录</button>
        </div>
    </div>
</form>
<script src="//captcha.luosimao.com/static/dist/api.js"></script>