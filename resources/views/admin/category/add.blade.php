@extends('common.baseAdmin')
@section('title',$title)
@section('css')
    <!-- layui.css -->
    <link href="{{ asset('static/plugins/layui/css/layui.css') }}" rel="stylesheet" />
    <!--font-awesome-->
    <link href="{{ asset('static/plugins/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" />
    <!--wangEditor-->
    <link href="{{ asset('static/plugins/wangEditor-2.1.23/dist/css/wangEditor.min.css') }}" rel="stylesheet" />
    <!--全局样式表-->
    <link href="{{ asset('static/layercss/global.css') }}" rel="stylesheet" />
    <style>
        #articleBoxTitle {
            text-align: center;
        }
        .form-main {
            max-width: 900px;
            min-width: 900px;
            text-align: left;
            margin: 0 auto;
            margin-top: 10px;
        }
    </style>
@stop
@section('content')
<body>
    <fieldset id="dataConsole" class="layui-elem-field layui-field-title">
        <legend id="articleBoxTitle">添加文章</legend>
        <div class="layui-field-box">
            <div id="articleContent">
                <!-- 内容 -->
                <form class="layui-form form-main">
                    <div class="layui-form-item">
                        <label class="layui-form-label">标题</label>
                        <div class="layui-input-block">
                            <input type="text" name="title" placeholder="请输入标题" autocomplete="off" required lay-verify="required" class="layui-input" />
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <label class="layui-form-label">分类</label>
                        <div class="layui-input-inline">
                            <select name="cid" lay-verify="required" id="categorybox">
                                <option value=""></option>
                                @foreach( $category as $value )
                                    <option value="{{$value->id}}">{{$value->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <label class="layui-form-label">作者</label>
                        <div class="layui-input-inline">
                            <input type="text" name="author" placeholder="请输入作者名" autocomplete="off" value="dwl" required lay-verify="required" class="layui-input">
                        </div>
                    </div>
                    <div class="layui-form-item layui-form-text">
                        <label class="layui-form-label">摘要</label>
                        <div class="layui-input-block">
                            <textarea placeholder="可以不填，如不填；则截取文章内容前300字为摘要" name="description" class="layui-textarea"></textarea>
                        </div>
                    </div>
                    <div class="layui-form-item layui-form-text">
                        <label class="layui-form-label">文本域</label>
                        <div class="layui-input-block">
                            <div id="editors" style="height:360px;">
                                <p>请输入内容...</p>
                            </div>
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <label class="layui-form-label">封面</label>
                        <div class="layui-upload">
                            <button type="button" class="layui-btn" id="uoloadCover">上传图片</button>
                            <div class="layui-upload-list" style="margin-left: 110px;">
                                <img class="layui-upload-img" width="200" id="pictrueUrl" src="">
                                <p id="uploadError"></p>
                            </div>
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <label class="layui-form-label">选项</label>
                        <div class="layui-input-block">
                            <input type="checkbox" name="status" title="显示" value="1" checked>
                            <input type="checkbox" name="is_original" title="原创" value="1">
                            <input type="checkbox" name="is_top" title="顶置" value="1">
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <div class="layui-input-block">
                            <button class="layui-btn" lay-submit lay-filter="article">立即提交</button>
                            <a id="articleBack" href="javascript:history.go(-1);" type="button" class="layui-btn layui-btn-primary">返回列表</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </fieldset>
    <!-- layui.js -->
    <script src="{{ asset('static/plugins/layui/layui.js') }}"></script>
    <script src="{{ asset('static/plugins/plupload/plupload.full.min.js') }}"></script>
    <script src="{{ asset('static/plugins/wangEditor-2.1.23/dist/js/lib/jquery-1.10.2.min.js') }}"></script>
    <script src="{{ asset('static/plugins/wangEditor-2.1.23/dist/js/wangEditor.min.js') }}"></script>
    <script src="{{ asset('static/layerjs/wangEditor.js') }}"></script>
    <script type="text/javascript">
        layui.config({
            base: '/static/layerjs/'
        }).use('articleAdd');
    </script>
</body>
@stop