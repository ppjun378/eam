@extends('common.baseAdmin')
@section('title',$title)
@section('css')
    <!-- layui.css -->
    <link href="{{ asset('static/plugins/layui/css/layui.css') }}" rel="stylesheet" />
    <!--font-awesome-->
    <link href="{{ asset('static/plugins/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" />
    <!--全局样式表-->
    <link href="{{ asset('static/layercss/global.css') }}" rel="stylesheet" />
    <link rel="stylesheet" href="{{ asset('static/layercss/mystyle.css') }}">
@stop
@section('content')
<body>
<fieldset id="dataConsole" class="layui-elem-field layui-field-title" >
    <legend>控制台</legend>
    <div class="layui-field-box">
        <div id="articleIndexTop">
            <div class="layui-form layui-form-pane">
                <div class="layui-form-item" style="margin:0;margin-top:15px;">
                    <div class="layui-inline">
                        <label class="layui-form-label">分类</label>
                        <div class="layui-input-inline">
                            <select name="cid" id="categorybox" lay-filter="category">
                                <option value=""></option>
                            </select>
                        </div>
                        <label class="layui-form-label">关键词</label>
                        <div class="layui-input-inline">
                            <input id="keyword" type="text" name="search" autocomplete="off" class="layui-input">
                        </div>
                        <div class="layui-input-inline" style="width:auto">
                            <button class="layui-btn" lay-submit lay-filter="formSearch">搜索</button>
                        </div>
                    </div>
                    <div class="layui-inline">
                        <div class="layui-input-inline" style="width:auto">
                            <a class="layui-btn layui-btn-normal" href="add">发表文章</a>
                        </div>
                    </div>
                    <div class="layui-inline">
                        <div class="layui-input-inline" style="width:auto">
                            <button class="layui-btn layui-btn layui-icon" onclick="location.reload(this);">&#xe669;</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</fieldset>
<fieldset id="dataList" class="layui-elem-field layui-field-title sys-list-field" >
    <legend style="text-align:center;">文章列表</legend>
    <div class="layui-field-box" style="position: fixed;">
        <div id="dataContent" class="">
            <!--内容区域 ajax获取-->
            <table id="databox" style="" class="layui-table" lay-even=""></table>
            <div id="pagebox"></div>
            {{--<blockquote class="layui-elem-quote layui-quote-nm layui-text" style="margin-top: 15px;"></blockquote>--}}
        </div>
    </div>
</fieldset>
<script src="{{ asset('static/js/jquery.min.js?v=2.1.4') }}"></script>
<!-- layui.js -->
<script src="{{ asset('static/plugins/layui/layui.js') }}"></script>
<!-- layui规范化用法 -->
<script type="text/javascript">
    layui.config({
        base: '/static/layerjs/'
    }).use('articleList');
</script>
</body>
@stop