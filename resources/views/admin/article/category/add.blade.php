@extends('common.baseAdmin')
@section('title',$title)
@section('css')
@stop
@section('content')
    <div style="margin-top: 5%;"></div>
    <form class="layui-form">
        <div class="layui-form-item">
            <label class="layui-form-label">分类名称：</label>
            <div class="layui-input-inline">
                <input id="categoryName" type="text" name="name" required lay-verify="required" placeholder="请输入分类名称" class="layui-input">
            </div>
        </div>
        <div class="layui-form-item layui-form-text">
            <label class="layui-form-label">分类描述：</label>
            <div class="layui-input-block" style="max-width: 300px;">
                <textarea id="categoryDescription" name="description" placeholder="请输入内容" class="layui-textarea"></textarea>
            </div>
        </div>
    </form>
@stop
