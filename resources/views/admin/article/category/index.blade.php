@extends('common.baseAdmin')
@section('title',$title)
@section('css')
    <style>
        legend {
            text-align: center;
        }
    </style>
@stop
@section('content')
    <body>
    <fieldset id="dataConsole" class="layui-elem-field layui-field-title">
        <legend>控制台</legend>
        <div class="layui-field-box">
            <div id="articleIndexTop">
                <div class="layui-form layui-form-pane" style="text-align: center;">
                    <div class="layui-form-item" style="margin:0;margin-top:15px;">
                        <div class="layui-inline">
                            <label class="layui-form-label">关键词</label>
                            <div class="layui-input-inline">
                                <input id="keyword" type="text" name="search" autocomplete="off" class="layui-input">
                            </div>
                            <div class="layui-input-inline" style="width:auto">
                                <button class="layui-btn" lay-submit lay-filter="formSearch">搜索</button>
                            </div>
                        </div>
                        <div class="layui-inline">
                            <div class="layui-input-inline" style="width:auto">
                                <a class="layui-btn layui-btn-normal" onclick="layui.categoryList.addData()">添加分类</a>
                            </div>
                        </div>
                        <div class="layui-inline">
                            <div class="layui-input-inline" style="width:auto">
                                <button class="layui-btn layui-btn layui-icon" onclick="location.reload(this);">
                                    &#xe669;
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </fieldset>
    <fieldset id="dataList" class="layui-elem-field layui-field-title sys-list-field">
        <legend id="dataBoxTitle">分类列表</legend>
        <div class="layui-field-box" style="position: fixed;">
            <div id="dataContent" class="">
                <!--内容区域 ajax获取-->
                <table id="databox" style="" class="layui-table" lay-even=""></table>
                <div id="pagebox"></div>
            </div>
        </div>
    </fieldset>
    </body>
@stop
@section('js')
    <!-- layui规范化用法 -->
    <script type="text/javascript">
        layui.config({
            base: '/static/layerjs/'
        }).use('categoryList');
    </script>
@stop