@extends('common.baseAdmin')
@section('title',$title)
@section('css')
@stop
@section('content')
    <body>
    <div style="display: block;height: 50px;"></div>
    <form class="layui-form">
        <div class="layui-form-item">
            <label class="layui-form-label">原密码</label>
            <div class="layui-input-inline">
                <input type="password" name="oldpass" id="oldpass" placeholder="请输入原密码" autocomplete="off" class="layui-input" lay-verify="password" >
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">新密码</label>
            <div class="layui-input-inline">
                <input type="password" name="newpass" id="newpass" placeholder="请输入新密码" autocomplete="off" class="layui-input" lay-verify="password" >
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">确认密码</label>
            <div class="layui-input-inline">
                <input type="password" name="repass" id="repass" placeholder="请确认新密码" autocomplete="off" class="layui-input" lay-verify="password" >
            </div>
        </div>
        <div class="layui-form-item">
            <div class="layui-input-block">
                <a class="layui-btn" lay-submit lay-filter="savepass">立即提交</a>
                <button type="reset" class="layui-btn layui-btn-primary">重置</button>
            </div>
        </div>
    </form>
    </body>
@stop
@section('js')
    <script type="text/javascript">
        layui.config({
            base: '/static/layerjs/'
        }).use('userPass');
    </script>
@stop