@extends('common.baseAdmin')
@section('title',$title)
@section('css')
    <!--cropper-->
    <link href="{{ asset('static/plugins/cropper/cropper.min.css') }}" rel="stylesheet">
    <link href="{{ asset('static/plugins/cropper/ImgCropping.css') }}" rel="stylesheet">
@stop
@section('content')
    <body>
    <div style="display: block;height: 50px;"></div>
    <form class="layui-form" lay-filter="formData" action="">
        <div style="width:50%;float:left;">
            @inject('UserInfoPresenter','App\Presenters\UserInfoPresenter')
            {{--角色--}}
            <div class="layui-form-item">
                <label class="layui-form-label">角色</label>
                <div class="layui-input-inline">
                    <select name="interest" lay-filter="role" disabled>
                        <option value=""></option>
                        <option value="0">超级管理员</option>
                        <option value="1" selected="">博主</option>
                    </select>
                </div>
                <div class="layui-form-mid layui-word-aux">当前角色不可更改</div>
            </div>

            {{--账号--}}
            <div class="layui-form-item">
                <label class="layui-form-label">账号</label>
                <div class="layui-input-inline">
                    <input type="text" name="account" autocomplete="off" placeholder="请输入账号" class="layui-input" value="{{ $session_info['account'] }}" disabled>
                </div>
                <div class="layui-form-mid layui-word-aux">不可修改，一般用于后台登入名</div>
            </div>

            {{--昵称--}}
            <div class="layui-form-item">
                <label class="layui-form-label">昵称</label>
                <div class="layui-input-inline">
                    <input type="text" name="nickname" lay-verify="nickname" autocomplete="off" placeholder="请输入昵称" class="layui-input" value="{{ $userinfo->nickname }}">
                </div>
            </div>

            {{--出生日期--}}
            <div class="layui-form-item">
                <label class="layui-form-label">出生日期</label>
                <div class="layui-input-inline">
                    <input type="text" name="birthdate" id="birthdate" placeholder="yyyy-mm-dd" autocomplete="off" class="layui-input" value="{{ $userinfo->birthdate }}">
                </div>
            </div>

            {{--性别--}}
            <div class="layui-form-item">
                <label class="layui-form-label">性别</label>
                <div class="layui-input-block">

                    <input type="radio" name="gender" value="男" title="男" {{ $UserInfoPresenter->genderM($userinfo->gender) }})>
                    <input type="radio" name="gender" value="女" title="女" {{ $UserInfoPresenter->genderF($userinfo->gender) }}>
                </div>
            </div>
        </div>
        <div style="width:50%;float: right;">
            {{--头像--}}
            <div class="layui-form-item">
                <label class="layui-form-label">头像</label>
                <div class="layui-input-block">
                    <div class="layui-upload-list" style="padding:10px 0px">
                        <img width="150px" height="150px" src="{{ $UserInfoPresenter->avatarCheck($userinfo->avatar) }}" id="srcimgurl" class="layui-upload-img">
                    </div>
                </div>
                <div class="layui-input-inline layui-btn-container" style="width: auto;">
                    <label class="layui-form-label"></label>
                    <a class="layui-btn" href="javascript:;" id="replaceImg" onclick="layui.userShow.tailorContainer()"><i class="layui-icon">&#xe67c;</i>修改图片</a>
                    <!--隐藏域 用来存储上传后的图片地址-->
                    <input type="hidden" name="avatar" id="pnaCoverPic" value="">
                </div>
                <div class="layui-form-mid layui-word-aux">头像的尺寸限定150x150px,大小在50kb左右</div>
            </div>
        </div>

        {{--地区选择--}}
        <div class="layui-form-item">
            <label class="layui-form-label">地址</label>
            <div id="addressText" style="display: block;">
                <div class="layui-input-inline">
                    <input type="text" id="region" name="region" lay-verify="region" autocomplete="off" placeholder="请输入地址" class="layui-input" value="{{$userinfo->region}}">
                </div>
                <a class="layui-btn layui-btn-primary" href="javascript:;" id="replaceImg" onclick="layui.userShow.showDistrict()">点击修改地址</a>
            </div>
            <div id="addressSelect" style="display: none">
                <div class="layui-input-inline">
                    <select name="provinces" lay-filter="provinces">
                        <option value="">请选择省</option>
                    </select>
                </div>
                <div class="layui-input-inline">
                    <select name="cities" lay-filter="cities">
                        <option value="">请选择市</option>
                    </select>
                </div>
                <div class="layui-input-inline">
                    <select name="districts" lay-filter="districts">
                        <option value="">请选择县/区</option>
                    </select>
                </div>
            </div>
        </div>

        {{--手机--}}
        <div class="layui-form-item">
            <div class="layui-inline">
                <label class="layui-form-label">验证手机</label>
                <div class="layui-input-inline">
                    <input type="tel" name="phone" lay-verify="myphone" autocomplete="off" class="layui-input" placeholder="请输入手机号" value="{{ $userinfo->phone }}">
                </div>
            </div>
        </div>

        {{--邮箱--}}
        <div class="layui-form-item">
            <div class="layui-inline">
                <label class="layui-form-label">验证邮箱</label>
                <div class="layui-input-inline">
                    <input type="text" name="email" lay-verify="myemail" autocomplete="off" class="layui-input" placeholder="请输入邮箱" value="{{ $userinfo->email }}">
                </div>
            </div>
        </div>

        {{--个人简介--}}
        <div class="layui-form-item layui-form-text">
            <label class="layui-form-label">个人简介</label>
            <div class="layui-input-block" style="max-width: 500px;">
                <textarea placeholder="请输入内容" class="layui-textarea" name="compendium">{{ $userinfo->compendium }}</textarea>
            </div>
        </div>

        {{--提交--}}
        <div class="layui-form-item">
            <div class="layui-input-block">
                <button class="layui-btn" lay-submit lay-filter="update">立即提交</button>
            </div>
        </div>
    </form>

    <!--图片裁剪器的工作页面-->
    <div style="display: none" class="tailoring-container">
        <div class="black-cloth" onClick="layui.userShow.closeTailor(this)"></div>
        <div class="tailoring-content">
            <div class="tailoring-content-one">
                <label title="上传图片" for="chooseImg" class="l-btn choose-btn">
                    <input type="file" accept="image/jpg,image/jpeg,image/png" name="file" id="chooseImg" class="hidden" onChange="layui.userShow.selectImg(this)">选择图片</label>
                <div class="close-tailoring"  onclick="layui.userShow.closeTailor(this)">×</div>
            </div>
            <div class="tailoring-content-two">
                <div class="tailoring-box-parcel">
                    <img id="tailoringImg"></div>
                <div class="preview-box-parcel">
                    <p>图片预览：</p>
                    <div class="square previewImg"></div>
                    <div class="circular previewImg"></div>
                </div>
            </div>
            <div class="tailoring-content-three">
                <span class="l-btn" style="cursor: pointer;" onclick="layui.userShow.resetImg()">复位</span>
                <span class="l-btn" style="cursor: pointer;" onclick="layui.userShow.rotateImg()">旋转</span>
                <span class="l-btn" style="cursor: pointer;" onclick="layui.userShow.cropperScaleX()">换向</span>
                <span class="l-btn sureCut" id="sureCut" style=" cursor: pointer;" onclick="layui.userShow.sureCut()">确定</span>
            </div>
        </div>
    </div>
    </body>
@stop
@section('js')
    <script src="{{ asset('static/plugins/cropper/cropper.min.js') }}"></script>
    <script type="text/javascript">
        layui.config({
            base: '/static/layerjs/'
        }).use('userShow');
    </script>
@stop
