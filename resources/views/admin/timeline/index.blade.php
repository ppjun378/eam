@extends('common.baseAdmin')
@section('title',$title)
@section('css')
    <link rel="stylesheet" href="{{ asset('static/layercss/mystyle.css') }}">
@stop
@section('content')
    <body>
    <fieldset id="timeLineConsole" class="layui-elem-field layui-field-title" style="display:block;text-align:center;">
        <legend>控制台</legend>
        <div class="layui-field-box">
            <div id="timeLineIndexTop">
                <form class="layui-form layui-form-pane" action="">
                    <div class="layui-form-item" style="margin:0;margin-top:15px;">
                        <div class="layui-inline" style="margin-right:0px">
                            <div class="layui-inline">
                                <a id="addTimeLine" class="layui-btn layui-btn-normal"
                                   onclick="layui.timelineList.addDataPage()">添加时光轴</a>
                            </div>
                            <div class="layui-inline" style="width:auto">
                                <button class="layui-btn layui-btn layui-icon" onclick="location.reload(this);">
                                    &#xe669;
                                </button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </fieldset>
    <fieldset id="timeLineList" class="layui-elem-field layui-field-title" style="display:block">
        <legend id="dataBoxTitle" style="text-align:center;">时光轴列表</legend>
        <div class="layui-field-box" style="position: fixed;">
            <div id="timeLineIndex">
                <!-- 内容区域 ajax获取 -->
                <table id="databox" class="layui-table"></table>
                <div id="pagebox"></div>
            </div>
        </div>
    </fieldset>


    </body>
@stop
@section('js')
    <!-- layui规范化用法 -->
    <script type="text/javascript">
        layui.config({
            base: '/static/layerjs/'
        }).use('timelineList');
    </script>
@stop
