@extends('common.baseAdmin')
@section('title',$title)
@section('css')
@stop
@section('content')
    <div style="padding-top: 20px;">
        <!-- 内容 -->
        <form class="layui-form form-main">
            <div class="layui-form-item layui-form-text">
                <label class="layui-form-label">内容</label>
                <div style="background: #ffffff" class="layui-input-inline">
                <textarea id="timeLineContent" name="editContent" lay-verify="timeLineContent"
                          class="layui-textarea">{{ $data->content }}</textarea>
                </div>
            </div>
        </form>
    </div>
@stop
@section('js')
    <!-- layui规范化用法 -->
    <script type="text/javascript">
        layui.define('layedit', function (exports) {
            var layedit = layui.layedit,
                $ = layui.jquery;
            var contentStr = layedit.build('timeLineContent', {
                tool: ['strong', 'italic', 'underline', 'del', '|', 'left', 'center', 'right', 'link', 'unlink', 'face']
            }); //建立编辑器

            var editTimeline = {
                getStr: function () {
                    return layedit.getContent(contentStr);
                }
            };

            exports('editTimeline',editTimeline);
        });

        function getdata() {
            return layui.editTimeline.getStr();
        };
    </script>
@stop