@extends('common.baseAdmin')
@section('title',$title)
@section('css')
    <link href="{{ asset('/static/css/bootstrap.min14ed.css?v=3.3.6') }}" rel="stylesheet">
    <link href="{{ asset('/static/css/plugins/blueimp/css/blueimp-gallery.min.css') }}" rel="stylesheet">
    <link href="{{ asset('/static/css/animate.min.css') }}" rel="stylesheet">
    <link href="{{ asset('/static/css/style.min862f.css?v=4.1.0') }}" rel="stylesheet">
@stop
@section('content')
<body class="gray-bg">
<div class="wrapper wrapper-content">
    <div class="row">
        <div class="col-sm-3">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <div class="file-manager">
                        <h5>显示：</h5>
                        <a onclick="layui.fileManager.selectFileType('')" class="file-control active">所有</a>
                        <a onclick="layui.fileManager.selectFileType('application/')" class="file-control">文档</a>
                        <a onclick="layui.fileManager.selectFileType('video/')" class="file-control">视频</a>
                        <a onclick="layui.fileManager.selectFileType('image/')" class="file-control">图片</a>
                        <div class="hr-line-dashed"></div>
                        <a class="btn btn-primary btn-block" href="/admin/file/upload">上传文件</a>
                        <div class="hr-line-dashed"></div>
                        <h5>文件夹</h5>
                        <ul class="folder-list" style="padding: 0">
                            <li><a onclick="layui.fileManager.selectFileType('application/')"><i class="fa fa-folder"></i> 文件</a></li>
                            <li><a onclick="layui.fileManager.selectFileType('image/')"><i class="fa fa-folder"></i> 图片</a></li>
                            <li><a onclick="layui.fileManager.selectFileType('audio/')"><i class="fa fa-folder"></i> 音乐</a></li>
                            <li><a onclick="layui.fileManager.selectFileType('video/')"><i class="fa fa-folder"></i> 电影</a></li>
                            <li><a onclick="layui.fileManager.selectFileType('text/')"><i class="fa fa-folder"></i> 书籍</a></li>
                        </ul>
                        <h5 class="tag-title">标签</h5>
                        <ul class="tag-list" style="padding: 0">
                            @foreach($filetags as $val)
                                <li><a onclick="layui.fileManager.selectFileTag('{{ $val->name }}')">{{ $val->name }}</a></li>
                            @endforeach
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-9 animated fadeInRight">
            <div class="row">
                <div class="col-sm-12 lightBoxGallery" id="file-box"></div>
            </div>
            <div id="filesPage" style="background: #ffffff; padding: 5px 30px 0px;"></div>
        </div>
    </div>
</div>
</body>
@stop
@section('js')
    <script src="{{ asset('/static/js/bootstrap.min.js?v=3.3.6') }}"></script>
    <script src="{{ asset('/static/js/content.min.js?v=1.0.0') }}"></script>
    <script type="text/javascript">
        layui.config({
            base: '/static/layerjs/'
        }).use('fileManager');
    </script>
    <script src="{{ asset('/static/js/plugins/blueimp/jquery.blueimp-gallery.min.js') }}"></script>
@stop
