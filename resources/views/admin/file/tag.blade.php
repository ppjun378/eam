@extends('common.baseAdmin')
@section('title',$title)
@section('css')
    <link href="{{ asset('/static/layercss/file_tag.css') }}" rel="stylesheet">
@stop
@section('content')
<body class="gray-bg">
<div class="tagBox">
    <div class="tagTiltle">
        <h3>你的文件标签</h3>
        {{--<p>Add up your 10 skill tags to display on your profile</p>--}}
    </div>
    <div class="tagCon clearfix" id="tagcon">

    </div>
    <div class="addBox clearfix" id="addBox">
        <input id="intxt" class="" type="text"/>
        <button id="addBtn">添加标签</button>
    </div>
</div>
</body>
@stop
@section('js')
<script src="{{ asset('/static/layerjs/fileTag.js') }}"></script>
@stop
