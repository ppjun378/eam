@extends('common.baseAdmin')
@section('title',$title)
@section('css')
    <link href="{{ asset('/static/css/bootstrap.min14ed.css?v=3.3.6') }}" rel="stylesheet">
    <link href="{{ asset('/static/css/animate.min.css') }}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ asset('/static/plugins/webuploader-0.1.5/dist/webuploader.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/static/layercss/webuploader.css') }}">
    <link href="{{ asset('/static/css/style.min862f.css?v=4.1.0') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('/static/layercss/upload_file_tag.css') }}">
@stop
@section('content')
<body class="gray-bg">
<div class="wrapper wrapper-content animated fadeIn">
    <div class="row">
        <div class="col-sm-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>文件上载（Layui Upload）</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                        <a class="dropdown-toggle" data-toggle="dropdown" href="form_file_upload.html#">
                            <i class="fa fa-wrench"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-user">
                            <li><a href="#">选项1</a>
                            </li>
                            <li><a href="#">选项2</a>
                            </li>
                        </ul>
                        <a class="close-link">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <div class="page-container">
                        <div class="layui-upload">
                            <div>
                                <button type="button" class="layui-btn layui-btn-normal" id="testList" style="float:left;">选择多文件</button>

                                <div id="tag-wrap1">
                                    <div class="label-selected1">
                                        <a href="javascript:;" class="layui-btn layui-btn-sm show-labelitem1" style="float: right; margin: 4px; display: block;line-height: 30px;">展开标签库 </a>
                                        <a href="javascript:;" class="layui-btn layui-btn-sm hide-labelitem1" style="float: right; margin: 4px; display: none;line-height: 30px;">收起标签库 </a>
                                        <input type="hidden" name="label1">
                                    </div>
                                    <div class="layui-col-md12" id="labelItem1">
                                        <!--标签库-->
                                        <div class="label-item1">
                                            <a href="javascript:;" class="replacelable" style="position: absolute;right:1rem;bottom:.75rem;color: #1994dc" onselectstart="return false">换一批</a>
                                            @foreach($filetags as $val)
                                                <li data="{{ $val->id }}"><span>{{ $val->name }}</span></li>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="layui-upload-list">
                                <table class="layui-table">
                                    <thead>
                                    <tr><th>文件名</th>
                                        <th>大小</th>
                                        <th>状态</th>
                                        <th>操作</th>
                                    </tr></thead>
                                    <tbody id="fileList"></tbody>
                                </table>
                            </div>
                            <button type="button" class="layui-btn" id="fileListAction">开始上传</button>
                            <a id="articleBack" href="javascript:history.go(-1);" type="button" class="layui-btn layui-btn-primary">返回列表</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>图片上载（Web Uploader）</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="fa fa-wrench"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-user">
                            <li><a href="#">选项1</a>
                            </li>
                            <li><a href="#">选项2</a>
                            </li>
                        </ul>
                        <a class="close-link">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <div class="page-container">

                        <div id="tag-wrap2">
                            <div class="label-selected2">
                                <a href="javascript:;" class="layui-btn layui-btn-sm show-labelitem2" style="float: right; margin: 4px; display: block;line-height: 30px;">展开标签库 </a>
                                <a href="javascript:;" class="layui-btn layui-btn-sm hide-labelitem2" style="float: right; margin: 4px; display: none;line-height: 30px;">收起标签库 </a>
                                <input type="hidden" name="label2">
                            </div>
                            <div class="layui-col-md12" id="labelItem2">
                                <!--标签库-->
                                <div class="label-item2">
                                    <a href="javascript:;" class="replacelable" style="position: absolute;right:1rem;bottom:.75rem;color: #1994dc" onselectstart="return false">换一批</a>
                                    @foreach($filetags as $val)
                                        <li data="{{ $val->id }}"><span>{{ $val->name }}</span></li>
                                    @endforeach
                                </div>
                            </div>
                        </div>

                        <div id="uploader" class="wu-example">
                            <div class="queueList">
                                <div id="dndArea" class="placeholder">
                                    <div id="filePicker"></div>
                                    <p>或将照片拖到这里，单次最多可选300张</p>
                                </div>
                            </div>
                            <div class="statusBar" style="display:none;">
                                <div class="progress">
                                    <span class="text">0%</span>
                                    <span class="percentage"></span>
                                </div>
                                <div class="info"></div>
                                <div class="btns">
                                    <div id="filePicker2"></div>
                                    <div class="uploadBtn">开始上传</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
@stop
@section('js')
    <script src="{{ asset('/static/js/bootstrap.min.js?v=3.3.6') }}"></script>
    <script src="{{ asset('/static/js/content.min.js?v=1.0.0') }}"></script>
    <script src="{{ asset('static/layerjs/upload_file_tag.js') }}"></script>
    <script type="text/javascript">
        var BASE_URL = "{{ asset('/static/plugins/webuploader-0.1.5/test/index.html') }}";
    </script>
    <script src="{{ asset('/static/plugins/webuploader-0.1.5/dist/webuploader.min.js') }}"></script>
    <script src="{{ asset('/static/layerjs/webuploader.js') }}"></script>
    <script type="text/javascript">
        layui.config({
            base: '/static/layerjs/'
        }).use('uploadlocal');
    </script>
@stop
