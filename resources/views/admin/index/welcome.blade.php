@extends('common.baseAdmin')
@section('title',$title)
@section('css')
    <link href="{{ asset('static/css/animate.min.css') }}" rel="stylesheet">
    <style>
        .layui-tab-content {
            position: absolute;
            top: 45px;
            bottom: 0px;
            right: 0px;
            left: 0;
        }
        .layui-tab-item {
            text-align: center;
            height: 100%;
        }
        .ht-box {
            display: inline-block;
            margin: 15px;
            padding: 15px 0;
            color: #fff;
            width: 12%;
        }
        .ht-box p:first-child {
            font-size: 40px;
        }
    </style>
@stop
@section('content')
    @inject('ConLogPresenter','App\Presenters\ConLogPresenter')
    @inject('StatisticsPresenter','App\Presenters\StatisticsPresenter')
    <!--主体内容-->
    <div class="layui-tab-content">
        <div class="layui-tab-item layui-show">
            <p style="padding: 10px 15px; margin-bottom: 20px; margin-top: 10px; border:1px solid #ddd;display:inline-block;">
                上次登陆
                <span style="padding-left:1em;">IP：{{ $ConLogPresenter->getPreviousLogin('login_ip') }}</span>
                <span style="padding-left:1em;">地点：{{ $ConLogPresenter->getPreviousLogin('login_location') }}</span>
                <span style="padding-left:1em;">时间：{{ $ConLogPresenter->getPreviousLogin('created_at') }}</span>
            </p>
            <fieldset class="layui-elem-field layui-field-title">
                <legend>统计信息</legend>
                <div class="layui-field-box">
                    <div style="display: inline-block; width: 100%;">
                        <div class="ht-box layui-bg-blue">
                            <p>{{ $StatisticsPresenter->UserCount() }}</p>
                            <p>用户总数</p>
                        </div>
                        <div class="ht-box layui-bg-red">
                            <p>{{ $StatisticsPresenter->RegTodayCount() }}</p>
                            <p>今日注册</p>
                        </div>
                        <div class="ht-box layui-bg-green">
                            <p>{{ $StatisticsPresenter->getLoginCount() }}</p>
                            <p>今日登陆</p>
                        </div>
                        <div class="ht-box layui-bg-orange">
                            <p>{{ $StatisticsPresenter->getArticleCount() }}</p>
                            <p>文章总数</p>
                        </div>
                        <div class="ht-box layui-bg-cyan">
                            <p>0</p>
                            <p>资源总数</p>
                        </div>
                        <div class="ht-box layui-bg-black">
                            <p>0</p>
                            <p>笔记总数</p>
                        </div>
                    </div>
                </div>
            </fieldset>
        </div>
    </div>
@stop
@section('js')
@stop
