<!DOCTYPE html>
<html>
<head>
    @inject('MetaPresenter','App\Presenters\MetaPresenter')
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta name="renderer" content="webkit">
    <meta http-equiv="Cache-Control" content="no-siteapp" />
    <meta name="keywords" content="{{ $MetaPresenter->getKeywords() }}">
    <meta name="description" content="{{ $MetaPresenter->getDescription() }}">
    <meta name="author" content="{{ $MetaPresenter->getAuthor() }}">
    @yield('meta')

    <title>@yield('title')</title>
    <!--[if lt IE 9]>
    <meta http-equiv="refresh" content="0;ie.html" />
    <![endif]-->
    <link rel="shortcut icon" href="/favicon.ico">
    <!-- layui.css -->
    <link href="{{ asset('static/plugins/layui/css/layui.css') }}" rel="stylesheet" />
    <!--font-awesome-->
    <link href="{{ asset('static/plugins/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" />
    <!--全局样式表-->
    <link href="{{ asset('static/layercss/global.css') }}" rel="stylesheet" />
    @yield('css')
</head>

@yield('content')

<script src="{{ asset('static/js/jquery.min.js?v=2.1.4') }}"></script>
<!-- layui.js -->
<script src="{{ asset('static/plugins/layui/layui.js') }}"></script>
@yield('js')
</html>
