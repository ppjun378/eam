/*
 Navicat MySQL Data Transfer

 Source Server         : 127.0.0.1
 Source Server Version : 80012
 Source Host           : 127.0.0.1
 Source Database       : eam

 Target Server Version : 80012
 File Encoding         : utf-8

 Date: 11/23/2018 17:58:23 PM
*/

SET NAMES utf8;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
--  Table structure for `eam_admins`
-- ----------------------------
DROP TABLE IF EXISTS `eam_admins`;
CREATE TABLE `eam_admins` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '管理员名称',
  `note` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '备注',
  `status` tinyint(4) NOT NULL COMMENT '状态',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
--  Records of `eam_admins`
-- ----------------------------
BEGIN;
INSERT INTO `eam_admins` VALUES ('1', '白俊生', '超级管理员', '1', null, null, null);
COMMIT;

-- ----------------------------
--  Table structure for `eam_assets`
-- ----------------------------
DROP TABLE IF EXISTS `eam_assets`;
CREATE TABLE `eam_assets` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `coding` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT '',
  `category_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `specification` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT '',
  `sn` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT '',
  `unit` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT '',
  `money` decimal(8,2) DEFAULT NULL,
  `use_the_company_id` int(11) DEFAULT NULL,
  `department_id` int(11) DEFAULT NULL,
  `purchase_date` int(11) DEFAULT NULL,
  `user_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `administrator` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  `area` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `location` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT '',
  `use_period` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT '',
  `supplier` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT '',
  `source` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT '',
  `note` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT '',
  `sort` int(11) DEFAULT '0',
  `image` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT '',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '2' COMMENT '资产状态',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
--  Records of `eam_assets`
-- ----------------------------
BEGIN;
INSERT INTO `eam_assets` VALUES ('1', 'zke12342', '1', '笔记本1', '14寸', '4478800892343211234331', '台', '4999.00', '1', '1', '1540958146', '1', '白浚生', '1', '1', '1', '1', '微软公司', '1', '1', '0', 'https://img11.360buyimg.com/n7/jfs/t25468/327/505003277/359498/708852ec/5b723894N7b459827.jpg', '2018-10-26 08:01:48', '2018-10-31 08:26:39', null, '2'), ('2', '2', '2', '笔记本2', '14寸', '4478800892343211234332', '台', '4999.00', '1', '1', '1540958146', null, '白浚生', null, null, '', '', '微软公司', '', '', '0', 'https://img11.360buyimg.com/n7/jfs/t25468/327/505003277/359498/708852ec/5b723894N7b459827.jpg', '2018-10-26 08:06:44', '2018-10-26 08:06:44', null, '2'), ('3', '3', '2', '笔记本3', '14寸', '4478800892343211234333', '台', '4999.00', '1', '1', '1540958146', null, '白浚生', null, null, '', '', '微软公司', '', '', '0', 'https://img11.360buyimg.com/n7/jfs/t25468/327/505003277/359498/708852ec/5b723894N7b459827.jpg', '2018-10-26 08:16:47', '2018-10-26 08:16:47', null, '2'), ('4', 'SK12324', '1', '笔记本电脑4', ' ', ' ', '台', '10.00', '1', '1', '1540958146', '1', '管理员', '1', '本地', '本地-1', '1', '微软公司', '购买', '', '1', 'https: //images.gitee.com/uploads/58/916358_ppjun378.png?1535425020', '2018-10-31 03:55:46', '2018-10-31 03:55:46', null, '2'), ('5', 'SK12325', '1', '笔记本电脑5', ' ', ' ', '台', '10.00', '1', '1', '1540958146', '1', '管理员', '1', '本地', '本地-1', '1', '微软公司', '购买', '', '1', 'https: //images.gitee.com/uploads/58/916358_ppjun378.png?1535425020', '2018-10-31 03:55:46', '2018-10-31 03:55:46', null, '2'), ('6', 'SK12326', '1', '笔记本电脑6', ' ', ' ', '台', '10.00', '1', '1', '1540958146', '1', '管理员', '1', '本地', '本地-1', '1', '微软公司', '购买', '', '1', 'https: //images.gitee.com/uploads/58/916358_ppjun378.png?1535425020', '2018-10-31 03:55:46', '2018-10-31 03:55:46', null, '2'), ('7', 'SK12327', '1', '笔记本电脑7', ' ', ' ', '台', '10.00', '1', '1', '1540958146', '1', '管理员', '1', '本地', '本地-1', '1', '微软公司', '购买', '', '1', 'https: //images.gitee.com/uploads/58/916358_ppjun378.png?1535425020', '2018-10-31 03:55:46', '2018-10-31 03:55:46', null, '2'), ('8', 'SK12328', '1', '笔记本电脑8', ' ', ' ', '台', '10.00', '1', '1', '1540958146', '1', '管理员', '1', '本地', '本地-1', '1', '微软公司', '购买', '', '1', 'https: //images.gitee.com/uploads/58/916358_ppjun378.png?1535425020', '2018-10-31 03:55:46', '2018-10-31 03:55:46', null, '2'), ('9', 'SK12329', '1', '笔记本电脑9', ' ', ' ', '台', '10.00', '1', '1', '1540958146', '1', '管理员', '1', '本地', '本地-1', '1', '微软公司', '购买', '', '1', 'https: //images.gitee.com/uploads/58/916358_ppjun378.png?1535425020', '2018-10-31 03:55:46', '2018-10-31 03:55:46', null, '2'), ('10', 'SK12310', '1', '笔记本电脑10', ' ', ' ', '台', '10.00', '1', '1', '1540958146', '1', '管理员', '1', '本地', '本地-1', '1', '微软公司', '购买', '', '1', 'https: //images.gitee.com/uploads/58/916358_ppjun378.png?1535425020', '2018-10-31 03:55:46', '2018-10-31 03:55:46', null, '2'), ('11', 'SK12311', '1', '笔记本电脑11', ' ', ' ', '台', '10.00', '1', '1', '1540958146', '1', '管理员', '1', '本地', '本地-1', '1', '微软公司', '购买', '', '1', 'https: //images.gitee.com/uploads/58/916358_ppjun378.png?1535425020', '2018-10-31 03:55:46', '2018-10-31 03:55:46', null, '2'), ('12', 'SK12312', '1', '笔记本电脑12', ' ', ' ', '台', '10.00', '1', '1', '1540958146', '1', '管理员', '1', '本地', '本地-1', '1', '微软公司', '购买', '', '1', 'https: //images.gitee.com/uploads/58/916358_ppjun378.png?1535425020', '2018-10-31 03:55:46', '2018-10-31 03:55:46', null, '2'), ('13', 'SK12313', '1', '笔记本电脑13', ' ', ' ', '台', '10.00', '1', '1', '1540958146', '1', '管理员', '1', '本地', '本地-1', '1', '微软公司', '购买', '', '1', 'https: //images.gitee.com/uploads/58/916358_ppjun378.png?1535425020', '2018-10-31 03:55:46', '2018-10-31 03:55:46', null, '2');
COMMIT;

-- ----------------------------
--  Table structure for `eam_borrows`
-- ----------------------------
DROP TABLE IF EXISTS `eam_borrows`;
CREATE TABLE `eam_borrows` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `borrow_user` int(11) NOT NULL,
  `borrow_date` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `expect_revert_date` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT '0',
  `borrow_operator` int(11) DEFAULT '1',
  `comment` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `select_assets` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '0',
  `signature_images` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `borrow_number` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '借用单号',
  `actual_revert_date` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '0' COMMENT '实际归还日期',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
--  Records of `eam_borrows`
-- ----------------------------
BEGIN;
INSERT INTO `eam_borrows` VALUES ('1', '1', '20181190', '20181190', '1', '借用', '1,3', '0', '', null, null, null, 'JY23213', '0'), ('2', '1', '2018-11-21', '2018-11-22', '1', '借用您的两个资产', '1,2', '0', '', '2018-11-21 03:29:09', '2018-11-21 03:29:09', null, 'JY123', '2018-11-22'), ('3', '1', '2018-11-21', '2018-11-22', '1', '借用您的两个资产', '1,2,3', '0', '', '2018-11-21 03:30:05', '2018-11-21 03:30:05', null, 'JY1233', '2018-11-22');
COMMIT;

-- ----------------------------
--  Table structure for `eam_category`
-- ----------------------------
DROP TABLE IF EXISTS `eam_category`;
CREATE TABLE `eam_category` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '分类名',
  `keywords` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '关键词',
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '描述',
  `sort` int(11) NOT NULL DEFAULT '0' COMMENT '排序',
  `pid` int(11) NOT NULL DEFAULT '0' COMMENT '分级栏目ID',
  `status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '启用(1)停用(2)',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
--  Records of `eam_category`
-- ----------------------------
BEGIN;
INSERT INTO `eam_category` VALUES ('1', '土地、房屋及建筑物', '', '', '1', '0', '1', null, null, null), ('2', '通用设备', '', '', '1', '0', '1', null, null, null), ('3', '专用设备', '', '', '1', '0', '1', null, null, null), ('4', '交通运输设备', '', '', '1', '0', '1', null, null, null), ('5', '电气设备', '', '', '1', '0', '1', null, null, null), ('6', '电子产品及通信设备', '', '', '1', '0', '1', null, null, null), ('7', '仪器仪表、计量标准器具及量具、衡器', '', '', '1', '0', '1', null, null, null), ('8', '文艺体育设备', '', '', '1', '0', '1', null, null, null), ('9', '图书文物及陈列品', '', '', '1', '0', '1', null, null, null), ('10', '家具用具及其他类', '', '', '1', '0', '1', null, null, null);
COMMIT;

-- ----------------------------
--  Table structure for `eam_companys`
-- ----------------------------
DROP TABLE IF EXISTS `eam_companys`;
CREATE TABLE `eam_companys` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `company_number` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '公司编号',
  `company_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '公司名称',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0' COMMENT '父类id',
  `sort` int(11) DEFAULT '0' COMMENT '排序',
  `is_company` tinyint(2) DEFAULT NULL COMMENT '是否为公司1.是0。否',
  `is_disabled` tinyint(2) DEFAULT NULL COMMENT '是否启用1.是0.否',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
--  Records of `eam_companys`
-- ----------------------------
BEGIN;
INSERT INTO `eam_companys` VALUES ('1', '1', '盈富永泰网络科技有限公司', null, null, null, '0', '0', null, null), ('2', '1', '网络部', null, null, null, '1', '0', null, null), ('3', '1', '销售部', null, null, null, '1', '0', null, null), ('4', '1', '客服部', null, null, null, '1', '0', null, null), ('5', '1', '人事部', null, null, null, '1', '0', null, null);
COMMIT;

-- ----------------------------
--  Table structure for `eam_deal_withs`
-- ----------------------------
DROP TABLE IF EXISTS `eam_deal_withs`;
CREATE TABLE `eam_deal_withs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `asset_id` int(11) NOT NULL COMMENT '资产编号',
  `deal_with_date` int(11) NOT NULL COMMENT '处理日期',
  `deal_with_man_id` int(11) NOT NULL COMMENT '处理人编号',
  `deal_with_content` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '处理内容',
  `deal_with_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '处理时资金的状态',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
--  Records of `eam_deal_withs`
-- ----------------------------
BEGIN;
INSERT INTO `eam_deal_withs` VALUES ('1', '1', '1542080735', '1', '【使用人】字段由<空>变更为“白俊生1.0', '1', null, null, null), ('2', '1', '1542080735', '1', '【使用人】字段由<空>变更为“白俊生2.0', '1', null, null, null), ('3', '1', '1542080735', '1', '【使用人】字段由<空>变更为“白俊生3.0', '1', null, null, null), ('4', '1', '1542080735', '1', '【使用人】字段由<空>变更为“白俊生4.0', '1', null, null, null);
COMMIT;

-- ----------------------------
--  Table structure for `eam_migrations`
-- ----------------------------
DROP TABLE IF EXISTS `eam_migrations`;
CREATE TABLE `eam_migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
--  Records of `eam_migrations`
-- ----------------------------
BEGIN;
INSERT INTO `eam_migrations` VALUES ('1', '2018_10_22_092831_create_assets_table', '1'), ('2', '2018_10_23_093412_create_category_table', '1'), ('3', '2018_11_01_031706_create_companys_table', '2'), ('4', '2018_11_01_033203_create_personnels_table', '3'), ('6', '2018_11_12_040250_create_deal_withs_table', '4'), ('8', '2018_11_13_022617_create_admins_table', '5'), ('9', '2018_11_15_032807_create_receives_table', '6'), ('10', '2018_11_15_035301_create_send_backs_table', '6'), ('11', '2018_11_21_023456_create_borrows_table', '7'), ('12', '2018_11_21_043205_create_repairs_table', '8');
COMMIT;

-- ----------------------------
--  Table structure for `eam_personnels`
-- ----------------------------
DROP TABLE IF EXISTS `eam_personnels`;
CREATE TABLE `eam_personnels` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `personnel_number` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '员工编号',
  `personnel_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '员工名称',
  `company_id` int(11) DEFAULT NULL COMMENT '公司编号',
  `department_id` int(11) DEFAULT NULL COMMENT '部门编号',
  `mobile_phone` char(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '手机',
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '邮箱',
  `status` tinyint(4) DEFAULT '1' COMMENT '在职状态0.离职 1.在职',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
--  Records of `eam_personnels`
-- ----------------------------
BEGIN;
INSERT INTO `eam_personnels` VALUES ('1', 'bai1', '白1', '1', '1', '18888888880', 'pp@qq.com', '1', null, null, null), ('2', 'bai2', '白2', '1', '1', '18888888881', 'p@qq.com', '1', null, null, null), ('3', 'yingyingying', '银银银', '1', '1', '18814188345', 'qas@qq.com', '1', '2018-11-14 04:18:20', '2018-11-14 04:18:20', null), ('4', 'huan', '胡安', '1', '1', '18814188341', 'qas@qq.com', '1', '2018-11-14 04:21:46', '2018-11-14 04:21:46', null), ('5', 'jun', '浚', '1', '1', '18814188341', 'qas@qq.com', '1', '2018-11-14 07:50:57', '2018-11-14 07:50:57', null);
COMMIT;

-- ----------------------------
--  Table structure for `eam_receives`
-- ----------------------------
DROP TABLE IF EXISTS `eam_receives`;
CREATE TABLE `eam_receives` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `borrow_number` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '领用单号',
  `borrow_user` int(11) NOT NULL,
  `borrow_date` int(11) NOT NULL,
  `use_company_id` int(11) NOT NULL,
  `use_department_id` int(11) NOT NULL DEFAULT '0',
  `address_type_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `expect_revert_date` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0' COMMENT '预计退库日期',
  `borrow_operator` int(11) NOT NULL DEFAULT '1',
  `comment` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `select_assets` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `status` int(11) NOT NULL DEFAULT '0',
  `signature_images` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
--  Records of `eam_receives`
-- ----------------------------
BEGIN;
INSERT INTO `eam_receives` VALUES ('1', 'LY123456', '1', '20181116', '1', '1', '1', '本地', '20181116', '1', '领用了新资产', '1,2,3', '1', 'https://ss2.baidu.com/6ONYsjip0QIZ8tyhnq/it/u=1877045261,201149284&fm=173&app=49&f=JPEG?w=640&h=427&s=6BB21CC15E4102CC448584100300D093', '2018-11-16 07:18:31', '2018-11-16 07:18:31', null), ('2', 'LY123456', '1', '20181116', '1', '1', '1', '本地', '20181116', '1', '领用了新资产', '1,2,3', '1', 'https://ss2.baidu.com/6ONYsjip0QIZ8tyhnq/it/u=1877045261,201149284&fm=173&app=49&f=JPEG?w=640&h=427&s=6BB21CC15E4102CC448584100300D093', '2018-11-16 07:19:49', '2018-11-16 07:19:49', null), ('3', 'LY123456', '1', '20181116', '1', '1', '1', '本地', '20181116', '1', '领用了新资产', '1,2,3', '1', 'https://ss2.baidu.com/6ONYsjip0QIZ8tyhnq/it/u=1877045261,201149284&fm=173&app=49&f=JPEG?w=640&h=427&s=6BB21CC15E4102CC448584100300D093', '2018-11-16 07:19:50', '2018-11-16 07:19:50', null), ('4', 'LY123456', '1', '20181116', '1', '1', '1', '本地', '20181116', '1', '领用了新资产', '1,2,3', '1', 'https://ss2.baidu.com/6ONYsjip0QIZ8tyhnq/it/u=1877045261,201149284&fm=173&app=49&f=JPEG?w=640&h=427&s=6BB21CC15E4102CC448584100300D093', '2018-11-16 07:19:51', '2018-11-16 07:19:51', null), ('5', 'LY123456', '1', '20181116', '1', '1', '1', '本地', '20181116', '1', '领用了新资产', '1,2,3', '1', 'https://ss2.baidu.com/6ONYsjip0QIZ8tyhnq/it/u=1877045261,201149284&fm=173&app=49&f=JPEG?w=640&h=427&s=6BB21CC15E4102CC448584100300D093', '2018-11-16 07:19:52', '2018-11-16 07:19:52', null), ('6', 'LY123456', '1', '20181116', '1', '1', '1', '本地', '20181116', '1', '领用了新资产', '1,2,3', '1', 'https://ss2.baidu.com/6ONYsjip0QIZ8tyhnq/it/u=1877045261,201149284&fm=173&app=49&f=JPEG?w=640&h=427&s=6BB21CC15E4102CC448584100300D093', '2018-11-16 07:19:53', '2018-11-16 07:19:53', null), ('7', 'LY123456', '1', '20181116', '1', '1', '1', '本地', '20181116', '1', '领用了新资产', '1,2,3', '1', 'https://ss2.baidu.com/6ONYsjip0QIZ8tyhnq/it/u=1877045261,201149284&fm=173&app=49&f=JPEG?w=640&h=427&s=6BB21CC15E4102CC448584100300D093', '2018-11-16 07:19:54', '2018-11-16 07:19:54', null), ('8', 'LY123456', '1', '20181116', '1', '1', '1', '本地', '20181116', '1', '领用了新资产', '1,2,3', '1', 'https://ss2.baidu.com/6ONYsjip0QIZ8tyhnq/it/u=1877045261,201149284&fm=173&app=49&f=JPEG?w=640&h=427&s=6BB21CC15E4102CC448584100300D093', '2018-11-16 07:19:55', '2018-11-16 07:19:55', null);
COMMIT;

-- ----------------------------
--  Table structure for `eam_repairs`
-- ----------------------------
DROP TABLE IF EXISTS `eam_repairs`;
CREATE TABLE `eam_repairs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `repair_number` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `money` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `repair_man` int(11) NOT NULL DEFAULT '1',
  `status` int(11) NOT NULL DEFAULT '0',
  `borrow_operator` int(11) NOT NULL,
  `business_creation_date` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `commit` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `select_assets` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `business_update_date` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `business_end_date` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
--  Records of `eam_repairs`
-- ----------------------------
BEGIN;
INSERT INTO `eam_repairs` VALUES ('1', 'WX123213', '资产坏了', '100', '1', '0', '1', '2018-10-11', '资产坏了先报修', '1,2,3', '2018-11-22 01:54:50', '2018-11-22 01:54:50', null, null, null), ('2', 'WX123e13', '资产坏了', '100', '1', '0', '1', '2018-10-11', '资产坏了先报修', '1,2,3', '2018-11-22 02:06:13', '2018-11-22 02:06:13', null, null, null), ('3', 'WX123ee3', '资产坏了', '100', '1', '0', '1', '2018-10-11', '资产坏了先报修', '1,2,3', '2018-11-22 02:07:00', '2018-11-22 02:07:00', null, null, null), ('4', 'WX1232ee3', '资产坏了啊啊啊啊', '100', '1', '0', '1', '2018-10-11', '资产坏了先报修', '1,2,3', '2018-11-22 02:18:38', '2018-11-22 02:18:38', null, null, null), ('5', 'WX1232e3', '资产坏了啊啊啊啊', '100', '1', '0', '1', '2018-10-11', '资产坏了先报修', '1,2,3', '2018-11-22 02:20:00', '2018-11-22 02:20:00', null, null, null);
COMMIT;

-- ----------------------------
--  Table structure for `eam_send_backs`
-- ----------------------------
DROP TABLE IF EXISTS `eam_send_backs`;
CREATE TABLE `eam_send_backs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `borrow_number` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `actual_revert_date` int(11) NOT NULL,
  `use_company_id` int(11) NOT NULL,
  `address_type_id` int(11) DEFAULT NULL,
  `address` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `borrow_operator` int(11) NOT NULL,
  `comment` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `signature_images` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT '0',
  `status` int(11) DEFAULT '0',
  `select_assets` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
--  Records of `eam_send_backs`
-- ----------------------------
BEGIN;
INSERT INTO `eam_send_backs` VALUES ('1', 'TK123213', '20181123', '1', '1', '本地', '1', '退还', 'http://1.jpg', '2', '1,2', null, '2018-11-20 08:46:10', null), ('2', 'Tk23213213', '20181123', '1', '1', '本地', '1', '退还物品', '0', '0', '1,2', '2018-11-20 06:54:21', '2018-11-20 06:54:21', null), ('3', 'Tk2321321', '20181123', '1', '1', '本地', '1', '退还物品', '0', '0', '1,2', '2018-11-20 06:54:24', '2018-11-20 06:54:24', null);
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
