<?php
/**
 * Users: Administrator
 * Date: 2019/01/19
 * Time: 17:00
 */

if (!function_exists('export_client_xls')) {
    /**
     * 客户数据导出xls表格
     * @param $data
     * @param string $title
     * @param $tableHeader
     * @param $fields
     * @param string $save_path
     * @param string $filename
     * @author ppjun378 <ppjun378@foxmail.com>
     */
    function export_client_xls($data, $title = '表单', $tableHeader, $fields, $save_path = '', $filename = 'client form.xls')
    {
        set_time_limit(0);
        $arr1 = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'];
        $arr2 = array();
        for ($i = 0; $i < count($arr1); $i++) {
            for ($j = 0; $j < count($arr1); $j++) {
                $arr2[] = $arr1[$i] . $arr1[$j];
            }
        }
        $arr = array_merge($arr1, $arr2); // 文件列数列表
        $headers = count($tableHeader); // 数据列数
        $letter = array_slice($arr, 0, $headers); // 从数组中取出一段（所用到的数据列）

        $spreadsheet = new Spreadsheet();

        foreach ($data as $key => $val) {
            $key = (string) $key;
            $sheet = $spreadsheet->createSheet()->setTitle($key);

            // 合并单元格
            $sheet->mergeCells('A1:' . $arr[$headers - 1] . '1');
            $sheet->setCellValue('A1', $title);

            // 列 样式设置
            for ($i = 0; $i < count($letter); $i++) {
                // 加粗
                $sheet->getStyle($letter[$i] . '1')->getFont()->setBold(true);
                // 水平居中
                $sheet->getStyle($letter[$i] . '1')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                // 垂直居中
                $sheet->getStyle($letter[$i] . '1')->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
                // 加粗
                $sheet->getStyle($letter[$i] . '2')->getFont()->setBold(true);
                // 水平居中
                $sheet->getStyle($letter[$i] . '2')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                // 垂直居中
                $sheet->getStyle($letter[$i] . '2')->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
                //设置单元格的值
                $sheet->setCellValue($letter[$i] . '2', $tableHeader[$i]);
                if (!in_array($fields[$i], ['id', 'name'])) {
                    // 设置列的宽度
                    $sheet->getColumnDimension($letter[$i])->setAutoSize(true);
                }
            }

            for ($i = 0; $i < count($val); $i++) {
                $sheet->setCellValue('A' . ($i + 3), ($i + 1)); // 序号
                for ($j = 1; $j < $headers; $j++) {
                    $field = $fields[$j];
                    // 数据写入
                    $sheet->setCellValue($arr[$j] . ($i + 3), $val[$i]->$field);
                    // sheet->setCellValueExplicit($arr[$j].($i+3),$data[$i][$field], PHPExcel_Cell_DataType::TYPE_STRING);
                }
            }
        }

        $writer = new Xlsx($spreadsheet);
        if ($save_path) { // 保存本地
            $save_dir = dirname($save_path); // 目录
            // $save_file = basename($save_path); // 文件
            if (!is_dir($save_dir)) {
                create_folders($save_dir);
            }

            $writer->save($save_path);
        } else { // 直接下载
            header('Content-Type: application/vnd.ms-excel');
            header("Content-Disposition: attachment;filename=$filename");
            header('Cache-Control: max-age=0');
            header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
            header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
            header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
            header('Pragma: public'); // HTTP/1.0
            $writer->save('php://output');
        }
    }
}
