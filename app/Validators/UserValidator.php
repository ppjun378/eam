<?php
/**
 * User: ppjun378
 * Date: 20190327
 * Time: 14:00
 */
namespace App\Validators;

use Illuminate\Support\Facades\Validator;

class UserValidator extends Validator
{
    /**
     * 注册验证输入
     * @param $input
     * @return \Illuminate\Contracts\Validation
     * @author ppjun378 <ppjun378@foxmail.com>
     */
    public static function regis($input)
    {
        $rules = [
            'account' => 'required',
            'password' => 'required|alpha_num|between:6,18',
        ];

        $messages = [
            'account.required' => '账号必须',
            'password.required' => '密码必须',
            'password.alpha_num' => '密码必须是字母或数',
            'password.between' => '密码必须在6到18位之间',
        ];

        return Validator::make($input, $rules, $messages);
    }

    /**
     * 修改密码验证
     * @param $input
     * @return \Illuminate\Contracts\Validation\Validator
     */
    public static function modifyPass($input)
    {
        $rules = [
            'oldpass' => 'required',
            'newpass' => 'required|between:6,18',
            'repass' => 'required|same:newpass',
        ];

        $messages = [
            'oldpass.required' => '原密码必须',
            'newpass.required' => '新密码必须',
            'newpass.between' => '密码必须在6到18位之间',
            'repass.required' => '二次密码必须',
            'repass.same' => '两次密码不一致',
        ];

        return Validator::make($input, $rules, $messages);
    }

    /**
     * 登陆验证输入
     * @param $input
     * @return \Illuminate\Contracts\Validation\Validator
     * @author ppjun378 <ppjun378@foxmail.com>
     */
    public static function login($input)
    {
        $rules = [
            'account' => 'required',
            'password' => 'required|between:6,18',
            'luotest_response' => 'required',
        ];
        $messages = [
            'account.required' => '账号必须',
            'passoword.required' => '密码必须',
            'passoword.between' => '密码必须在6到18位之间',
            'repass.required' => '人机验证必须',

        ];

        return Validator::make($input, $rules, $messages);
    }
}
