<?php
/**
 * User: ppjun378
 * Data: 2018/11/14
 * Time: 16:30
 */
namespace App\Service;

use App\Model\Category;

class CategoryService
{
    /**
     * 获取列表数据
     * @param $request
     * @return mixed
     * @author ppjun378 <ppjun378@foxmail.com>
     */
    public static function dataList($request)
    {
        $keyword = $request->input('keyword', ''); // 关键词
        $parent_id = $request->input('parent_id', 0); // 父类编号

        $fields = ['id', 'name', 'pid', 'status'];
        $data = Category::select($fields)
            ->where('pid', $parent_id)
            ->where(function ($query) use ($keyword) {
                if ($keyword) {
                    $query->whereRaw('concat(`name`) like ' . '\'%' . $keyword . '%\'');
                }
            })
            ->orderBy('id', 'desc')
            ->get();

        $list['code'] = 0;
        $list['msg'] = "查询成功";
        $list['data'] = json_decode($data, true);
        $list['count'] = count($list['data']);
        return $list;

    }

    /**
     * 获取详情
     * @param $id
     * @return mixed
     * @author ppjun378 <ppjun378@foxmail.com>
     */
    public static function getDetail($id)
    {
        $field = '*';
        $data = Category::select($field)
            ->where('id', $id)
            ->first();
        return $data;
    }

    /**
     * 添加/更新
     * @param $request
     * @return mixed
     * @author ppjun378 <ppjun378@foxmail.com>
     */
    public static function store($request)
    {
        $data = $request->all();
        $id = $request->input('id', '');
        $map = [];
        $fields = [];
        foreach ($data as $k => $v) {
            if ($v === null) {$v = '';}
            if (in_array($k, $fields) && !empty($v)) {
                $map[$k] = $v;
            }
        }

        $data_exist = Category::where('id', $id)->first();
        if ($id && $data_exist) {
            $result = Category::where('id', $id)->update($map);
        } else {
            $result = Category::create($map);
        }
        return $result;

    }

    /**
     * 更改
     * @param $request
     * @return bool
     * @author ppjune378 <ppjun378@foxmail.com>
     */
    public static function save($request)
    {
        $id = $request->input('id', '');
        $field = $request->input('field', '');
        $value = $request->input('value', '');
        if ($field) {
            $map[$field] = $value;
            $request = Category::where('id', $id)->update($map);
            if ($request) {
                return true;
            } else {
                return false;
            }
        }
    }

    /**
     * 删除
     * @param $request
     * @param mixed $name
     * @author ppjun378 <ppjun378@foxmail.com>
     */
    public static function destory($request)
    {
        $id = $request->input('id', '');
        $res = Category::destory($id);
        if ($res > 0) {
            return $res;
        } else {
            return $false;
        }
    }

    /**
     * 恢复删除
     * @param $request
     * @return mixed
     * @author ppjun378 <ppjun378@foxmail.com>
     */
    public static function restore($request)
    {
        $id = $request->input('id', '');
        $res = Category::where('id', $id)->restore();
        return $res;
    }

}
