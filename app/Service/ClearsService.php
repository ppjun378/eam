<?php
/**
 * User: ppjun378
 * Data: 2018/11/15
 * Time: 14:30
 */

namespace App\Service;

use App\Model\Assets;
use App\Model\Clears;

class ClearsService
{
    /**
     * 获取列表数据
     * @param $request
     * @return mixed
     * @author ppjun378 <ppjun378@foxmail.com>
     */
    public static function dataList($request)
    {
        $keyword = $request->input('keyword', '');
        $size = $request->input('limit', 10); // 每页显示的数据条数
        $fields = ['id','clear_number','borrow_operator','clear_date', 'content', 'select_assets'];
        $data = Clears::select($fields)
            ->where('status',1)
            ->where(function ($query) use ($keyword) {
                if ($keyword) {
                    $query->whereRaw('concat(`clear_number`) like ' . '\'%' . $keyword . '%\'');
                }
            })
            ->orderBy('id', 'desc')
            ->paginate($size);

        $list['code'] = 0;
        $list['msg'] = "查询成功";
        $list['count'] = count($data);
        $data = $data->toArray();

        //计算资产个数
        foreach ($data['data'] as $k => $v) {
            $data['data'][$k]['assets_count'] = count(explode(',', $v['select_assets']));
            unset($data['data'][$k]['select_assets']);
        }

        $list['data'] = $data['data'];

        return $list;
    }

    /**
     * 详情
     * @param $id
     * @return mixed
     * @author ppjun378 <ppjun378@foxmail.com>
     */
    public static function getDetail($request)
    {
        $id = $request->input('id', '');
        $id = 1;
        $field = ['clear_number', 'content', 'clears.status', 'select_assets', 'clear_date', 'borrow_operator'];
        $data = Clears::select($field)
            ->where('clears.id', $id)
            ->first();
        $data = $data->toArray();

        $field = ['name', 'coding', 'specification', 'sn', 'image as asset_image'];
        $data['select_assets'] = Assets::select($field)
            ->whereIn('id', explode(',', $data['select_assets']))
            ->get();

        $list['code'] = 0;
        $list['msg'] = "查询成功";
        $list['data'] = $data;

        return $list;
    }

    /**
     * 添加/更新
     * @param $request
     * @return mixed
     * @author ppjun378 <ppjun378@foxmail.com>
     */
    public static function store($request)
    {
        $data = $request->all();
        $id = $request->input('id', '');
        $map = [];
        $fields = [
            'clear_number', // 报废单号
            'content', //清理说明
            'borrow_operator', //清理人
            'clear_date', //清理日期
            'select_assets', //报废资产
            'status', //报废状态
        ];
        foreach ($data as $k => $v) {
            if ($v === null) {$v = '';}
            if (in_array($k, $fields) && !empty($v)) {
                $map[$k] = $v;
            }
        }

        $data_exist = Clears::where('id', $id)->first();
        if ($id && $data_exist) {
            $result = Clears::where('id', $id)->update($map);
        } else {
            $result = Clears::create($map);
        }
    }

    /**
     * 更改
     * @param $request
     * @return bool
     * @author ppjun378 <ppjun378@foxmail.com>
     */
    public static function save($request)
    {
        $id = $request->input('id', '');
        $field = $request->input('field', '');
        $value = $request->input('value', '');
        if ($field) {
            $map[$field] = $value;
            $request = Clears::where('id', $id)->update($map);
            if ($request) {
                return true;
            } else {
                return false;
            }
        }
    }

    /**
     * 删除
     * @param $request
     * @return bool\int
     * @author ppjun378 <ppjun378@foxmail.com>
     */
    public static function destory($request)
    {
        $id = $request->input('id', '');
        $res = Clears::destory($id);
        if ($res > 0) {
            return $res;
        } else {
            return false;
        }
    }

    /**
     * 恢复删除
     * @param $request
     * @return mixed
     * @author ppjun378 <ppjun378@foxmail.com>
     */
    public static function restore($request)
    {
        $id = $request->input('id', '');
        $res = Clears::where('id', $id)->restore();
        return $res;
    }

}
