<?php

/**
 * User: ppjun378
 * Data: 2018/11/13
 * Time: 18:00
 */

namespace App\Service;

use App\Model\Companys;
use App\Model\Personnels;

class PersonnelsService
{
    /**
     * 获取资产列表数据
     * @param $request
     * @return mixed
     * @author ppjun378 <ppjun378@foxmail.com>
     */
    public static function dataList($request)
    {
        $keyword = $request->input('keyword', '');

        $size = $request->input('pagenum', 5); // 每页显示的数据条数
        $page = $request->input('page', 1);
        $fields = ['personnels.id', 'personnel_number', 'personnel_name', 'companys.company_name', 'department_id', 'mobile_phone', 'email', 'status'];

        $data = Personnels::select($fields)
            ->join('companys', 'companys.id', '=', 'personnels.company_id')
            ->where(function ($query) use ($keyword) {
                // $query->orWhere('user_name', 'like', '%' . $keyword . '%');
                // ->orWhere('coding', 'like', '%' . $keyword . '%');
                if ($keyword) {
                    $query->whereRaw('concat(`personnel_number`,`personnel_name`,`mobile_phone`,`email`) like ' . '\'%' . $keyword . '%\'');
                }

            })
            ->orderBy('id', 'desc')

            ->paginate($size);
        // dump(DB::getQueryLog());
        $list['code'] = 0;
        $list['msg'] = "查询成功";
        $list['count'] = count($data);
        $data = $data->toArray();

        $l = array();
        $m = array();
        foreach ($data['data'] as $key => $val) {

            if (!empty($val['department_id'])) {
                // 查询使用公司名称
                $l = Companys::select('company_name as department_name')
                    ->where('id', '=', $val['department_id'])
                    ->first()
                    ->toArray();

            } else {
                $l['department_name'] = null;
            }
            unset($data['data'][$key]['department_id']);
            $data['data'][$key]['department_name'] = $l['department_name'];

        }

        $list['data'] = $data['data'];

        return $list;

    }

    /**
     * 获取资产详情
     * @param $id
     * @return mixed
     * @author ppjun378 <ppjun378@foxmail.com>
     */
    public static function getDetail($request)
    {
        $company_list = Companys::select(['id', 'company_name'])
            ->where('parent_id', 0)
            ->get()
            ->toArray();

        $new_company_list = array();
        foreach ($company_list as $key => $val) {
            $new_company_list[$val['id']] = $val['company_name'];
        }

        $id = $request->input('id', '');
        $fields = ['personnel_number', 'personnel_name', 'company_id', 'department_id', 'companys.company_name as department_name', 'mobile_phone', 'email', 'status'];
        $data = Personnels::select($fields)
            ->join('companys', 'companys.id', '=', 'department_id')
            ->where('personnels.id', $id)
            ->first();
        $data = $data->toArray();
        $data['company_name'] = $new_company_list[$data['company_id']];

        $list['code'] = 0;
        $list['msg'] = 'success';
        $list['data'] = $data;
        return $list;
    }

    /**
     * 添加/更新 资产
     * @param $request
     * @return mixed
     * @author ppjun378 <ppjun378@foxmail.com>
     */
    public static function store($request)
    {
        $data = $request->all();
        $id = $request->input('id', '');
        $map = [];
        $fields = ['id', 'personnel_number', 'personnel_name', 'company_id', 'department_id', 'mobile_phone', 'email', 'status'];
        foreach ($data as $k => $v) {
            if ($v === null) {$v = '';}
            if (in_array($k, $fields) && !empty($v)) {
                $map[$k] = $v;
            }
        }

        $data_exist = Personnels::where('id', $id)->first();
        if ($id && $data_exist) {
            $result = Personnels::where('id', $id)->update($map);
        } else {
            $result = Personnels::create($map);
        }
        return $result;
    }

    /**
     * 更改
     * @param $request
     * @return bool
     * @author ppjun378 <ppjun378@foxmail.com>
     */
    public static function save($request)
    {
        $id = $request->input('id', '');
        $field = $request->input('field', '');
        $value = $request->input('value', '');
        if (field) {
            $map[$field] = $value;
            $request = Personnels::where('id', $id)->update($map);
            if ($request) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    /**
     * 删除
     * @param $request
     * @return bool|int
     * @author ppjun378 <ppjun378@foxmail.com>
     */
    public static function destory($request)
    {
        $id = $request->input('id', '');
        $res = Personnels::destory($id);
        if ($res > 0) {
            return $res;
        } else {
            return false;
        }
    }

    /**
     * 恢复删除
     * @param $request
     * @return mixed
     * @author ppjun378 <ppjun378@foxmail.com>
     */
    public static function restore($request)
    {
        $id = $request->input('id', '');
        $res = Personnels::where('id', $id)->restore();
        return $res;
    }

}
