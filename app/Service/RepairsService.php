<?php
/**
 * User: ppjun378
 * Data: 2018/11/15
 * Time: 14:30
 */

namespace App\Service;

use App\Model\Assets;
use App\Model\Repairs;

class RepairsService
{
    /**
     * 获取列表数据
     * @param $request
     * @return mixed
     * @author ppjun378 <ppjun378@foxmail.com>
     */
    public static function dataList($request)
    {
        $keyword = $request->input('keyword', '');
        $size = $request->input('limit', 10); // 每页显示的数据条数
        $fields = ['repair_number', 'personnels.personnel_name as repair_man', 'repairs.status', 'business_creation_date', 'business_update_date', 'business_end_date', 'content', 'select_assets'];
        $data = Repairs::select($fields)
            ->join('personnels', 'personnels.id', '=', 'repairs.repair_man')
            ->where(function ($query) use ($keyword) {
                if ($keyword) {
                    $query->whereRaw('concat(`repair_number`) like ' . '\'%' . $keyword . '%\'');
                }
            })
            ->orderBy('repairs.id', 'desc')
            ->paginate($size);

        $list['code'] = 0;
        $list['msg'] = "查询成功";
        $list['count'] = count($data);
        $data = $data->toArray();

        //计算资产个数
        foreach ($data['data'] as $k => $v) {
            $data['data'][$k]['assets_count'] = count(explode(',', $v['select_assets']));
            unset($data['data'][$k]['select_assets']);
        }

        $list['data'] = $data['data'];

        return $list;
    }

    /**
     * 详情
     * @param $id
     * @return mixed
     * @author ppjun378 <ppjun378@foxmail.com>
     */
    public static function getDetail($request)
    {
        $id = $request->input('id', '');
        $field = ['repair_number', 'content', 'money', 'business_creation_date', 'commit', 'repairs.status', 'select_assets', 'personnels.personnel_name as repair_man', 'borrow_operator'];
        $data = Repairs::select($field)
            ->join('personnels', 'personnels.id', '=', 'repairs.repair_man')
            ->where('repairs.id', $id)
            ->first();
        $data = $data->toArray();

        $field = ['name', 'coding', 'specification', 'sn', 'image as asset_image'];
        $data['select_assets'] = Assets::select($field)
            ->whereIn('id', explode(',', $data['select_assets']))
            ->get();

        $list['code'] = 0;
        $list['msg'] = "查询成功";
        $list['data'] = $data;

        return $list;
    }

    /**
     * 添加/更新
     * @param $request
     * @return mixed
     * @author ppjun378 <ppjun378@foxmail.com>
     */
    public static function store($request)
    {
        $data = $request->all();
        $id = $request->input('id', '');
        $map = [];
        $fields = [
            'repair_number', //维修单号
            'content', //维修内容
            'money', //维修话费
            'repair_man', //报修人
            'status', //维修状态
            'borrow_operator', //处理人
            'business_creation_date', //业务创建日期
            'commit', //备注信息
            'select_assets', //维修资产
        ];
        foreach ($data as $k => $v) {
            if ($v === null) {$v = '';}
            if (in_array($k, $fields) && !empty($v)) {
                $map[$k] = $v;
            }
        }

        $data_exist = Repairs::where('id', $id)->first();
        if ($id && $data_exist) {
            $result = Repairs::where('id', $id)->update($map);
        } else {
            $result = Repairs::create($map);
        }
    }

    /**
     * 更改
     * @param $request
     * @return bool
     * @author ppjun378 <ppjun378@foxmail.com>
     */
    public static function save($request)
    {
        $id = $request->input('id', '');
        $field = $request->input('field', '');
        $value = $request->input('value', '');
        if ($field) {
            $map[$field] = $value;
            $request = Repairs::where('id', $id)->update($map);
            if ($request) {
                return true;
            } else {
                return false;
            }
        }
    }

    /**
     * 删除
     * @param $request
     * @return bool\int
     * @author ppjun378 <ppjun378@foxmail.com>
     */
    public static function destory($request)
    {
        $id = $request->input('id', '');
        $res = Repairs::destory($id);
        if ($res > 0) {
            return $res;
        } else {
            return false;
        }
    }

    /**
     * 恢复删除
     * @param $request
     * @return mixed
     * @author ppjun378 <ppjun378@foxmail.com>
     */
    public static function restore($request)
    {
        $id = $request->input('id', '');
        $res = Repairs::where('id', $id)->restore();
        return $res;
    }

}
