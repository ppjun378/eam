<?php
/**
 * User: ppjun378
 * Data: 2018/12/25
 * Time: 12:00
 */

namespace App\Service;

class AssetsAttrsService
{
    /**
     * 获取列表数据
     * @param $request
     * @return mixed
     * @author ppjun378 <ppjun378@foxmail.com>
     */
    public static function dataList($request)
    {
        $keyword = $request->input('keyword', '');
        $size = $request->input('limit', 10); // 每页显示的数据条数
        $status = (int) $request->input('status', '');
        $fields = ['id', 'AssetsAttrs_number', 'AssetsAttrs_name', 'status'];
        $data = AssetsAttrs::select($fields)
            ->when($keyword, function ($query) use ($keyword) {
                $query->orWhere('AssetsAttrs_number', 'like', '%' . $keyword . '%');
                $query->orWhere('AssetsAttrs_name', 'like', '%' . $keyword . '%');
            })
            ->orderBy('id', 'desc')
            ->paginate($size);

        $list['code'] = 0;
        $list['msg'] = '查询成功';
        $list['count'] = count($data);
        $data = $data->toArray();
        $list['data'] = $data['data'];
        return $list;

    }

    /**
     * 详情
     * @param $id
     * @return mixed
     * @author ppjun378 <ppjun378@foxmail.com>
     */
    public static function getDetail($request)
    {
        $id = $request->input('id', '');
        $field = ['AssetsAttrs_number', 'AssetsAttrs_name', 'status'];
        $data = AssetsAttrs::select($field)
            ->where('id', $id)
            ->first();
        $data = $data->toArray();

        $list['code'] = 0;
        $list['msg'] = "查询成功";
        $list['data'] = $data;

        return $list;

    }

    /**
     * 添加/更新
     * @param $request
     * @return mixed
     * @author ppjun378 <ppjun378@foxmail.com>
     */
    public static function store($request)
    {
        $data = $request->all();
        $id = $request->input('id', '');
        $map = [];
        $fields = [
            'AssetsAttrs_number', // 区域编码
            'AssetsAttrs_name', // 区域名称
            'status', // 状态
        ];
        foreach ($data as $k => $v) {
            if ($v === null) {$v = '';}
            if (in_array($k, $fields) && !empty($v)) {
                $map[$k] = $v;
            }
        }

        $data_exist = AssetsAttrs::where('id', $id)->first();
        if ($id && $data_exist) {
            $result = AssetsAttrs::where('id', $id)->update($map);
            $list['code'] = 0;
            $list['msg'] = '修改成功';
        } else {
            $result = AssetsAttrs::create($map);
            $id = $result->id;

            $list['code'] = 0;
            $list['msg'] = "success";
            $list['data'] = array('id' => $id);
            return $list;
        }

    }

    /**
     * 更改
     * @param $request
     * @return bool
     * @author ppjun378 <ppjun378@foxmail.com>
     */
    public static function save($request)
    {
        $id = $request->input('id', '');
        $field = $request->input('field', '');
        $value = $request->input('value', '');
        if ($field) {
            $map[$field] = $value;
            $request = AssetsAttrs::where('id', $id)->update($map);
            if ($request) {
                return true;
            } else {
                return false;
            }
        }
    }

    /**
     * 删除
     * @param $request
     * @return bool\int
     * @author ppjun378 <ppjun378@foxmail.com>
     */
    public static function destory($request)
    {
        $id = $request->input('id', '');
        $res = AssetsAttrs::destory($id);
        if ($res > 0) {
            return $res;
        } else {
            return false;
        }
    }

    /**
     * 恢复删除
     * @param $request
     * @return mixed
     * @author ppjun378 <ppjun378@foxmail.com>
     */
    public static function restore($request)
    {
        $id = $request->input('id', '');
        $res = AssetsAttrs::where('id', $id)->restore();
        return $res;
    }

}
