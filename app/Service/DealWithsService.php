<?php
/**
 * Users: ppjun378
 * Data: 2018/11/12
 * Time: 14:00
 */
namespace App\Service;

use App\Model\DealWiths;

class DealWithsService
{
    /**
     * 获取资产列表数据
     * @param $request
     * @return  $mixed
     * @author ppjun378 <ppjun378@foxmail.com>
     */

    public static function dataList($request)
    {
        // 资产ID
        $asset_id = (int) $request->input('asset_id', '');
        $asset_id = 1;

        $fields = ['admins.name as deal_with_man', 'deal_with_status', 'deal_with_date', 'deal_with_content'];
        $size = $request->input('limit', 10); //每页显示的数据条数
        $data = DealWiths::select($fields)
            ->join('admins', 'admins.id', '=', 'deal_withs.deal_with_man_id')
            ->where('asset_id', $asset_id)
            ->orderBy('deal_withs.id', 'desc')
            ->paginate($size);

        $list['code'] = 0;
        $list['msg'] = '查询成功';
        $list['count'] = count($data);
        $data = $data->toArray();
        $list['data'] = $data['data'];

        return $list;
    }

    /**
     * 获取详情
     * @param $id
     * @return mixed
     * @author ppjun378 <ppjun378@foxmail.com>
     */
    public static function getDatail($id)
    {
        $field = '*';
        $data = DealWiths::select($field)
            ->where('id', $id)
            ->first();
        return $data;

    }

    /**
     * 添加/更新
     * @param $request
     * @return mixed
     * @author ppjun378 <ppjun378@foxmail.com>
     */
    public static function store($request)
    {
        $data = $request->all();
        $id = $request->input('id', '');
        $map = [];
        $fields = [];
        foreach ($data as $k => $v) {
            if ($v === null) {$v = '';}
            if (in_array($k, $fields) && !empty($v)) {
                $map[$k] = $v;
            }
        }

        $data_exist = DealWiths::where('id', $id)->first();
        if ($id && $data_exist) {
            $result = DealWiths::where('id', $id)->update($map);
        } else {
            $result = DealWiths::create($map);
        }
        return $result;

    }

    /**
     * 更改
     * @param $request
     * $return bool
     * @author ppjun378 <ppjun378@foxmail.com>
     */
    public static function save($request)
    {
        $id = $request->input('id', '');
        $field = $request->input('field', '');
        $value = $request->input('value', '');
        if ($field) {
            $map[$field] = $value;
            $request = DealWiths::where('id', $id)->update($map);
            if ($request) {
                return true;
            } else {
                return false;
            }
        }
    }

    /**
     * 删除
     * @param $request
     * @return mixed
     * @author ppjun378 <ppjun378@foxmail.com>
     */
    public static function destory($request)
    {
        $id = $request->input('id', '');
        $res = DealWiths::destory($id);
        if ($res > 0) {
            return $res;
        } else {
            return false;
        }
    }

    /**
     * 恢复删除
     * @param $request
     * @return mixed
     * @author ppjun378 <ppjun378@foxmail.com>
     */
    public static function restore($request)
    {
        $id = $request->input('id', '');
        $res = DealWiths::where('id', $id)->restore();
        return $res;
    }

}
