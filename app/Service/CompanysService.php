<?php
/**
 * User: ppjun378
 * Data: 2018/10/23
 * Time: 15:00
 */
namespace app\Service;

use App\Model\Companys;

class CompanysService
{
    /**
     * 获取列表数据
     * @param $request
     * @return mixed
     * @author ppjun378 <ppjun378@foxmail.com>
     */
    public static function dataList($request)
    {
        $keyword = $request->input('keyword', ''); // 关键词
        $parent_id = $request->input('parent_id', 0); // 父类编号

        $fields = ['id', 'company_number', 'company_name'];
        $data = Companys::select($fields)
            ->where('parent_id', $parent_id)
            ->where(function ($query) use ($keyword) {
                if ($keyword) {
                    $query->whereRaw('concat(`company_number`,`company_name`) like ' . '\'%' . $keyword . '%\'');
                }
            })
            ->orderBy('id', 'desc')
            ->get();

        $list['code'] = 0;
        $list['msg'] = "查询成功";
        $list['data'] = json_decode($data, true);
        $list['count'] = count($list['data']);
        return $list;
    }

    /**
     * 获取资产详情
     * @param $id
     * @return mixed
     * @author ppjun378 <ppjun378@foxmail.com>
     */
    public static function getDetail($id)
    {
        $field = '*';
        $data = Companys::select($field)
            ->where('id', $id)
            ->first();
        return $data;
    }

    /**
     * 添加/更新
     * @param $request
     * @return mixed
     * @author ppjun378 <ppjun378@foxmail.com>
     */
    public static function store($request)
    {
        $data = $request->all();
        $id = $request->input('id', '');
        $map = [];
        $fields = ['id'];
        foreach ($data as $k => $v) {
            if ($v == null) {$v = '';}
            if (in_array($k, $fields) && !empty($v)) {
                $map[$k] = $v;
            }
        }

        $data_exist = Companys::where('id', $id)->first();
        if ($id && $data_exist) {
            $result = Companys::where('id', $id)->update($map);
        } else {
            $result = Companys::create($map);
        }
        return $result;
    }

    /**
     * 更改
     * @param $request
     * @return bool
     * @author ppjun378 <ppjun378@foxmail.com>
     */
    public static function save($request)
    {
        $id = $request->input('id', '');
        $field = $request->input('field', '');
        $value = $request->input('value', '');
        if (field) {
            $map[$field] = $value;
            $request = Companys::where('id', $id)->update($map);
            if ($request) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    /**
     * 删除
     * @param $request
     * @return mixed
     * @author ppjun378 <ppjun378@foxmail.com>
     */
    public static function destory($request)
    {
        $id = $request->input('id', '');
        $res = Companys::destory($id);
        if ($res > 0) {
            return $res;
        } else {
            return false;
        }
    }

    /**
     * 恢复删除
     * @param $request
     * @return mixed
     * @author ppjun378 <ppjun378@foxmail.com>
     */
    public static function restore($request)
    {
        $id = $request->input('id', '');
        $res = Companys::where('id', $id)->restore();
        return $res;
    }

    /**
     * 批量录入公司部门信息
     *
     */
    public static function curl()
    {

        $res = '[{"Code":"01","Name":"盈富永泰集团","NewId":"ab2dd760-55ef-4073-a29e-3e04e98c1cf5","CompanyId":36419,"CompanyName":"盈富永泰集团","DepartId":null,"Pid":"","HasChildren":true,"IsCompany":true,"IsDisabled":false},{"Code":"10","Name":"离职停职","NewId":"3f70b7b5-15b5-418d-a2c7-00d6879d7018","CompanyId":36419,"CompanyName":"盈富永泰集团","DepartId":186936,"Pid":"ab2dd760-55ef-4073-a29e-3e04e98c1cf5","HasChildren":false,"IsCompany":false,"IsDisabled":false},{"Code":"11","Name":"顾问部","NewId":"a3f5e271-50bd-45d1-a5d5-c6294930c5a6","CompanyId":36419,"CompanyName":"盈富永泰集团","DepartId":186937,"Pid":"ab2dd760-55ef-4073-a29e-3e04e98c1cf5","HasChildren":false,"IsCompany":false,"IsDisabled":false},{"Code":"123","Name":"技术部","NewId":"b17498f0-d53a-4573-9e53-0092811fada8","CompanyId":36419,"CompanyName":"盈富永泰集团","DepartId":181841,"Pid":"ab2dd760-55ef-4073-a29e-3e04e98c1cf5","HasChildren":true,"IsCompany":false,"IsDisabled":true},{"Code":"2","Name":"网络部","NewId":"09f78ada-4717-477b-b984-96ff3c0d38f3","CompanyId":36419,"CompanyName":"盈富永泰集团","DepartId":186929,"Pid":"ab2dd760-55ef-4073-a29e-3e04e98c1cf5","HasChildren":false,"IsCompany":false,"IsDisabled":false},{"Code":"3","Name":"行政部","NewId":"0cc26ce8-8978-4584-940d-8168ec1ecd8a","CompanyId":36419,"CompanyName":"盈富永泰集团","DepartId":186930,"Pid":"ab2dd760-55ef-4073-a29e-3e04e98c1cf5","HasChildren":false,"IsCompany":false,"IsDisabled":false},{"Code":"4","Name":"人力资源部","NewId":"075efeff-7362-4647-8553-8aa2332c6d8d","CompanyId":36419,"CompanyName":"盈富永泰集团","DepartId":186931,"Pid":"ab2dd760-55ef-4073-a29e-3e04e98c1cf5","HasChildren":false,"IsCompany":false,"IsDisabled":false},{"Code":"456","Name":"销售部","NewId":"df17e177-b92a-4a29-957a-3d6d2c8c5a6c","CompanyId":36419,"CompanyName":"盈富永泰集团","DepartId":181842,"Pid":"ab2dd760-55ef-4073-a29e-3e04e98c1cf5","HasChildren":false,"IsCompany":false,"IsDisabled":true},{"Code":"5","Name":"财务部","NewId":"eb63f13c-8993-4041-9980-bffbffbd4646","CompanyId":36419,"CompanyName":"盈富永泰集团","DepartId":186932,"Pid":"ab2dd760-55ef-4073-a29e-3e04e98c1cf5","HasChildren":false,"IsCompany":false,"IsDisabled":false},{"Code":"6","Name":"招商部","NewId":"3d68ce30-aa78-45d6-bbd4-16dadaf51e4d","CompanyId":36419,"CompanyName":"盈富永泰集团","DepartId":186933,"Pid":"ab2dd760-55ef-4073-a29e-3e04e98c1cf5","HasChildren":false,"IsCompany":false,"IsDisabled":false},{"Code":"7","Name":"客服拓展部","NewId":"68b7e307-9863-4ad7-a664-879bc2821147","CompanyId":36419,"CompanyName":"盈富永泰集团","DepartId":186934,"Pid":"ab2dd760-55ef-4073-a29e-3e04e98c1cf5","HasChildren":false,"IsCompany":false,"IsDisabled":false},{"Code":"789","Name":"人事部","NewId":"abba7095-149a-464a-945d-cd29107accdf","CompanyId":36419,"CompanyName":"盈富永泰集团","DepartId":181843,"Pid":"ab2dd760-55ef-4073-a29e-3e04e98c1cf5","HasChildren":false,"IsCompany":false,"IsDisabled":true},{"Code":"8","Name":"董经办","NewId":"73c61c3a-f5d1-4cbf-bc5d-c828c054c1cb","CompanyId":36419,"CompanyName":"盈富永泰集团","DepartId":182306,"Pid":"ab2dd760-55ef-4073-a29e-3e04e98c1cf5","HasChildren":false,"IsCompany":false,"IsDisabled":false},{"Code":"9","Name":"董事长","NewId":"785b579d-e07c-4dc0-af3a-f7a2aa56b333","CompanyId":36419,"CompanyName":"盈富永泰集团","DepartId":186935,"Pid":"ab2dd760-55ef-4073-a29e-3e04e98c1cf5","HasChildren":false,"IsCompany":false,"IsDisabled":false},{"Code":"k","Name":"k","NewId":"083cd5ff-ef66-4b2c-84c4-a4b090710236","CompanyId":36419,"CompanyName":"盈富永泰集团","DepartId":182305,"Pid":"b17498f0-d53a-4573-9e53-0092811fada8","HasChildren":false,"IsCompany":false,"IsDisabled":true}]';
        return json_decode($res, true);
    }

    /**
     * 批量倒入数据
     *
     */
    public static function importCompany($data)
    {
        $fields = ['company_number', 'company_name', ''];
        foreach ($data as $k => $v) {
            if ($v == null) {$v = '';}
            if (in_array($k, $fields) && !empty($v)) {
                $map[$k] = $v;
            }
        }

        exit;
        $data_exist = Companys::insert($map);

    }

}




