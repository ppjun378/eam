<?php
/**
 * User: Administrator
 * Date: 2018/12/07
 * Time: 17:00
 */

namespace App\Admin\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller;

class BaseController extends Controller
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    /**
     * 成功返回json数据格式
     * @param $data
     * @param integer $code
     * @param string $message
     * @param int $status
     * @param \Illuminate\Http\JsonResponse
     * @author <ppjun378@foxmail.com>
     */
    public function data($data, $code = 0, $message = 'success', $status = 200)
    {
        return response()->json([
            'data' => $data,
            'code' => $code,
            'message' => $message,
        ], $status, [], JSON_UNESCAPED);
    }

    /**
     * 返回成功
     * @param integer $code
     * @param string $message
     * @param int $status
     * @return \Illuminate\Http\JsonResponse
     * @author <rehun@foxmail.com>
     */
    public function success($code = 0, $message = 'success', $status = 200)
    {
        return response()->json([
            'code' => $code,
            'message' => $message,
        ], $status, [], JSON_UNESCAPED_UNICODE);
    }
}
