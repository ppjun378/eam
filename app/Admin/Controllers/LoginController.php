<?php
/**
 * User: ppjun378
 * Date: 2018/9/13
 * Time: 16:47
 */
namespace App\Admin\Controllers;

// use App\Service\LoginService;
// use App\Validators\UserValidator;
use Illuminate\Http\Request;

class LoginController extends BaseController
{
    /**
     * 登录页面
     * @return \Illuminate\Contracts\Validation\Validator|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @author <ppjun378@foxmail.com>
     */
    public function index()
    {
        echo 1;exit;
        $title = '时空阁登录';
        $view = view('admin/login/index', compact('title'));
        return $view;
    }

    /**
     * 登录弹窗
     * @return \Illuminate\Contracts\Validation\Validator|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @author <ppjun378@foxmail.com>
     */
    public function popup()
    {
        $siteKey = config('sundry.luosimao.site_key');
        $view = view('admin/login/popup', compact('siteKey'));
        return $view;
    }

    /**
     * 登录
     * @param Request $request
     * @return array
     * @author <ppjun378@foxmail.com>
     */
    public function login(Request $request)
    {
        $validate = UserValidator::login($request->all());
        if ($validate->fails()) {
            return $this->error(4000, $validate->errors()->first(), 200);
        }
        $result = LoginService::login($request);
        return $result;
    }

    /**
     * 注册
     * @param Request $request
     * @return array
     * @author <ppjun378@foxmail.com>8
     */
    public function register(Request $request)
    {
        $validate = UserValidator::regis($request->all());
        if ($validate->fails()) {
            return $this->error(4000, $validate->errors()->first(), 200);
        }
        $result = LoginService::register($request);
        return $result;
    }

    /**
     * 登出
     * @param Request $request
     * @return array
     * @author <ppjun378@foxmail.com>
     */
    public function logout(Request $request)
    {
        $result = LoginService::logout($request);
        if ($result) {
            return redirect('/admin/login');
        }
    }

    /**
     * Api登录
     * @param Request $request
     * @return array
     * @author: <ppjun378@foxmail.com>
     * @Time: 2019/1/1   4:08 PM
     */
    public function apiLogin(Request $request)
    {
        return LoginService::apiLogin($request);
    }
}
