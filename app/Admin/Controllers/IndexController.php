<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/1207
 * Time: 12:00
 */

namespace App\Admin\Controllers;

class IndexController extends BaseController
{
    /**
     * 后台首页
     * @return \Illuminate\Contracts\Validation\Validator|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @author <ppjun378@foxmail.com>
     */
    public function adminIndex()
    {
        echo 'adminindex';exit;
        $title = '时空阁后台';
        $view = view('admin/index/index', compact('title'));
        return $view;
    }

    /**
     * 欢迎页面
     * @return \Illuminate\Contracts\Validation\Validator|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @author <ppjun378@foxmail.com>
     */
    public function welcome()
    {
        $title = '欢迎页';
        $view = view('admin/index/welcome', compact('title'));
        return $view;
    }
}
