<?php
/**
 * User: ppjun378
 * Date: 2018/10/24
 * Time: 14:00
 */

namespace APP\Repositories;

class AssetsRepository
{
    /**
     * 资产列表（条件分页）
     * @param $request
     * @return mixed
     * @author ppjun378 <ppjun378@foxmail.com>
     * @time: 2018/10/24
     */
    public function dateList($request)
    {
        $size = $request->input('limit', 10);
        $keyword = $request->input('keyword', '');
        $fields = ['id', 'coding', 'category_id', 'name', 'specification', 'sn', 'unit', 'money', 'use_the_company_id', 'department_id', 'purchase_date', 'user_name', 'administrator', 'company_id', 'area', 'location', 'use_period', 'supplier', 'source', 'note', 'image', 'sort'];
        $data = Assets::select($fields)
            ->where(function ($query) use ($keyword) {
                //$query->orWhere('name', 'like', '%' . $keyword . '%')
                $query->whereRaw("concat(`codding`,`name`,`sn`,`user_name`,`administrator`,`area`,`location`,`supplier`),'like' '%" . $keyword . "%'");
            })
            ->orderBy('sort', 'desc')
            ->orderBy('id', 'desc')
            ->paginate($size);
        $data = $data->toArray();
        $list['data'] = $data['data'];
        $list['count'] = $data['total'];
        $list['code'] = 0;
        $list['msg'] = '';
        return $list;
    }

    /**
     * 删除资产
     * @param $data
     * @return mixed
     * @author ppjun378 <ppjun378@foxmail.com>
     * @time: 2018/10/25
     */
    public static function destory($id)
    {
        return Assets::destory($id);
    }

    /**
     * 修改资产
     * @param $id
     * @param $data
     * @return mixed
     * @author ppjun378 <ppjun378@foxmail.com>
     * @time: 2018/10/25
     */
    public static function save($id, $data)
    {
        return Assets::where('id', $id)->update($data);
    }

    /**
     * 获取单个数据
     * @param $id
     * @return
     * @author ppjun378 <ppjun378@foxmail.com>
     * @time: 2018/10/25
     */
    public static function getFirst($id)
    {
        return Assets::where('id', $id)->orderBy('id', 'desc')->first();
    }

}
