<?php
/**
 * User: ppjun378
 * Date: 2018/11/7
 * Time: 14:00
 */

namespace App\Repositories;

class CompanysRepositories
{
    /**
     * 获取列表
     * @param $request
     * @return mixed
     * @author ppjun378 <ppjun378@foxmail.com>
     */
    public static function getlist($request)
    {
        $field = ['id', 'name'];
        $data = Companys::select($field)
            ->where('status', '1')
            ->orderBy('sort', 'desc')
            ->orderBy('id', 'desc')
            ->get();
        return $data;
    }

    /**
     * 获取列表（条件分页）
     * @param $request
     * @return mixed
     * @author ppjun378 <ppjun378@foxmail.com>
     */
    public static function deteList($request)
    {
        $size = $request->input('limit', 10);
        $fields = ['id', 'name'];
        $data = Companys::select($fields)
            ->where(function ($query) use ($keyword) {
                $query->orWhere('name', 'like', '%' . $keyword . '%');
            })
            ->orderBy('status', 'asc')
            ->orderBy('sort', 'desc')
            ->orderBy('id', 'desc')
            ->paginate($size);
        $data = $data->toArray();
        $list['data'] = $data['data'];
        $list['count'] = $data['total'];
        $list['code'] = 0;
        $list['msg'] = '';
        return $list;
    }

    /**
     * 查询名称是否存在
     * @param $name
     * @return bool
     * @author ppjun378 <ppjun378@foxmail.com>
     */
    public static function checkName($name)
    {
        $data = Companys::where('name', $name)->first();
        if ($data) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * 修改
     * @param $data
     * @return mixed
     * @author ppjun378 <ppjun378@foxmail.com>
     */
    public static function save($id, $data)
    {
        return Companys::where('id', $id)->update($data);
    }

    /**
     * 删除
     * @param mixed
     * @param $data
     * @return mixed
     * @author ppjun378 <ppjun378@foxmail.com>
     */
    public static function destroy($id)
    {
        return Companys::destory($id);
    }

    /**
     * 获取单个数据
     * @param $id
     * @return mixed
     * @author ppjun378 <ppjun378@foxmail.com>
     */
    public static function getFirst($id)
    {
        return Companys::where('id', $id)->orderBy('id', 'desc')->first();
    }

}
