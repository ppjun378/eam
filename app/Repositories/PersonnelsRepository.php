<?php
/**
 * User: ppjun378
 * Date: 2018/11/13
 * Time: 15:00
 */

namespace App\Repositories;

class PersonnelsRepository
{
    /**
     * 列表（条件分页）
     * @param $request
     * @return mixed
     * @author ppjun378 <ppjun378@foxmail.com>
     * @time 1018/11/13
     */
    public function dateList($request)
    {
        $size = $request->input('limit', 10);
        $keyword = $request->input('keyword', '');
        $fields = ['id'];
        $data = Personnels::select($fields)
            ->where(function ($query) use ($keyword) {
                //$query->orWhere('name', 'like', '%' . $keyword . '%')
                $query->whereRaw("concat(`codding`,`name`,`sn`,`user_name`,`administrator`,`area`,`location`,`supplier`),'like' '%" . $keyword . "%'");
            })
            ->orderBy('sort', 'desc')
            ->orderBy('id', 'desc')
            ->paginate($size);
        $data = $data->toArray();
        $list['data'] = $data['data'];
        $list['count'] = $data['total'];
        $list['code'] = 0;
        $list['msg'] = '';

    }

    /**
     * 删除
     * @param $id
     * @param $data
     * @return mixed
     * @author ppjun378 <email@email.com>
     */
    public static function destory($id)
    {
        return Personnels::destory($id);
    }

    /**
     * 修改资产
     * @param $id
     * @param $data
     * @return mixed
     * @author ppjun378 <ppjun378@foxmail.com>
     */
    public static function save($id, $data)
    {
        return Personnels::where('id', $id)->update($data);
    }

    /**
     * 获取单个数据
     * @param $id
     * @return
     * @author ppjun378 <ppjun378@foxmail.com>
     * @time 2018/11/11
     */
    public static function getFirst($id)
    {
        return Personnels::where('id', $id)->orderBy('id', 'desc')->first();
    }

}
