<?php
/**
 * User: ppjun378
 * Data: 2018/11/15
 * Time: 15:30
 */
namespace App\Repositories;

class SendBacksRepository
{
    /**
     * 列表（条件分页）
     * @param $request
     * @return mixed
     * @author ppjun378 <ppjun378@foxmail.com>
     */
    public function detaList($request)
    {
        $size = $request->input('limit', 10);
        $fields = ['id'];
        $data = SendBacks::select($fields)
            ->orderBy('id', 'desc')
            ->paginate($size);
        $data = $data->toArray();
        $list['data'] = $data['data'];
        $list['count'] = $data['total'];
        $list['code'] = 0;
        $list['msg'] = '';
        return $list;
    }

    /**
     * 删除
     * @param $data
     * @return mixed
     * @author ppjun378 <ppjun378@foxmail.com>
     * @time: 2018/11/15
     */
    public static function destory($id)
    {
        return SendBacks::destory($id);
    }

    /**
     * 获取单个数据
     * @param $id
     * @return mixed
     * @author ppjun378 <ppjun378@foxmail.com>
     * @time: 2018/11/15
     */
    public static function getFirst($id)
    {
        return SendBacks::where('id', $id)->orderBy('id', 'desc')->first();
    }
}
