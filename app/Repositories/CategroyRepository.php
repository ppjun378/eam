<?php
/**
 * User: ppjun378
 * Date: 2018/10/24
 * Time: 11:30
 */

namespace App\Repositories;

class CategoryRepository
{
    /**
     * 获取分类
     * @param $request
     * @return mixed
     * @author ppjun378 <ppjun378@foxmail.com>
     * @time: 2018/10/24 11:30
     */
    public static function getlist($request)
    {
        $field = ['id', 'name'];
        $data = Category::select($field)
            ->where('status', 1)
            ->orderBy('sort', 'desc')
            ->orderBy('id', 'desc')
            ->get();
        return $data;
    }

    /**
     * 分类列表（条件分页）
     * @param $request
     * @return mixed
     * @author ppjun378 <ppjun378@ppjun378.com>
     * @time 2018/10/24 11:35
     */
    public static function dateList($request)
    {
        $size = $request->input('limit', 10);
        $keyword = $request->input('keyword', '');
        $fields = ['id', 'name', 'description', 'sort', 'status', 'created_at'];
        $data = Category::select($fields)
            ->where(function ($query) use ($keyword) {
                $query->orWhere('name', 'like', '%' . $keyword . '%');
            })
            ->orderBy('status', 'asc')
            ->orderBy('sort', 'desc')
            ->orderBy('id', 'desc')
            ->paginate($size);
        $data = $data->toArray();
        $list['data'] = $data['data'];
        $list['count'] = $data['total'];
        $list['code'] = 0;
        $list['msg'] = '';
        return $list;
    }

    /**
     * 分类名称是否存在
     * @param $name
     * @return bool
     * @author ppjun378 <ppjun378@ppjun378.com>
     * @time: 2018/10/24 11:45
     */
    public static function checkName($name)
    {
        $data = Category::where('name', $name)->first();
        if ($data) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * 删除分类
     * @param $data
     * @return mixed
     * @author ppjun378 <ppjun378@foxmail.com>
     * @time: 2018/10/24 11:47
     */
    public static function destroy($id)
    {
        return Category::destroy($id);
    }

    /**
     * 分类修改保存
     * @param $id
     * @param $data
     * @return mixed
     * @author ppjun378 <ppjun378@foxmail.com>
     * @time: 2018/10/24 11:50
     */
    public static function save($id, $data)
    {
        return Category::where('id', $id)->update($data);
    }

    /**
     * 获取单个数据
     * @param $id
     * @return mixed
     * @author ppjun378 <ppjun378@foxmail.com>
     * @time: 2018/10/24 11:53
     */
    public static function getFirst($id)
    {
        return Category::where('id', $id)->orderBy('id', 'desc')->first();
    }

}
