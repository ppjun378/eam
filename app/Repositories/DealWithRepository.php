<?php
/**
 * User: ppjun378
 * Data: 2018/11/11
 * Time: 14:00
 */

namespace App\Repositories;

use App\Model\DealWiths;

class DealWithRepository
{
    /**
     * 列表（条件分页）
     * @param $request
     * @return mixed
     * @author ppjun378 <ppjun378@foxmail.com>
     */
    public function detaList($request)
    {
        $size = $request->input('limit', 10);
        $fields = ['id'];
        $data = DealWiths::select($fields)
            ->orderBy('id', 'desc')
            ->paginate($size);
        $data = $data->toArray();
        $list['data'] = $data['data'];
        $list['count'] = $data['total'];
        $list['code'] = 0;
        $list['msg'] = '';
        return $list;
    }

    /**
     * 删除
     * @param $data
     * @return mixed
     * @author ppjun378 <ppjun378@foxmail.com>
     * @time: 2018/10/25
     */
    public static function destory($id)
    {
        return DealWiths::destory($id);
    }

    /**
     * 获取单个数据
     * @param $id
     * @return mixed
     * @author ppjun378 <ppjun378@foxmail.com>
     * @time: 2018/10/25
     */
    public static function getFirst($id)
    {
        return DealWiths::where('id', $id)->orderBy('id', 'desc')->first();
    }
}
