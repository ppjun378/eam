<?php
/**
 * User: ppjun378
 * Date: 2018/12/07
 * Time: 12:00
 */
namespace App\Api\Controllers;

use App\Http\Controllers\Controller;

class BaseController extends Controller
{
    /**
     * 成功返回json数据格式
     * @param $data
     * @param integer $code
     * @param string $message
     * @param int $status
     * @return \Illuminate\Http\JsonResponse
     * @author ppjun378 <ppjun378@foxmail.com>
     */
    public function data($data, $code = 0, $message = 'success', $status = 200)
    {
        return response()->json([
            'data' => $data,
            'code' => $code,
            'message' => $message,
        ], $status, [], JSON_UNESCAPED_UNICODE);
    }

    /**
     * 返回成功
     * @param integer $code
     * @param string $message
     * @param int $status
     * @return \Illuminate\Http\JsonResponse
     * @author ppjun378 <ppjun378@foxmail.com>
     */
    public function success($code = 0, $message = 'success', $status = 200)
    {
        return response()->json([
            'code' => $code,
            'message' => $message,
        ], $status, [], JSON_UNESCAPED_UNICODE);
    }

    /**
     * 返回失败
     * @param int $code
     * $param string $message
     * @param int $status
     * @return \Illuminate\Http\JsonResponse
     * @author ppjun378 <ppjun378@foxmail.com>
     */
    public function error()
    {
        return response()->json([
            'code' => $code,
            'message' => $message,
        ], $status, [], JSON_UNESCAPED_UNICODE);
    }

}
