<?php
/**
 * User: ppjun378
 * Data: 2018/11/13
 * Time: 18:00
 */

namespace App\Api\Controllers;

use App\Service\PersonnelsService;
use Illuminate\Http\Request;

class PersonnelsController extends BaseController
{
    public function index()
    {
        echo 1;
    }

    /**
     * 列表页面
     * @return \Illuminate\Contracts\Validation\Validator|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @author ppjun378 <ppjun378@foxmail.com>
     */
    public function show(Request $request)
    {
        echo 1;

    }

    /**
     * 添加
     * @param Request $request
     * @return  mixed
     * @author  ppjun378 <ppjun378@foxmail.com>
     */
    public function add(Request $request)
    {

    }

    /**
     * 编辑页面
     * @param Request $request
     * @param $id
     * @return \Illuminate\Contracts\Validation\Validator|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @author <ppjun378@foxmail.com>
     */
    public function edit(Request $request)
    {

        $data = PersonnelsService::getDetail($request);
        return $data;
    }

    /**
     * 获取数据
     * @param Request $request
     * @return mixed
     * @author ppjun378 <ppjun378@foxmail.com>
     */
    public function getinfo(Request $request)
    {
        $result = PersonnelsService::dataList($request);
        return $result;
    }

    public function save(Request $request)
    {
        $request = PersonnelsServce::save($request);
        if ($result) {
            return $this->success();
        } else {
            return $this->error();
        }
    }

    /**
     * 添加/更新
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @author ppjun378 <ppjun378@foxmail.com>
     */
    public function store(Request $request)
    {
        $result = PersonnelsService::store($request);
        if ($result) {
            return $this->success();
        } else {
            return $this->error();
        }
    }

    /**
     * 删除
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @author ppjun378 <ppjun378@foxmail.com>
     */
    public function destroy(Request $request)
    {
        $result = PersonnelsService::destory($request);
        if ($result) {
            return $this->success();
        } else {
            return $this->error();
        }

    }

    /**
     * 恢复删除
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @author <rehun@foxmail.com>
     */
    public function restore(Request $request)
    {
        $result = PersonnelsService::restore($request);
        if ($result) {
            return $this->success();
        } else {
            return $this->error();
        }

    }

}
