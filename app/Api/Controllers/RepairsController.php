<?php
/**
 * User: ppjun378
 * Data: 2018/11/21
 * Time: 14:30
 */
namespace App\Api\Controllers;

use App\Service\RepairsService;
use Illuminate\Http\Request;

class RepairsController extends BaseController
{
    public function index()
    {

    }

    /**
     * 列表页面
     * @return \Illuminate\Contracts\Validation\Validator|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @author ppjun378 <ppjun378@foxmail.com>
     */
    public function show()
    {

    }

    /**
     * 编辑页面
     * @param Request $request
     * @param $id
     * @return \Illuminate\Contracts\Validation\Validator|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @author <ppjun378@foxmail.com>
     */
    public function edit(Request $request)
    {
        $data = RepairsService::getDetail($request);
        return $data;
    }

    /**
     * 获取数据
     * @param Request $request
     * @return  mixed
     * @author  ppjun378 <ppjun378@foxmail.com>
     */
    public function getinfo(Request $request)
    {
        $result = RepairsService::DataList($request);
        return $result;
    }

    /**
     * 添加/更新
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @author ppjun378 <ppjun378@foxmail.com>
     */
    public function store(Request $request)
    {
        $result = RepairsService::store($request);
        if ($request) {
            return $this->success();
        } else {
            return $this->error();
        }
    }

    /**
     * 删除
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @author ppjun378 <ppjun378@foxmail.com>
     */
    public function destroy(Request $request)
    {
        $result = RepairsService::destory($request);
        if ($result) {
            return $this->success();
        } else {
            return $this->error();
        }
    }

    /**
     * 恢复删除
     * $param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @author ppjun378 <ppjun378@foxmail.com>
     */
    public function restore(Request $request)
    {
        $result = RepairsService::restore($request);
        if ($result) {
            return $this->success();
        } else {
            return $this->error();
        }
    }
}
