<?php
/**
 * User: ppjun378
 * Data: 2018/12/12D
 * Time: 15:00
 */

namespace App\Api\Controllers\Home;

use App\Service\AssetsService;
use Illuminate\Http\Request;

class AssetsController extends BaseController
{
    public function index()
    {

    }

    /**
     * 列表页面
     * @return \Illuminate\Contracts\Validation\Validator|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @author ppjun378 <ppjun378@foxmail.com>
     */
    public function show()
    {

    }

    /**
     * 添加资产
     * @param Request $request
     * @return  mixed
     * @author  ppjun378 <ppjun378@foxmail.com>
     */
    public function add(Request $request)
    {

    }

    /**
     * 编辑页面
     * @param Request $request
     * @param $id
     * @return \Illuminate\Contracts\Validation\Validator|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @author <ppjun378@foxmail.com>
     */
    public function edit(Request $request)
    {
        $data = AssetsService::getDetail($request);
        return $data;
    }

    /**
     * 获取数据
     * @param Request $request
     * @return mixed
     * @author ppjun378 <ppjun378@foxmail.com>
     */
    public function getinfo(Request $request)
    {
        $result = AssetsService::userDataList($request);
        return $result;
    }

    public function save(Request $request)
    {
        $request = AssetsServce::save($request);
        if ($result) {
            return $this->success();
        } else {
            return $this->error();
        }
    }

    /**
     * 添加/更新
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @author ppjun378 <ppjun378@foxmail.com>
     */
    public function store(Request $request)
    {
        $result = AssetsService::store($request);
        if ($result) {
            return $this->success();
        } else {
            return $this->error();
        }
    }

    /**
     * 用户申请接口 1.领用 2.退还 3.借用
     * @param $request
     * @return mixed
     * @author ppjun378 <ppjun378@foxmail.com>
     */
    public function select_use_assets(Request $request)
    {
        echo 1;
        $result = AssetsService::select_use_assets($request);
        exit;
        if ($result) {
            return $this->success();
        } else {
            return $this->error();
        }
    }

}
