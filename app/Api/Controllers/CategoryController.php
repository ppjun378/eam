<?php
/**
 * User: ppjun378
 * Data: 2018/11/14
 * Time: 16:00
 */

namespace App\Api\Controllers;

use App\Service\CategoryService;
use Illuminate\Http\Request;

class CategoryController extends BaseController
{
    public function index()
    {

    }

    /**
     * 列表页面
     * @return \Illuminate\Contracts\Validation\Validator|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @author ppjun378 <ppjun378@foxmail.com>
     */
    public function show()
    {

    }

    /**
     * 添加资产
     * @param Request $request
     * @return  mixed
     * @author  ppjun378 <ppjun378@foxmail.com>
     */
    public function add(Request $request)
    {

    }

    /**
     * 编辑页面
     * @param Request $request
     * @param $id
     * @return \Illuminate\Contracts\Validation\Validator|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @author <ppjun378@foxmail.com>
     */
    public function edit(Request $request)
    {
        $data = CategoryService::getDetail($request);
        return $data;
    }

    /**
     * 获取数据
     * @param Request $request
     * @return mixed
     * @author ppjun378 <ppjun378@foxmail.com>
     */
    public function getinfo(Request $request)
    {
        $result = CategoryService::dataList($request);
        return $result;
    }

    public function save(Request $request)
    {
        $request = CategoryServce::save($request);
        if ($result) {
            return $this->success();
        } else {
            return $this->error();
        }
    }

    /**
     * 添加/更新
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @author ppjun378 <ppjun378@foxmail.com>
     */
    public function store(Request $request)
    {
        $result = CategoryService::store($request);
        if ($result) {
            return $this->success();
        } else {
            return $this->error();
        }
    }

    /**
     * 删除
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @author ppjun378 <ppjun378@foxmail.com>
     */
    public function destroy(Request $request)
    {
        $result = CategoryService::destory($request);
        if ($result) {
            return $this->success();
        } else {
            return $this->error();
        }

    }

    /**
     * 恢复删除
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @author <rehun@foxmail.com>
     */
    public function restore(Request $request)
    {
        echo 1;
        $result = CategoryService::restore($request);
        if ($result) {
            return $this->success();
        } else {
            return $this->error();
        }

    }

}
