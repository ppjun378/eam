<?php
/**
 * User: ppjun378
 * Data: 2018/10/22
 * Time: 18:00
 */

namespace App\Api\Controllers;

use App\Service\DealWithsService;
use Illuminate\Http\Request;

class DealWithsController extends BaseController
{
    public function index()
    {

    }

    /**
     * 列表页面
     * @return \Illuminate\Contracts\Validation\Validator|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @author ppjun378 <ppjun378@foxmail.com>
     */
    public function show()
    {

    }

    /**
     * 获取数据
     * @param Request $request
     * @return  mixed
     * @author  ppjun378 <ppjun378@foxmail.com>
     */
    public function getinfo(Request $request)
    {
        $result = DealWithsService::DataList($request);
        return $result;
    }

    /**
     * 删除
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @author ppjun378 <ppjun378@foxmail.com>
     */
    public function destroy(Request $request)
    {
        $result = DealWithsService::destory($request);
        if ($result) {
            return $this->success();
        } else {
            return $this->error();
        }
    }

    /**
     * 恢复删除
     * $param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @author ppjun378 <ppjun378@foxmail.com>
     */
    public function restore(Request $request)
    {
        $result = DealWithsService::restore($request);
        if ($result) {
            return $this->success();
        } else {
            return $this->error();
        }
    }

}
