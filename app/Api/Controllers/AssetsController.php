<?php
/**
 * User: ppjun378
 * Data: 2018/10/22
 * Time: 18:00
 */

namespace App\Api\Controllers;

use App\Service\AssetsService;
use Excel;
use Illuminate\Http\Request;

class AssetsController extends BaseController
{
    public function index()
    {

    }

    /**
     * 列表页面
     * @return \Illuminate\Contracts\Validation\Validator|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @author ppjun378 <ppjun378@foxmail.com>
     */
    public function show()
    {

    }

    /**
     * 添加资产
     * @param Request $request
     * @return  mixed
     * @author  ppjun378 <ppjun378@foxmail.com>
     */
    public function add(Request $request)
    {

    }

    /**
     * 编辑页面
     * @param Request $request
     * @param $id
     * @return \Illuminate\Contracts\Validation\Validator|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @author <ppjun378@foxmail.com>
     */
    public function edit(Request $request)
    {
        $data = AssetsService::getDetail($request);
        return $data;
    }

    /**
     * 获取数据
     * @param Request $request
     * @return mixed
     * @author ppjun378 <ppjun378@foxmail.com>
     */
    public function getinfo(Request $request)
    {
        $result = AssetsService::dataList($request);
        return $result;
    }

    public function save(Request $request)
    {
        $request = AssetsServce::save($request);
        if ($result) {
            return $this->success();
        } else {
            return $this->error();
        }
    }

    /**
     * 添加/更新
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @author ppjun378 <ppjun378@foxmail.com>
     */
    public function store(Request $request)
    {
        $result = AssetsService::store($request);
        if ($result) {
            return $this->success();
        } else {
            return $this->error();
        }
    }

    /**
     * 删除
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @author ppjun378 <ppjun378@foxmail.com>
     */
    public function destroy(Request $request)
    {
        $result = ArticleService::destory($request);
        if ($result) {
            return $this->success();
        } else {
            return $this->error();
        }

    }

    /**
     * 恢复删除
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @author <rehun@foxmail.com>
     */
    public function restore(Request $request)
    {
        $result = ArticleService::restore($request);
        if ($result) {
            return $this->success();
        } else {
            return $this->error();
        }

    }

    /**
     * 上传图片
     */
    public function uploadImage(Request $request)
    {

        $result = AssetsService::upload_image($request);
        return $result;
    }

    /**
     *
     * Excel导入
     */
    public function import_excel(Request $request)
    {
        $result = AssetsService::import_date();
        if ($result) {
            return $this->success();
        } else {
            return $this->error();
        }

        // $content = file_get_contents($filePath);
        // $fileType = mb_detect_encoding($content, array('UTF-8', 'GBK', 'LATIN1', 'BIG5')); //获取当前文本编码格式
        // print_r($filePath);
        // exit;
        // Excel::load($filePath, function ($reader) {
        //     $rows = $reader->all();
        //     echo '<pre>';
        //     print_r($rows);
        //     echo '</pre>';

        // }); //以指定的编码格式打开文件
    }

}
