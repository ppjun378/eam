<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Personnels extends Model
{
    use SoftDeletes;

    protected $table = 'personnels';

    protected $fillable = [
        'id',
        'personnel_number',
        'personnel_name',
        'company_id',
        'department_id',
        'mobile_phone',
        'email',
        'status',
    ];
}
