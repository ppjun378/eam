<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AssetsAttrs extends Model
{
    use SoftDeletes;

    protected $table = 'assets_attrs';

    protected $fillable = [
        'attr_name',
        'status',
        'attr_on',
        'attr_type_id',
    ];
}
