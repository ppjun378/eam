<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Clears extends Model
{
    use SoftDeletes;

    protected $table = 'clears';

    protected $fillable = [
        'clear_number', // 报废单号
        'content', //清理说明
        'borrow_operator', //清理人
        'clear_date', //清理日期
        'select_assets', //报废资产
        'status', //报废状态
    ];
}
