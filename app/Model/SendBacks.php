<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SendBacks extends Model
{
    use SoftDeletes;

    protected $table = 'send_backs';

    protected $fillable = [
        'borrow_number', //退库单号
        'actual_revert_date', //实际退库日期
        'use_company_id', //退库后使用公司
        'address_type_id', //退库后区域
        'address', //退库后存放地点
        'borrow_operator', //制单人
        'comment', //退库备注
        'signature_images', //签字图片
        'status', //状态
        'select_assets', //退库资产
    ];
}
