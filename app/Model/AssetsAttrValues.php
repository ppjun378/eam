<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AssetsAttrValues extends Model
{
    use SoftDeletes;
    protected $table = 'assets_attr_values';

    protected $fillable = [
        'attr_id',
        'attr_value',
        'asset_id',
    ];
}
//我迟到的次数用完了，老板也不喜欢别人迟到，迟到是我个人问题，是我不守时，他说过迟到3次就自动离职，我的错，我离职吧。


