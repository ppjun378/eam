<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Assets extends Model
{
    use SoftDeletes;

    protected $table = 'assets';

    protected $fillable = [
        'id',
        'coding', //资产编号
        'category_id', //资产类别
        'name', //资产名称
        'specification', //规格
        'sn', //SN号
        'unit', //计量单位
        'money', //金额
        'use_the_company_id', //使用公司
        'department_id', //使用部门
        'purchase_date', //购入日期
        'user_name', //使用人
        'administrator', //管理员
        'company_id', //所属公司
        'area', //区域
        'location', //存放地点
        'use_period', //使用期限
        'supplier', //供应商
        'source', //来源
        'note', //备注
        'image', //照片
    ];
}
?>
