<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Repairs extends Model
{
    use SoftDeletes;

    protected $table = 'repairs';

    protected $fillable = [
        'repair_number', //维修单号
        'content', //维修内容
        'money', //维修话费
        'repair_man', //报修人
        'status', //维修状态
        'borrow_operator', //处理人
        'business_creation_date', //业务创建日期
        'business_update_date', //维修处理中日期
        'business_end_date', //已完成报修日期
        'commit', //备注信息
        'select_assets', //维修资产
    ];
}
