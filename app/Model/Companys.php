<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Companys extends Model
{
    use SoftDeletes;

    protected $table = 'companys';

    protected $fillable = [
        'id',
        'company_number', // 公司编号
        'company_name', // 公司名称
        'parent_id', // 父类id
    ];
}
