<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Receives extends Model
{
    use SoftDeletes;

    protected $table = 'receives';

    protected $fillable = [
        'borrow_number', // 领用单号
        'borrow_user', // 领用人
        'borrow_date', // 领用日期
        'use_company_id', // 领用后使用公司
        'use_department_id', // 领用后使用部门
        'address_type_id', // 领用后使用区域
        'address', // 领用后存放地点
        'expect_revert_date', // 预计退库日期
        'borrow_operator', // 制单人
        'comment', // 备注
        'select_assets', // 领用资产
        'status', // 状态
        'signature_images', // 签字图片
    ];
}
