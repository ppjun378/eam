<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Borrows extends Model
{
    use SoftDeletes;

    protected $table = 'borrows';

    protected $fillable = [
        'borrow_number',
        'borrow_user', // 借用人
        'borrow_date', //借用日期
        'actual_revert_date', //实际归还日期
        'expect_revert_date', //预计归还日期
        'borrow_operator', //制单人
        'comment', //备注
        'select_assets', //领用资产
        'status', //状态
        'signature_images', //签字图片
    ];
}
