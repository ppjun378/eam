<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DealWiths extends Model
{
    use SoftDeletes;

    protected $table = 'deal_withs';

    protected $fillable = [
        'asset_id',
        'deal_with_date',
        'deal_with_man_id',
        'deal_with-content',
        'deal_with_status',
    ];

}
