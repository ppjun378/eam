<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Route;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to your controller routes.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        //

        parent::boot();
    }

    /**
     * Define the routes for the application.
     *
     * @return void
     */
    public function map()
    {
        // $this->mapApiRoutes();

        $this->mapWebRoutes();

        $this->mapEamRoutes();

        $this->mapAdminRoutes();
    }

    /**
     * Define the "web" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapWebRoutes()
    {
        Route::middleware('web')
            ->namespace($this->namespace)
            ->group(base_path('routes/web.php'));
    }

    /**
     * Define the "api" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapApiRoutes()
    {
        Route::prefix('api')
            ->middleware('api')
            ->namespace($this->namespace)
            ->group(base_path('routes/api.php'));
    }

    /**
     * eam路由文件
     * @author ppjun378 <ppjun378@foxmail.com>
     */
    protected function mapEamRoutes()
    {
        Route::middleware('api')
            ->namespace('App\Api\Controllers')
            ->group(base_path('routes/eam.php'));
    }

    /**
     * 后台路由文件
     * @author ppjun378 <ppjun378@foxmail.com>
     * @Time: 2019/03/27 11:15 AM
     */
    protected function mapAdminRoutes()
    {
        Route::middleware('admin')
            ->namespace('App\Admin\Controllers')
            ->group(base_path('routes/admin.php'));
    }

}
