layui.define(['layer', 'flow'], function (exports) {
    var $ = layui.jquery,
        layer = layui.layer,
        flow = layui.flow;

    var keyword = '';
    var tag_id = 0;
    var tag_name = '';

    // 文章列表 - 信息流
    function list() {
        $('#article-box').empty();
        flow.load({
            elem: '#article-box',
            isAuto: true,
            end: '没有更多了',
            mb: 200,
            done: function (page, next) {
                var pages; // 总页数
                var lis = [];
                $.ajax({
                    type: 'get',
                    url: '/api/article/list',
                    contentType: 'application/json',
                    data: {page: page, size: 10, keyword: keyword,tagId:tag_id},
                    dataType: 'json',
                    success: function (res) {
                        if (res.code === 0) {
                            var info = res.data.data;
                            if(Array.isArray(info) && info.length <= 0) {
                                var searchWord = '';
                                if(keyword.length > 0) {
                                    searchWord += keyword;
                                }
                                if (tag_id > 0) {
                                    if(keyword.length > 0) { searchWord += ' <em style="color: #000;">&&</em> '; }
                                    searchWord += tag_name;
                                }
                                if(searchWord !== '') {
                                    var html = '未搜索到与【<span style="color: #FF5722;">'+searchWord+'</span>】有关的文章，<span style="color: #3AAAFF; cursor: pointer;" onclick="layui.articleList.showList()">随便看看</span>吧！';
                                    $('#kw-msg').css('display','block').html(html);
                                }
                            } else {
                                $('#kw-msg').css('display','none').html('');
                            }
                            setTimeout(function () {
                                $.each(info, function (index, item) {
                                    var tags = '';
                                    $.each(item.tag_bind, function (index1, item1) {
                                        tags += '<a href="' + item1.to_tag.id + '">' + item1.to_tag.name + '</a>';
                                    });
                                    var str = '<div  class="article shadow">' +
                                        '<div class="article-left"> ' +
                                        '<img src="' + item.cover + '" alt="' + item.title + '" /> ' +
                                        '</div> ' +
                                        '<div class="article-right"> ' +
                                        '<div class="article-title"> ' +
                                        '<a href="detail/'+item.id+'">' + item.title + '</a> ' +
                                        '</div> ' +
                                        '<div class="article-abstract"> ' + item.description + ' ' +
                                        '</div></div> ' +
                                        '<div class="clear"></div> ' +
                                        '<div class="article-footer"> ' +
                                        '<span><i class="fa fa-clock-o"></i>&nbsp;&nbsp;' + item.created_at + '</span> ' +
                                        '<span class="article-author"><i class="fa fa-user"></i>&nbsp;&nbsp;' + item.author + '</span> ' +
                                        '<span><i class="fa fa-tag"></i>&nbsp;&nbsp; ' + tags + '&nbsp;&nbsp; </span> ' +
                                        '<span class="article-viewinfo"><i class="fa fa-eye"></i>&nbsp;' + item.click + '</span> ' +
                                        '<span class="article-viewinfo"><i class="fa fa-commenting"></i>&nbsp;'+ item.comments.length +'</span> ' +
                                        '</div></div>';
                                    lis.push(str);
                                });
                                pages = res.data.last_page;
                                next(lis.join(''), page < pages);
                            }, 500);
                        } else {
                            layer.msg('获取数据失败', {icon: 2});
                        }
                    },
                    error: function (e) {
                        layer.msg(e.responseText);
                    }
                });
            }
        });
    }

    list();

    var articleList = {
        // 搜索关键词
        searchKeyword: function () {
            keyword = $('input[name=keyword]').val();
            if(keyword.length>18) {
                layer.msg('输入的关键词太长');
                return false;
            }
            list();
        },
        // 搜索标签
        searchTag: function(obj,$tid) {
            if($(obj).hasClass('show-select-btn')) {
                tag_id = 0;
                tag_name = '';
                $(obj).removeClass('show-select-btn');
            } else {
                tag_id = $tid;
                tag_name = $(obj).text();
                $(obj).siblings().removeClass('show-select-btn');
                $(obj).addClass('show-select-btn');
            }
            list();
        },
        // 清除搜索条件
        showList: function () {
            keyword = '';
            tag_name = '';
            tag_id = 0;
            list();
        }
    };

    exports('articleList', articleList);
});