prettyPrint();
layui.use(['form','layedit'], function () {
    var $ = layui.jquery,
        form = layui.form,
        layedit = layui.layedit;

    // 评论的编辑器
    var editIndex = layedit.build('remarkEditor',{
        height:150,
        tool: ['face','|','left','center','right','|','link']
    });
    // 评论和留言的编辑器的验证
    form.verify({
        content: function (value) {
            value = $.trim(layedit.getText(editIndex));
            if(value === "") {
                layer.msg("自少得有一个字吧！");
                return false;
            }
            layedit.sync(editIndex);
        }
    });

    // 提交评论
    form.on('submit(formRemark)',function (data) {
        var index = layer.load(1);
        var aid = data.field.aid;
        var content = data.field.editorContent;
        console.log(content);
        $('#remarkEditor').val('');
        $.ajax({
            type: "post",
            url: "/api/article/comment",
            data: {aid:aid,content:content},
            success:function (resp) {
                layer.close(index);
                if(resp.code === 0) {
                    var html = '<li><div class="comment-parent"><img src="' + resp.data.avatar + '"alt="' + resp.data.nickname + '"/><div class="info"><span class="username">' + resp.data.nickname + '</span><span class="time">' + resp.data.created_at + '</span></div><div class="content">' + content + '</div></div></li>';
                    $('.blog-comment').prepend(html);
                    layer.msg("评论成功",{icon:1});
                    form.render();
                } else {
                    layer.msg(resp.message || "评论失败，请稍后再试！",{icon:2});
                }
                return false;
            },
            error:function (resp) {
                layer.close(index);
                layer.msg("服务器繁忙，请稍后再试",{icon:2});
                return false;
            }
        });
        return false;
    });
});