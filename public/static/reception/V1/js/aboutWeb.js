layui.use(['element', 'jquery', 'form', 'layedit'], function () {
    var $ = layui.jquery,
        element = layui.element,
        form = layui.form,
        layedit = layui.layedit;

    //评论和留言的编辑器
    var editIndex = layedit.build('remarkEditor', {
        height: 150,
        tool: ['face', '|', 'left', 'center', 'right', '|', 'link'],
    });
    //评论和留言的编辑器的验证
    form.verify({
        content: function (value) {
            value = $.trim(layedit.getText(editIndex));
            if (value === "") {
                return "自少得有一个字吧";
            }
            layedit.sync(editIndex);
        }
    });

    //Hash地址的定位
    var layid = location.hash.replace(/^#tabIndex=/, '');
    if (layid === "") {
        element.tabChange('tabAbout', 1);
    }
    element.tabChange('tabAbout', layid);

    element.on('tab(tabAbout)', function (elem) {
        location.hash = 'tabIndex=' + $(this).attr('lay-id');
    });

    function getFriendLink() {
        $.ajax({
            type: "get",
            url: "/api/friendLink/list",
            success:function (resp) {
                if(resp.code === 0) {
                    var html = '';
                    $.each(resp.data, function (index, item) {
                        html += '<li>\n' +
                            '<a target="_blank" href="'+item.domain+'" title="'+item.title+'" class="friendlink-item">\n' +
                            '<p class="friendlink-item-pic"><img src="/'+item.logo+'" alt="'+item.title+'" /></p>\n' +
                            '<p class="friendlink-item-title">'+item.title+'</p>\n' +
                            '<p class="friendlink-item-domain">'+item.domain+'</p>\n' +
                            '</a>\n' +
                            '</li>';
                    });
                    $('.friendlink').html(html);
                }
            },
        });
    }
    getFriendLink();

    //监听留言提交
    form.on('submit(formLeaveMessage)', function (data) {
        var index = layer.load(1);
        var content = data.field.editorContent;
        $.ajax({
            type: 'post',
            url: '/api/guest/book',
            data:{content:content},
            success:function(res) {
                layer.close(index);
                if(res.code === 0) {
                    var html = '<li><div class="comment-parent"><img src="' + res.data.avatar + '"alt="' + res.data.nickname + '"/><div class="info"><span class="username">' + res.data.nickname + '</span></div><div class="content">' + res.data.content + '</div><p class="info info-footer"><span class="time">' + res.data.created_at + '</span><a class="btn-reply"href="javascript:;" onclick="btnReplyClick(this)">回复</a></p></div><!--回复表单默认隐藏--><div class="replycontainer layui-hide"><form class="layui-form"action=""><input type="hidden" name="pid" value="' + res.data.id + '"><div class="layui-form-item"><textarea name="replyContent"lay-verify="replyContent"placeholder="请输入回复内容"class="layui-textarea"style="min-height:80px;"></textarea></div><div class="layui-form-item"><button class="layui-btn layui-btn-mini"lay-submit="formReply"lay-filter="formReply">提交</button></div></form></div></li>';
                    $('.blog-comment').prepend(html);
                    layer.msg("留言成功", { icon: 1 });
                    form.render();
                } else {
                    layer.msg(res.message || '留言失败，请稍后再试！' , { icon: 2 });
                }
                return false;
            },
            error:function(e) {
                layer.close(index);
                layer.msg("服务器繁忙，请稍后再试", { icon: 2 });
            }
        });
        return false;
    });

    //监听留言回复提交
    form.on('submit(formReply)', function (data) {
        var index = layer.load(1);
        var content = data.field.replyContent;
        var pid = data.field.pid;
        $.ajax({
            type: 'post',
            url: '/api/guest/book',
            data: {content:content,pid:pid},
            success:function(res) {
                layer.close(index);
                if(res.code === 0) {
                    var html = '<div class="comment-child"><img src="' + res.data.avatar + '" alt="' + res.data.nickname + '"/><div class="info"><span class="username">' + res.data.nickname + '</span><span>' + res.data.content + '</span></div><p class="info"><span class="time">' + res.data.created_at + '</span></p></div>';
                    $(data.form).find('textarea').val('');
                    $(data.form).parent('.replycontainer').before(html).siblings('.comment-parent').children('p').children('a').click();
                    layer.msg("回复成功", { icon: 1 });
                } else {
                    layer.msg(res.message || '回复留言失败，请稍后再试！' , { icon: 2 });
                }
                return false;
            },
            error:function(e) {
                layer.close(index);
                layer.msg("服务器繁忙，请稍后再试", { icon: 2 });
                return false;
            }
        });
        return false;
    });
});

function btnReplyClick(elem) {
    var $ = layui.jquery;
    $(elem).parent('p').parent('.comment-parent').siblings('.replycontainer').toggleClass('layui-hide');
    if ($(elem).text() === '回复') {
        $(elem).text('收起');
    } else {
        $(elem).text('回复');
    }
}
systemTime();
function systemTime() {
    //获取系统时间。
    var dateTime = new Date();
    var year = dateTime.getFullYear();
    var month = dateTime.getMonth() + 1;
    var day = dateTime.getDate();
    var hh = dateTime.getHours();
    var mm = dateTime.getMinutes();
    var ss = dateTime.getSeconds();

    //分秒时间是一位数字，在数字前补0。
    mm = extra(mm);
    ss = extra(ss);

    //将时间显示到ID为time的位置，时间格式形如：19:18:02
    document.getElementById("time").innerHTML = year + "-" + month + "-" + day + " " + hh + ":" + mm + ":" + ss;
    //每隔1000ms执行方法systemTime()。
    setTimeout("systemTime()", 1000);
}

//补位函数。
function extra(x) {
    //如果传入数字小于10，数字前补一位0。
    if (x < 10) {
        return "0" + x;
    }
    else {
        return x;
    }
}