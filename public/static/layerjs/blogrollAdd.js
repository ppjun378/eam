layui.define(['jquery','layer'],function (exports) {
    var $ = layui.jquery,
        layer = layui.layer;

    var tailoringImg = $('#tailoringImg');
    var tailoringContent = $(".tailoring-content");
    var tailoringcontainer = $(".tailoring-container");

    // ***************** 图片裁剪的js ****************
    //弹出框水平垂直居中
    (window.onresize = function () {
        var win_height = $(window).height();
        var win_width = $(window).width();
        if (win_width <= 512){
            tailoringContent.css({
                "top": (win_height - tailoringContent.outerHeight())/2,
                "left": 0
            });
        }else{
            tailoringContent.css({
                "top": (win_height - tailoringContent.outerHeight())/2,
                "left": (win_width - tailoringContent.outerWidth())/2
            });
        }
    })();

    //cropper图片裁剪
    tailoringImg.cropper({
        aspectRatio: 300/300,//默认比例
        preview: '.previewImg',//预览视图
        guides: false,  //裁剪框的虚线(九宫格)
        autoCropArea: 1,  //0-1之间的数值，定义自动剪裁区域的大小，默认0.8
        dragCrop: true,  //是否允许移除当前的剪裁框，并通过拖动来新建一个剪裁框区域
        movable: true,  //是否允许移动剪裁框
        resizable: true,  //是否允许改变裁剪框的大小
        zoomable: false,  //是否允许缩放图片大小
        mouseWheelZoom: true,  //是否允许通过鼠标滚轮来缩放图片
        touchDragZoom: true,  //是否允许通过触摸移动来缩放图片
        rotatable: true,  //是否允许旋转图片
        crop: function(e) {
            // console.log(e);
            // 输出结果数据裁剪图像。
        }
    });

    // 截图换向判断
    var flagX = true;

    var blogrollAdd = {
        // 弹出图片裁剪框
        tailorContainer:function () {
            tailoringcontainer.css('display','block');
        },
        // 图像上传
        selectImg:function (file) {
            if (!file.files || !file.files[0]){
                return;
            }
            var reader = new FileReader();
            reader.onload = function (evt) {
                var replaceSrc = evt.target.result;
                //更换cropper的图片
                $('#tailoringImg').cropper('replace', replaceSrc,false);//默认false，适应高度，不失真
            }
            reader.readAsDataURL(file.files[0]);
        },
        // 旋转
        rotateImg:function () {
            tailoringImg.cropper("rotate", 90);
        },
        // 复位
        resetImg:function () {
            tailoringImg.cropper("reset");
        },
        // 换向
        cropperScaleX:function () {
            if(flagX){
                tailoringImg.cropper("scaleX", -1);
                flagX = false;
            }else{
                tailoringImg.cropper("scaleX", 1);
                flagX = true;
            }
            flagX !== flagX;
        },
        // 裁剪后的处理
        sureCut:function () {
            if (tailoringImg.attr("src") == null ){
                layer.msg('请先选择图片',2);
                return false;
            }else{
                var cas = tailoringImg.cropper('getCroppedCanvas');//获取被裁剪后的canvas
                var base64url = cas.toDataURL('image/png'); //转换为base64地址形式
                blogrollAdd.putb64(base64url);
                blogrollAdd.closeTailor();
            }
        },
        // 关闭裁剪框
        closeTailor:function () {
            tailoringcontainer.toggle();
        },
        // 上传裁剪的图片
        putb64:function (base64url) {
            $('#srcimgurl').attr('src',base64url);
            $('#logoPic').val(base64url);
        },
        // 关闭iframe层
        closeIframe:function () {
            var index = parent.layer.getFrameIndex(window.name); //获取窗口索引
            parent.layer.close(index);
        },
        // 提交友链
        submitBlogRoll:function() {
            var title = $('input[name=title]').val();
            var domain = $('input[name=domain]').val();
            // 验证url的正则
            var reUrl =  /^((ht|f)tps?):\/\/([\w-]+(\.[\w-]+)*\/?)+(\?([\w\-\.,@?^=%&:\/~\+#]*)+)?$/;
            if(reUrl.test(domain)===false) {
                layer.msg("地址必须为http或https开头的子目录。如http://www.dwlblog.cn");
                return false;
            }
            var logo= $('#logoPic').val();
            var load = layer.load(1);
            $.ajax({
                type: "post",
                url: "store",
                dataType: "json",
                data:{title:title,domain:domain,logo:logo},
                success:function (resp) {
                    layer.close(load);
                    if(resp.code > 0) {
                        layer.msg(resp.message || '操作失败');
                        return false;
                    } else {
                        layer.msg(resp.message || '操作成功');
                        setTimeout("layui.blogrollAdd.ParentReload()",1000);
                    }
                },
                error:function (resp) {
                    layer.close(load);
                    layer.msg(resp.message || '服务繁忙');
                    return false;
                }
            });
        },
        // 保存友链
        saveBlogRoll:function (id) {
            var title = $('input[name=title]').val();
            var domain = $('input[name=domain]').val();
            var logo= $('#logoPic').val();
            var load = layer.load(1);
            $.ajax({
                type: "post",
                url: "/admin/blogroll/save",
                dataType: "json",
                data:{id:id,title:title,domain:domain,logo:logo},
                success:function (resp) {
                    layer.close(load);
                    if(resp.code > 0) {
                        layer.msg(resp.message || '操作失败');
                        return false;
                    } else {
                        layer.msg(resp.message || '操作成功');
                        setTimeout("layui.blogrollAdd.ParentReload()",1000);

                    }
                },
                error:function (resp) {
                    layer.close(load);
                    layer.msg(resp.message || '服务繁忙');
                    return false;
                }
            });
        },
        // 父窗口处理
        ParentReload:function () {
            var index = parent.layer.getFrameIndex(window.name); //获取窗口索引
            parent.layui.blogrollList.reloadList();
            parent.layer.close(index);
        }
    };

    exports('blogrollAdd',blogrollAdd);
});
