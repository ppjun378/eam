layui.define(['laypage', 'layer', 'table'], function (exports) {
    var $ = layui.jquery,
        layer = layui.layer,
        table = layui.table;

    var tableIns = function () {
        table.render({
            elem: "#databox",
            method: "get",
            page: true,
            url: "getinfo",
            cellMinWidth: 80,
            cols: [[
                {field: "id", title: "ID", width: 50, sort: true, fixed: 'left'},
                {field: "name", title: "分类名称", width: 300},
                {field: "description", title: "描述"},
                {field: "sort", title: "排序", width: 100, sort: true, align: 'center'},
                {field: "status", title: "状态", width: 150, sort: true, align: 'center',
                    templet: function (d) {
                        if (d.status === 1) {
                            return '<button class="layui-btn layui-btn-xs layui-btn-radius" onclick="layui.categoryList.alterationData(this,'+d.id+')">启用</button>';

                        } else {
                            return '<button class="layui-btn layui-btn-xs layui-btn-radius layui-btn-danger" onclick="layui.categoryList.alterationData(this,'+d.id+')">停用</button>';
                        }
                    }
                },
                {field: "created_at", title: "创建时间", width: 200, sort: true, align: 'center'},
                {field: "", title: "操作", width: 200, align: 'center',
                    templet: function (d) {
                        return '<button class="layui-btn layui-btn-sm layui-btn-danger" onclick="layui.categoryList.deleteData(this,'+d.id+')"><i class="layui-icon">&#xe640;</i></button>';
                    }
                }
            ]],
            done: function (res) {
                //
            }
        });
    };

    tableIns();

    // 监听关键字/词 keyword
    $("#keyword").change(function () {
        keyword = $(this).val();
        table.reload('databox',{
            where: {
                keyword: keyword
            },
            page: {
                curr: 1 // 重新从第1页开始
            }
        });
    });

    // 添加文章分类请求
    function store(data, index) {
        $.ajax({
            type: 'post',
            url: 'store',
            dataType: 'json',
            data: {name: data.name, description: data.description},
            success: function (resp) {
                if (resp.code === 0) {
                    layer.msg("添加成功",{icon:6});
                } else {
                    layer.msg(resp.message || "操作失败",{icon:5});
                }
                tableIns();
                layer.close(index);
            },
            error: function (resp) {
                layer.msg('服务器繁忙,请稍后再试');
            }
        });
    }

    var categoryList = {
        addData: function () {
            var url = "add";
            layer.open({
                type: 2, // 0(信息框，默认);1(页面层);2(iframe层);3(加载层);4(tips层)
                skin: '',
                anim: 1, // 动画效果0-6,不需要-1
                title: '新增分类', // 标题
                fix: false, // 即鼠标滚动时，层是否固定在可视区域，如果不是，设置fixed:fasle即可
                shade: 0, // 阴影度
                maxmin: true,
                id: (new Date()).valueOf(), // 设定id,防止重复弹出
                btn: ['添加', '取消'],
                area: ['500px', '300px'], // 框口宽高
                content: url, //内容
                yes: function (index, layero) {
                    // 获取到子页面到内容
                    var body = layer.getChildFrame('body', index);
                    var cateName = body.find('input').attr('name', 'name').val();
                    var cateDescription = body.find('textarea').attr('name', 'description').val();
                    if (cateName.length < 1) {
                        layer.msg("分类名称必须");
                        return false;
                    }

                    var data = new Array();
                    data['name'] = cateName;
                    data['description'] = cateDescription;
                    store(data, index);
                }
            });
        },
        deleteData: function (index, id) {
            layer.confirm('是否确认删除？', {
                btn: ['确认','取消']
            }, function () {
                $.ajax({
                    type: "post",
                    url: "destroy",
                    dataType: "json",
                    data: {id: id},
                    success: function (resp) {
                        if (resp.code === 0) {
                            $(index).parents('tr').remove();
                            layer.msg('删除成功', {icon: 6});
                        } else {
                            layer.msg(resp.message || '操作失败', {icon: 5});
                        }
                    },
                    error: function (resp) {
                        layer.msg('服务繁忙');
                    }
                });
            });
            return false;
        },
        alterationData: function (index, id) {
            $.ajax({
                type: 'post',
                url: 'alteration',
                dataType: 'json',
                data: {id:id},
                success: function (resp) {
                    if (resp.code === 0) {
                        layer.msg("修改成功",{icon:6});
                        var status = $(index).hasClass('layui-btn-danger');
                        if(status) {
                            $(index).removeClass('layui-btn-danger').text('启用');
                        } else {
                            $(index).addClass('layui-btn-danger').text('停用');
                        }
                    } else {
                        layer.msg(resp.message || "操作失败",{icon:5});
                    }
                },
                error: function (resp) {
                    layer.msg('服务器繁忙');
                }
            });
        }
    };
    exports('categoryList', categoryList);
});