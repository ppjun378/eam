layui.define(['jquery','form','layer'], function (exports) {
    var $ = layui.jquery,
        layer = layui.layer,
        form = layui.form;

    // 验证
    form.verify({
        password:[ /^[\S]{6,12}$/,'密码必须6到12位，且不能出现空格']
    });

    // 提交数据
    form.on('submit(savepass)', function (data) {
        var datas = data.field;
        var index = layer.load(1);
        $.ajax({
            type: "post",
            url: "pass",
            dataType: "json",
            data:datas,
            success:function (resp) {
                layer.close(index);
                if(resp.code > 0) {
                    layer.msg(resp.message || '操作失败');
                } else {
                    // layer.msg(resp.message || '操作成功');
                    layer.open({
                        content:resp.message,
                        skin: 'meg',
                        time:1,
                        end:function () {
                            top.location.reload();
                        }
                    });
                    // return true;
                }
            },
            error:function (resp) {
                layer.close(index);
                layer.msg(resp.message || '服务繁忙');
            }
        });
        return false;
    });

    exports('userPass',{});
});