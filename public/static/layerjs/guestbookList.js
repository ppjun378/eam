layui.define(['layer', 'table'], function (exports) {
    var $ = layui.jquery,
        layer = layui.layer,
        table = layui.table;

    // 获取列表
    var tableIns = function () {
        table.render({
            elem: "#dataContent",
            method: "get",
            page: true,
            url: "list",
            cellMinWidth: 80,
            cols: [[
                {field: "created_at", title: "创建时间", width: 200, sort: true, align: 'center'},
                {
                    field: "", title: "留言人", width: 200, align: 'center',
                    templet: function (d) {
                        return d.oauth_user.nickname;
                    }
                },
                {field: "content", title: "内容"},
                {
                    field: "", title: "操作", width: 200, align: 'center',
                    templet: function (d) {
                        var html = '';
                        if (d.is_reply > 0) {
                            html += '<button title="查看回复" class="layui-btn layui-btn-sm layui-btn-normal" onclick="layui.guestbookList.dataReply(' + d.id + ')">' +
                                '<i class="layui-icon">&#xe611;</i>' +
                                '</button>';
                        }
                        if (d.deleted_at) {
                            html += '<button title="还原删除" class="layui-btn layui-btn-sm" onclick="layui.guestbookList.restoreData(this,'+d.id+')">' +
                                '<i class="layui-icon">&#xe669;</i>' +
                                '</button>';
                            html += '<button title="永久删除" class="layui-btn layui-btn-sm layui-btn-danger" onclick="layui.guestbookList.deleteData(this,'+d.id+')">' +
                                '<i class="layui-icon">&#x1006;</i>' +
                                '</button>';
                        } else {
                            html += '<button title="删除" class="layui-btn layui-btn-sm layui-btn-warm" onclick="layui.guestbookList.softDelete(this,' + d.id + ')">' +
                                '<i class="layui-icon">&#xe640;</i>' +
                                '</button>';
                        }
                        return html;
                    }
                },
            ]],
            done: function (resp) {
                //
            }
        });
    };
    tableIns();

    var guestbookList = {
        // 软删除
        softDelete: function (index, id) {
            layer.confirm('确定删除？', {
                btn: ['确定', '取消'] //按钮
            }, function () {
                var load = layer.load(1);
                $.ajax({
                    type: 'post',
                    url: "/admin/guestbook/destroy",
                    data: {id: id},
                    success: function (res) {
                        if (res.code === 0) {
                            var html = '';
                            html += '<button title="还原删除" class="layui-btn layui-btn-sm" onclick="layui.guestbookList.restoreData(this,'+id+')">' +
                                '<i class="layui-icon">&#xe669;</i>' +
                                '</button>';
                            html += '<button title="永久删除" class="layui-btn layui-btn-sm layui-btn-danger" onclick="layui.guestbookList.deleteData(this,'+id+')">' +
                                '<i class="layui-icon">&#x1006;</i>' +
                                '</button>';
                            $(index).parent().append(html);
                            $(index).remove();
                            layer.msg("操作成功", {icon: 1});
                        } else {
                            layer.msg(res.message || '操作失败',{icon: 2});
                        }
                        layer.close(load);
                    },
                    error: function (resp) {
                        layer.close(load);
                        layer.msg('服务器繁忙');
                    }
                });
            });
        },
        // 还原删除
        restoreData: function (index,id) {
            $.ajax({
                type: "post",
                url: "/admin/guestbook/restore",
                dataType: "json",
                data: {id: id},
                success: function (resp) {
                    if (resp.code === 0) {
                        var html = '';
                        html += '<button title="删除" class="layui-btn layui-btn-sm layui-btn-warm" onclick="layui.guestbookList.softDelete(this,' + id + ')">' +
                            '<i class="layui-icon">&#xe640;</i>' +
                            '</button>';
                        $(index).parent().html(html);
                        layer.msg('恢复成功', {icon: 1});
                    } else {
                        layer.msg(resp.message || '操作失败', {icon: 2});
                    }
                },
                error: function (resp) {
                    layer.msg('服务繁忙');
                }
            });
        },
        // 永久删除
        deleteData: function (index, id) {
            layer.confirm('此操作无法恢复，是否确认删除？', {
                btn: ['确定', '取消'] //按钮
            }, function () {
                var load = layer.load(1);
                $.ajax({
                    type: 'post',
                    url: "/admin/guestbook/delete",
                    data: {id: id},
                    success: function (res) {
                        if (res.code === 0) {
                            $(index).parents('tr').remove();
                            layer.msg("删除成功", {icon: 1});
                        } else {
                            layer.msg(res.message || '操作失败',{icon: 2});
                        }
                        layer.close(load);
                    },
                    error: function (resp) {
                        layer.close(load);
                        layer.msg('服务器繁忙');
                    }
                });
            });
        },
        // 查看回复
        dataReply: function (id) {
            var url = 'reply/' + id;
            layer.open({
                type: 2,
                area: ['50vw', '45vh'],
                offset: '20vh',
                title: '查看回复',
                closeBtn: 1,
                shade: 0.6,
                shadeClose: false,
                skin: 'layer-css',
                content: url
            });
        }
    };

    exports('guestbookList', guestbookList);
});