var layer;
layui.define(['layer'],function () {
    layer = layui.layer;
});
var tagcon = $("tagcon");
var addBox = $("addBox");
var addBtn = addBox.children[1];
var intxt = addBox.children[0];
var divs = tagcon.children;

function $(id){ return document.getElementById(id); }//$获取元素函数封装
function crele(ele){ return document.createElement(ele); } //创建元素
function adson(father,son1,son2,son3,clas1,clas2,clas3,clas4,con1,con2){
    father.appendChild(son1);
    father.appendChild(son2);
    father.appendChild(son3);
    father.className = clas1;
    son1.className = clas2;
    son2.className = clas3;
    son3.className = clas4;
    son1.innerHTML = con1;
    son3.innerHTML = con2;
}

// 显示标签
function addTag(tag) {
    var newdiv = crele("div");
    var newem = crele("em");
    var newspan = crele("span");
    var newa = crele("a");
    if(divs.length == 0){//最新添加的标签在最前边
        tagcon.appendChild(newdiv);
    }else{
        tagcon.insertBefore(newdiv,divs[0]);
    }
    adson(newdiv,newem,newspan,newa,"tag","tagtxt","move","closetag",tag,"×");
}

// 添加数据
function getTagList() {
    jQuery.ajax({
        url: "tag/names",
        type: "get",
        dataType: "json",
        success:function (res) {
            if(res.code == 0) {
                jQuery.each(res.data, function (index,value) {
                    addTag(value.name);
                });
            } else {
                layui.use('layer',function () {
                    var layer = layui.layer;
                    layer.msg(res.message||"暂无数据");
                });
            }
        },
        error:function () {
            layui.use('layer',function () {
                var layer = layui.layer;
                layer.msg("服务器繁忙");
            });
        }
    });
}

jQuery(document).ready(function () {
    getTagList();
});

//输入框聚焦和失焦的效果
intxt.onfocus = function(){
    intxt.style.backgroundColor = "#fff";
}
intxt.onblur = function () {
    intxt.style.backgroundColor = "#e3e3e3";
}

//点击add按钮添加标签
addBtn.onclick = function () {
    var name = intxt.value;
    if(name != ""){
        jQuery.ajax({
            url: "tag/add",
            type: "post",
            dataType: "json",
            data: {name:name},
            success:function (res) {
                if(res.code == 0) {
                    addTag(name);
                    intxt.value = "";
                } else {
                    layui.use('layer',function () {
                        var layer = layui.layer;
                        layer.msg(res.message||"请勿重复添加");
                    });
                }
            },
            error:function() {
                layui.use('layer',function () {
                    var layer = layui.layer;
                    layer.msg("服务器繁忙");
                });
            }
        });
    }else{
        layer.msg("你还没有输入呢！");
    }
}

// 删除标签
jQuery(".tagCon").on('click','.closetag',function () {
    var obj = this;
    var name = obj.parentNode.firstChild.innerText;
    jQuery.ajax({
        url: "tag/delete",
        type: "post",
        dataType: "json",
        data: {name:name},
        success:function (res) {
            if(res.code == 0) {
                obj.parentNode.style.display = "none";
            } else {
                layui.use('layer',function () {
                    var layer = layui.layer;
                    layer.msg(res.message||"服务器繁忙");
                });
            }
        },
        error:function () {
            layui.use('layer',function () {
                var layer = layui.layer;
                layer.msg("服务器繁忙");
            });
        }
    });
    // obj.parentNode.style.display = "none";
})
