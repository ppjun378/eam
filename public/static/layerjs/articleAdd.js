layui.define(['layer','form','upload'], function (exports) {
    var $ = layui.jquery,
        layer = layui.layer,
        form = layui.form,
        upload = layui.upload;

    var coverUrl = '';

    // 封面图片上传
    var uploadInst = upload.render({
        elem: '#uploadCover',
        url: '/admin/common/upload/cover',
        data: { type: 'cover'},
        before: function(obj){
            //预读本地文件示例，不支持ie8
            obj.preview(function(index, file, result){
                $('#pictrueUrl').attr('src', result); //图片链接（base64）
            });
        },
        done: function(res){
            //如果上传失败
            if(res.code !== 0){
                return layer.msg('上传失败');
            } else {
                //上传成功
                coverUrl = res.data;
                $('#pictrueUrl').attr('data-url',coverUrl); //图片链接（base64）
            }
        },
        error: function(){
            //演示失败状态，并实现重传
            var demoText = $('#uploadError');
            demoText.html('<span style="color: #FF5722;">上传失败</span> <a class="layui-btn layui-btn-xs demo-reload">重试</a>');
            demoText.find('.demo-reload').on('click', function(){
                uploadInst.upload();
            });
        }
    });

    // 新增、更新
    form.on('submit(saveArticle)', function(data){
        // console.log(data.field);
        // 获取编辑器区域完整html代码
        var html = editor.$txt.html();
        // 获取编辑器纯文本内容
        var text = editor.$txt.text();
        var datas = data.field;

        datas['content'] = html;
        datas['text'] = text;
        if(coverUrl) {
            datas['cover'] = coverUrl;
        }
        $.ajax({
            type: "post",
            url: "/admin/article/store",
            data: datas,
            dataType: "json",
            success:function(res){
                if(res.code > 0){
                    layer.msg(res.msg || '操作失败');
                    return false;
                }else{
                    layer.msg(res.msg || '操作成功');
                    window.location.href = "/admin/article/show";
                    return true;
                }
            },
            error:function (res) {
                layer.msg(res.msg || '服务繁忙');
            }
        });
        return false;
    });

    var articleAdd = {
        addTag:function (indexs) {
            layer.prompt({
                formType: 0, //输入框类型，支持0（文本）默认1（密码）2（多行文本）
                value: '', //初始时的值，默认空字符
                maxlength: 32, //可输入文本的最大长度，默认500
                title: '添加标签',
                // area: ['500px', '150px'] //自定义文本域宽高
            }, function(value, index, elem){
                $.ajax({
                    type: "post",
                    url: "tag/store",
                    dataType: "json",
                    data: {name:value},
                    success: function (resp) {
                        console.log(resp);
                        if(resp.code === 0) {
                            var html = ' <input type="checkbox" name="tag['+resp.data.id+']" title="'+resp.data.name+'" checked>';
                            $(indexs).before(html);
                            form.render();
                            layer.msg("添加成功",{icon:6});
                        } else {
                            layer.msg(resp.message || "操作失败",{icon:5});
                        }
                        layer.close(index);
                    },
                    error: function (resp) {
                        layer.msg('服务器繁忙,请稍后再试');
                    }
                });
            });
        }
    }

    exports('articleAdd',articleAdd);
});