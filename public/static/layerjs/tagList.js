layui.define(['layer','table'], function (exports) {
    var $ = layui.jquery,
        layer = layui.layer,
        table = layui.table;

    var tableIns = function () {
        table.render({
            elem: "#databox",
            method: "get",
            page: true,
            url: "list",
            cellMinWidth: 80,
            cols: [[
                {field: "id", title: "ID", width: 50, sort: true, fixed: 'left'},
                {field: "name", title: "标签名称"},
                {field: "sort", title: "排序", width: 100, sort: true, align: 'center'},
                {
                    field: "status", title: "状态", width: 150, sort: true, align: 'center',
                    templet: function (d) {
                        if (d.status === 1) {
                            return '<button class="layui-btn layui-btn-xs layui-btn-radius" onclick="layui.tagList.alterationData(this,'+d.id+')">启用</button>';

                        } else {
                            return '<button class="layui-btn layui-btn-xs layui-btn-radius layui-btn-danger" onclick="layui.tagList.alterationData(this,'+d.id+')">停用</button>';
                        }
                    }
                },
                {field: "created_at", title: "创建时间", width: 200, sort: true, align: 'center'},
                {
                    field: "", title: "操作", width: 200, align: 'center',
                    templet: function (d) {
                        return '<button class="layui-btn layui-btn-sm layui-btn-danger" onclick="layui.tagList.deleteData(this,'+d.id+')"><i class="layui-icon">&#xe640;</i></button>';
                    }
                }
            ]],
            done: function (resp) {
                // console.log(resp);
            }
        });
    };

    tableIns();

    // 监听关键字/词 keyword
    $("#keyword").change(function () {
        keyword = $(this).val();
        table.reload('databox', {
            where: {
                keyword: keyword
            },
            page: {
                curr: 1 // 重新从第1页开始
            }
        });
    });

    function addData() {
        //
    }

    var tagList = {
        addData:function () {
            layer.prompt({
                formType: 0, //输入框类型，支持0（文本）默认1（密码）2（多行文本）
                value: '', //初始时的值，默认空字符
                maxlength: 32, //可输入文本的最大长度，默认500
                title: '添加标签',
                // area: ['500px', '150px'] //自定义文本域宽高
            }, function(value, index, elem){
                $.ajax({
                    type: "post",
                    url: "store",
                    dataType: "json",
                    data: {name:value},
                    success: function (resp) {
                        if(resp.code === 0) {
                            layer.msg("添加成功",{icon:6});
                        } else {
                            layer.msg(resp.message || "操作失败",{icon:5});
                        }
                        tableIns();
                        layer.close(index);
                    },
                    error: function (resp) {
                        layer.msg('服务器繁忙,请稍后再试');
                    }
                });
            });
        },
        deleteData: function (index, id) {
            layer.confirm('是否确认删除？', {
                btn: ['确定','取消']
            }, function () {
                $.ajax({
                    type: "post",
                    url: "destroy",
                    dataType: "json",
                    data: {id: id},
                    success: function (resp) {
                        if(resp.code === 0) {
                            $(index).parents('tr').remove();
                            layer.msg('删除成功',{icon:6});
                        } else {
                            layer.msg(resp.message || '操作失败', {icon:5});
                        }
                    },
                    error: function (resp) {
                        layer.msg('服务繁忙');
                    }
                });
            });
            return false;
        },
        alterationData: function (index, id) {
            $.ajax({
                type: 'post',
                url: 'alteration',
                dataType: 'json',
                data: {id:id},
                success: function (resp) {
                    if (resp.code === 0) {
                        layer.msg("修改成功",{icon:6});
                        var status = $(index).hasClass('layui-btn-danger');
                        if(status) {
                            $(index).removeClass('layui-btn-danger').text('启用');
                        } else {
                            $(index).addClass('layui-btn-danger').text('停用');
                        }
                    } else {
                        layer.msg(resp.message || "操作失败",{icon:5});
                    }
                },
                error: function (resp) {
                    layer.msg('服务器繁忙');
                }
            });
        }
    };

    exports('tagList',tagList);
});