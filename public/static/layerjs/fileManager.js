layui.define(['laypage','layer','pagesize'],function(exports) {
    var $ = layui.jquery,
        layer = layui.layer,
        laypage = layui.laypage;

    var total = 0;
    var curr = 1;
    var limit = 20;
    var keyword = '';
    var ids = [];

    // 获取列表
    function getData() {
        $.ajax({
            url: 'list',
            type: 'get',
            dataType: 'json',
            data: {page:curr,size:limit,filetype:keyword,ids:ids},
            success:function (res) {
                total = res.total;
                var data = res.data;
                if(data.length != 0) {
                    var str = '';
                    $.each(data,function (index,value) {
                        str += '<div class="file-box"><div class="file">';
                        str += '<a target="_blank" href="'+value.preview+'" title="'+value.file_name+'" data-gallery="">';
                        if(pictureJudgment(value.file_type)) {
                            str += '<div class="image">';
                            str += '<img alt="image" class="img-responsive" src="'+value.preview+'">';
                            str += '</div>';
                        } else {
                            str += '<div class="icon">';
                            str += '<i class="fa '+iconClassName(value.file_type)+'"></i>';
                            str += '</div>';
                        }
                        str += '<div class="file-name">';
                        str += '<p class="file-title" style="line-height:1.2em;height:1.2em;overflow: hidden;text-overflow:ellipsis;white-space: nowrap;">'+value.file_name+'</p>';
                        str += '<small>添加时间：'+value.created_at+'</small>';
                        str += '</div></a></div></div>';
                    });
                    str += '<div id="blueimp-gallery" class="blueimp-gallery">\n' +
                        '<div class="slides"></div>\n' +
                        '<h3 class="title"></h3>\n' +
                        '<a class="prev"><</a>\n' +
                        '<a class="next">></a>\n' +
                        '<a class="close">×</a>\n' +
                        '<a class="play-pause"></a>\n' +
                        '<ol class="indicator"></ol>\n' +
                        '</div>';
                    $("#file-box").html(str);
                    filePulse();
                    pageShow();
                } else {
                    var str = '<div style="text-align: center; padding: 30px; font-size: 2em; background: #ffffff;">暂无文件数据，请先上传文件</div>';
                    $("#file-box").html(str);
                }
            },
            error:function (res) {
                layer.msg("服务器繁忙，请稍后再试");
            }
        });
    }

    // 分页功能
    function pageShow() {
        laypage.render({
            elem: 'filesPage'
            ,count: total
            ,curr: curr
            ,limit: limit
            ,limits: [10,20,30,40,50]
            ,layout: ['count', 'prev', 'page', 'next', 'limit', 'refresh', 'skip']
            ,jump: function(obj,first){
                total = obj.count;
                curr = obj.curr // 得到当前页，以便向服务端请求对应页的数据。
                limit = obj.limit // 得到每页显示的条数
                //首次不执行
                if(!first){
                    getData();
                }
            }
        })
    }

    /**
     * 判断是否图片
     * @param type
     * @returns {boolean}
     */
    function pictureJudgment(type) {
        switch (type) {
            case 'image/png':
            case 'image/jpg':
            case 'image/jpeg':
            case 'image/gif':
                bool = true;
                break;
            default:
                bool = false;
                break;
        }
        return bool;
    }

    function iconClassName(type) {
        switch (type) {
            case 'audio/mp3':
                iconClass = 'fa-music';
                break;
            case 'video/mpeg':
            case 'video/mp4':
                iconClass = 'fa-film';
                break;
            case 'application/vnd.ms-excel':
                iconClass = 'fa-bar-chart-o';
                break;
            case 'application/msword':
                iconClass = 'fa-file';
                break;
            default:
                iconClass = 'fa-file';
                break;
        }
        return iconClass;
    }

    function filePulse() {
        $(".file-box").each(function(){
            animationHover(this,"pulse")
        })
    }

    $(document).ready(function(){
        getData();
    });

    var fileManager = {
        selectFileType: function (str) {
            keyword = str;
            getData();
            keyword = '';
        },
        selectFileTag: function (str) {
            $.ajax({
                url: 'tag/list',
                type: 'post',
                dataType: 'json',
                data: {name:str},
                success:function (res) {
                    if(res.code === 0) {
                        ids = res.data;
                        var newids = [];
                        $.each(ids,function (index,value) {
                            if(value > 0) {
                                newids.push(value);
                            }
                        })
                        if(newids.length) {
                            getData();
                            ids = [];
                        } else {
                            layer.msg("无此标签数据");
                        }
                    } else {
                        layer.msg("操作失败，请稍后再试");
                    }
                },
                error:function (res) {
                    layer.msg("服务器繁忙，请稍后再试");
                }
            });
        }
    };

    exports('fileManager',fileManager);
});
