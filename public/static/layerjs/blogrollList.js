layui.define(['layer','table'],function (exports) {
    var $ = layui.jquery,
        layer = layui.layer,
        table = layui.table;

    // 获取列表
    var tableIns = function () {
        table.render({
            elem: "#friendlinkIndex",
            method: "get",
            page: true, // 开启分页
            url: "list", // 数据接口
            cellMinWidth: 80,
            cols: [[
                {field: "sort", title: "排序", width: 100, sort: true, align: 'center'},
                {field: "logo", title: "LOGO", width: 100, align: 'center',
                    templet:function (d) {
                        var html = '';
                        if(d.logo) {
                            html = '<img src="/'+d.logo+'" alt="'+d.title+'" width="50" />';
                        }
                        return html;
                    }
                },
                {field: "title", title: "title", width: 300},
                {field: "domain", title: "链接"},
                {field: "status", title: "状态", width: 100, sort: true, align: 'center',
                    templet:function (d) {
                        var buttonStatus = "";
                        var displayStatus = 'blockStatus';
                        if (d.deleted_at) {
                            buttonStatus += '<button class="layui-btn layui-btn-xs layui-btn-radius layui-btn-disabled blockStatus">删除</button>';
                            displayStatus = 'noneStatus';
                        } else {
                            buttonStatus += '<button class="layui-btn layui-btn-xs layui-btn-radius layui-btn-disabled noneStatus">删除</button>';
                        }
                        if (d.status === 1) {
                            buttonStatus += '<button class="layui-btn layui-btn-xs layui-btn-radius '+displayStatus+'" onclick="layui.blogrollList.alterStatus(this,'+d.id+')">启用</button>';
                        } else {
                            buttonStatus += '<button class="layui-btn layui-btn-xs layui-btn-radius layui-btn-danger '+displayStatus+'" onclick="layui.blogrollList.alterStatus(this,'+d.id+')">停用</button>';
                        }
                        return buttonStatus;
                    }
                },
                {field: "created_at", title: "创建时间", width: 200, align: 'center'},
                {field: "", title: "操作", width: 200, align: 'center',
                    templet: function (d) {
                        var html = '<button title="编辑" class="layui-btn layui-btn-sm layui-btn-normal" onclick="layui.blogrollList.editDataPage('+d.id+')">' +
                            '<i class="layui-icon">&#xe642;</i>' +
                            '</button>';
                        if (d.deleted_at) {
                            html += '<button title="永久删除" class="layui-btn layui-btn-sm layui-btn-danger" onclick="layui.blogrollList.forceDelete(this,'+d.id+')">' +
                                '<i class="layui-icon">&#x1006;</i>' +
                                '</button>';
                            html += '<button title="还原删除" class="layui-btn layui-btn-sm" onclick="layui.blogrollList.restoreData(this,'+d.id+')">' +
                                '<i class="layui-icon">&#xe669;</i>' +
                                '</button>';
                        } else {
                            html += '<button title="删除" class="layui-btn layui-btn-sm layui-btn-warm" onclick="layui.blogrollList.softDelete(this,'+d.id+')">' +
                                '<i class="layui-icon">&#xe640;</i>' +
                                '</button>';
                        }
                        return html;
                    }
                }
            ]],
            done: function (resp) {
                //
            }
        });
    };
    tableIns();

    var blogrollList = {
        // 刷新列表
        reloadList:function() {
            tableIns();
        },
        // 新增页面
        addDataPage: function () {
            var url = "add";
            layer.open({
                type: 2,
                title: '添加友链',
                area: ['850px','700px'],
                fixed: false, // 不固定
                maxmin: true,
                content: url,
            });
        },
        // 编辑页面
        editDataPage: function (id) {
            var url = "edit/"+id;
            layer.open({
                type: 2,
                title: '编辑友链',
                area: ['850px','700px'],
                fixed: false, // 不固定
                maxmin: true,
                content: url,
            });
        },
        // 软删除
        softDelete: function (index,id) {
            layer.confirm('是否确认删除？', {
                btn: ['确认','取消']
            }, function () {
                $.ajax({
                    type: "post",
                    url: "destroy",
                    dataType: "json",
                    data: {id: id},
                    success: function (resp) {
                        if (resp.code === 0) {
                            var statusbtn1 = $(index).parents('td').prev().prev().find('.blockStatus');
                            var statusbtn2 = $(index).parents('td').prev().prev().find('.noneStatus');
                            statusbtn1.addClass('noneStatus').removeClass('blockStatus');
                            statusbtn2.addClass('blockStatus').removeClass('noneStatus');
                            var html = '<button title="永久删除" class="layui-btn layui-btn-sm layui-btn-danger" onclick="layui.blogrollList.forceDelete(this,'+id+')"><i class="layui-icon">&#x1006;</i></button><button title="还原删除" class="layui-btn layui-btn-sm" onclick="layui.blogrollList.restoreData(this,'+id+')"><i class="layui-icon">&#xe669;</i></button>';
                            $(index).parent().append(html);
                            $(index).remove();
                            layer.msg('删除成功', {icon: 6});
                        } else {
                            layer.msg(resp.message || '操作失败', {icon: 5});
                        }
                    },
                    error: function (resp) {
                        layer.msg('服务繁忙');
                    }
                });
            });
            return false;
        },
        // 还原删除
        restoreData: function (index,id) {
            $.ajax({
                type: "post",
                url: "restore",
                dataType: "json",
                data: {id: id},
                success: function (resp) {
                    if (resp.code === 0) {
                        var statusbtn1 = $(index).parents('td').prev().prev().find('.blockStatus');
                        var statusbtn2 = $(index).parents('td').prev().prev().find('.noneStatus');
                        statusbtn1.addClass('noneStatus').removeClass('blockStatus');
                        statusbtn2.addClass('blockStatus').removeClass('noneStatus');
                        var actHtml = '<button title="编辑" class="layui-btn layui-btn-sm layui-btn-normal" onclick="layui.blogrollList.editDataPage('+id+')"><i class="layui-icon">&#xe642;</i></button><button title="删除" class="layui-btn layui-btn-sm layui-btn-warm" onclick="layui.blogrollList.softDelete(this,'+id+')"><i class="layui-icon">&#xe640;</i></button>';
                        $(index).parent().html(actHtml);
                        layer.msg('恢复成功', {icon: 6});
                    } else {
                        layer.msg(resp.message || '操作失败', {icon: 5});
                    }
                },
                error: function (resp) {
                    layer.msg('服务繁忙');
                }
            });
        },
        // 彻底删除
        forceDelete: function (index,id) {
            layer.confirm('此操作无法恢复，是否确认删除？', {
                btn: ['确认','取消']
            }, function () {
                $.ajax({
                    type: "post",
                    url: "delete",
                    dataType: "json",
                    data: {id: id},
                    success: function (resp) {
                        if (resp.code === 0) {
                            $(index).parents('tr').remove();
                            layer.msg('删除成功', {icon: 6});
                        } else {
                            layer.msg(resp.message || '操作失败', {icon: 5});
                        }
                    },
                    error: function (resp) {
                        layer.msg('服务繁忙');
                    }
                });
            });
            return false;
        },
        // 更改状态
        alterStatus: function (index,id) {
            $.ajax({
                type: "post",
                url: "status",
                dataType: "json",
                data: {id: id},
                success: function (resp) {
                    if (resp.code === 0) {
                        var status = $(index).hasClass('layui-btn-danger');
                        if(status) {
                            $(index).removeClass('layui-btn-danger').text('启用');
                        } else {
                            $(index).addClass('layui-btn-danger').text('停用');
                        }
                        layer.msg('更改成功', {icon: 6});
                    } else {
                        layer.msg(resp.message || '操作失败', {icon: 5});
                    }
                },
                error: function (resp) {
                    layer.msg('服务繁忙');
                }
            });
        }
    };

    exports('blogrollList',blogrollList);
});