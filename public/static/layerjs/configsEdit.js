layui.define(['layer','form'],function (exports) {
    var $ = layui.jquery,
        layer = layui.layer,
        form = layui.form;

    // 保存配置项值
    form.on('submit(saveConfigs)', function (data) {
        var datas = data.field;
        $.ajax({
            type: "post",
            url: "save",
            data: datas,
            dataType: "json",
            success:function (resp) {
                if(resp.code > 0) {
                    layer.msg(resp.msg || '保存失败');
                } else {
                    layer.msg(resp.msg || '保存成功');
                }
                return true;
            },
            error:function (resp) {
                layer.msg(resp.msg || '服务繁忙');
            }
        });
        return false;
    });

    exports('configsEdit',{});
});