layui.define(['layer', 'table'], function (exports) {
    var $ = layui.jquery,
        layer = layui.layer,
        table = layui.table;

    var tableIns = function () {
        table.render({
            elem: "#databox",
            method: "get",
            page: true, // 开启分页
            url: "list", // 数据接口
            cellMinWidth: 80,
            cols: [[
                {field: "created_at", title: "添加时间", width: 200, sort: true, align: 'center'},
                {field: "content", title: "内容"},
                {field: "status", title: "状态", width: 150, sort: true, align: 'center',
                    templet: function (d) {
                        var buttonStatus = "";
                        var displayStatus = 'blockStatus';
                        if (d.deleted_at) {
                            buttonStatus += '<button class="layui-btn layui-btn-xs layui-btn-radius layui-btn-disabled blockStatus">删除</button>';
                            displayStatus = 'noneStatus';
                        } else {
                            buttonStatus += '<button class="layui-btn layui-btn-xs layui-btn-radius layui-btn-disabled noneStatus">删除</button>';
                        }
                        if (d.status === 1) {
                            buttonStatus += '<button class="layui-btn layui-btn-xs layui-btn-radius '+displayStatus+'" onclick="layui.timelineList.alterStatus(this,'+d.id+')">启用</button>';
                        } else {
                            buttonStatus += '<button class="layui-btn layui-btn-xs layui-btn-radius layui-btn-danger '+displayStatus+'" onclick="layui.timelineList.alterStatus(this,'+d.id+')">停用</button>';
                        }
                        return buttonStatus;
                    }
                },
                {field: "", title: "操作", width: 300, align: 'center',
                    templet: function (d) {
                        var html = '<button title="编辑" class="layui-btn layui-btn-sm layui-btn-normal" onclick="layui.timelineList.editDataPage('+d.id+')">' +
                            '<i class="layui-icon">&#xe642;</i>' +
                            '</button>';

                        if (d.deleted_at) {
                            html += '<button title="永久删除" class="layui-btn layui-btn-sm layui-btn-danger" onclick="layui.timelineList.forceDelete(this,'+d.id+')">' +
                                '<i class="layui-icon">&#x1006;</i>' +
                                '</button>';
                            html += '<button title="还原删除" class="layui-btn layui-btn-sm" onclick="layui.timelineList.restoreData(this,'+d.id+')">' +
                                '<i class="layui-icon">&#xe669;</i>' +
                                '</button>';
                        } else {
                            html += '<button title="删除" class="layui-btn layui-btn-sm layui-btn-warm" onclick="layui.timelineList.softDelete(this,'+d.id+')">' +
                                '<i class="layui-icon">&#xe640;</i>' +
                                '</button>';
                        }
                        return html;
                    }
                }
            ]],
            done: function (resp) {
                // console.log(resp);
            }
        });
    };
    tableIns();

    // 添加时光轴
    function store(index,data) {
        var load = layer.load(1);
        $.ajax({
            type: 'post',
            url: 'store',
            dataType: 'json',
            data: {content: data},
            success: function (resp) {
                layer.close(load); // 关闭加载层
                if (resp.code === 0) {
                    layer.msg("添加成功", {icon: 6});
                    tableIns();
                    layer.close(index)
                    return true;
                } else {
                    layer.msg(resp.message || "操作失败", {icon: 5});
                }
            },
            error: function (resp) {
                layer.close(load);
                layer.msg('服务器繁忙');
            }
        });
        return false;
    };

    // 更新时光轴
    function save(index,id,data) {
        var load = layer.load(1);
        $.ajax({
            type: 'post',
            url: 'save',
            dataType: 'json',
            data: {id:id,content: data},
            success: function (resp) {
                layer.close(load); // 关闭加载层
                if (resp.code === 0) {
                    layer.msg("修改成功", {icon: 6});
                    tableIns();
                    layer.close(index)
                    return true;
                } else {
                    layer.msg(resp.message || "操作失败", {icon: 5});
                }
            },
            error: function (resp) {
                layer.close(load);
                layer.msg('服务器繁忙');
            }
        });
        return false;
    }

    var timelineList = {
        // 新增页面
        addDataPage: function () {
            var url = "add";
            layer.open({
                type: 2,
                skin: '',
                anim: 1,
                title: '添加时光轴',
                fix: false,
                shade: 0,
                maxmin: true,
                id: (new Date()).valueOf(),
                area: ['680px', '500px'],
                content: url,
                btn: ['添加', '取消'],
                yes: function (index,layero) {
                    //获取iframe的body元素
                    var body = layer.getChildFrame('body',index);
                    //得到iframe页的窗口对象
                    var iframeWin = window[layero.find('iframe')[0]['name']];
                    var datas = iframeWin.getdata();
                    if (datas.length < 1) {
                        layer.msg("内容必须");
                        return false;
                    }
                    store(index,datas);
                }
            });
        },
        // 编辑页面
        editDataPage: function (id) {
            var url = "edit/"+id;
            layer.open({
                type: 2,
                skin: "",
                anim: 1,
                title: '编辑时光轴',
                fix: false,
                shade: 0,
                maxmin: true,
                id: (new Date()).valueOf(),
                area: ['680px','500px'],
                content: url,
                btn: ['保存','取消'],
                yes: function (index,layero) {
                    var iframeWin = window[layero.find('iframe')[0]['name']];
                    var datas = iframeWin.getdata();
                    if (datas.length < 1) {
                        layer.msg("内容必须");
                        return false;
                    }
                    save(index,id,datas);
                }
            });
        },
        // 软删除
        softDelete: function (index,id) {
            layer.confirm('是否确认删除？', {
                btn: ['确认','取消']
            }, function () {
                $.ajax({
                    type: "post",
                    url: "destroy",
                    dataType: "json",
                    data: {id: id},
                    success: function (resp) {
                        if (resp.code === 0) {
                            var statusbtn1 = $(index).parents('td').prev().find('.blockStatus');
                            var statusbtn2 = $(index).parents('td').prev().find('.noneStatus');
                            statusbtn1.addClass('noneStatus').removeClass('blockStatus');
                            statusbtn2.addClass('blockStatus').removeClass('noneStatus');
                            var html = '<button title="永久删除" class="layui-btn layui-btn-sm layui-btn-danger" onclick="layui.timelineList.forceDelete(this,'+id+')"><i class="layui-icon">&#x1006;</i></button><button title="还原删除" class="layui-btn layui-btn-sm" onclick="layui.timelineList.restoreData(this,'+id+')"><i class="layui-icon">&#xe669;</i></button>';
                            $(index).parent().append(html);
                            $(index).remove();
                            layer.msg('删除成功', {icon: 6});
                        } else {
                            layer.msg(resp.message || '操作失败', {icon: 5});
                        }
                    },
                    error: function (resp) {
                        layer.msg('服务繁忙');
                    }
                });
            });
            return false;
        },
        // 还原删除
        restoreData: function (index,id) {
            $.ajax({
                type: "post",
                url: "restore",
                dataType: "json",
                data: {id: id},
                success: function (resp) {
                    if (resp.code === 0) {
                        var statusbtn1 = $(index).parents('td').prev().find('.blockStatus');
                        var statusbtn2 = $(index).parents('td').prev().find('.noneStatus');
                        statusbtn1.addClass('noneStatus').removeClass('blockStatus');
                        statusbtn2.addClass('blockStatus').removeClass('noneStatus');
                        var actHtml = '<button title="编辑" class="layui-btn layui-btn-sm layui-btn-normal" onclick="layui.timelineList.editDataPage('+id+')"><i class="layui-icon">&#xe642;</i></button><button title="删除" class="layui-btn layui-btn-sm layui-btn-warm" onclick="layui.timelineList.softDelete(this,'+id+')"><i class="layui-icon">&#xe640;</i></button>';
                        $(index).parent().html(actHtml);
                        layer.msg('恢复成功', {icon: 6});
                    } else {
                        layer.msg(resp.message || '操作失败', {icon: 5});
                    }
                },
                error: function (resp) {
                    layer.msg('服务繁忙');
                }
            });
        },
        // 彻底删除
        forceDelete: function (index,id) {
            layer.confirm('此操作无法恢复，是否确认删除？', {
                btn: ['确认','取消']
            }, function () {
                $.ajax({
                    type: "post",
                    url: "delete",
                    dataType: "json",
                    data: {id: id},
                    success: function (resp) {
                        if (resp.code === 0) {
                            $(index).parents('tr').remove();
                            layer.msg('删除成功', {icon: 6});
                        } else {
                            layer.msg(resp.message || '操作失败', {icon: 5});
                        }
                    },
                    error: function (resp) {
                        layer.msg('服务繁忙');
                    }
                });
            });
            return false;
        },
        // 更改状态
        alterStatus: function (index,id) {
            $.ajax({
                type: "post",
                url: "alterstatus",
                dataType: "json",
                data: {id: id},
                success: function (resp) {
                    if (resp.code === 0) {
                        var status = $(index).hasClass('layui-btn-danger');
                        if(status) {
                            $(index).removeClass('layui-btn-danger').text('启用');
                        } else {
                            $(index).addClass('layui-btn-danger').text('停用');
                        }
                        layer.msg('更改成功', {icon: 6});
                    } else {
                        layer.msg(resp.message || '操作失败', {icon: 5});
                    }
                },
                error: function (resp) {
                    layer.msg('服务繁忙');
                }
            });
        }
    };

    exports('timelineList', timelineList);
});