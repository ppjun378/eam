$(function(){
    // 展示标签库按钮
    $(".show-labelitem1").on("click",function(){
        $(this).hide();
        $(".hide-labelitem1").show();
        $("#labelItem1").show();
    });
    // 隐藏标签库按钮
    $(".hide-labelitem1").on("click",function(){
        $(this).hide();
        $(".show-labelitem1").show();
        $("#labelItem1").hide();
    });
    // 选择标签（未选择）
    $(".label-item1").on("click","li",function(){
        var id = $(this).attr("data");
        var text = $(this).children("span").html();
        var labelHTML = "<li data='"+id+"''>x "+text+"</li>";
        if($(this).hasClass("selected1")){
            return false;
        }else if($(".label-selected1").children("li").length >= 5){
            layer.msg("最多可以选择5个哦");
            return false;
        }
        $(".label-selected1").append(labelHTML);
        val1 = '';
        for(var i = 0; i < $(".label-selected1").children("li").length; i++){
            val1 += $(".label-selected1").children("li").eq(i).attr("data")+',';
        }
        $("input[name='label1']").val(val1);
        $(this).addClass("selected1");
    });
    // 选择标签（已选择）
    var val1 = "";
    $(".label-selected1").on("click","li",function(){
        var id = $(this).attr("data");
        val1 = '';
        $(this).remove();
        for(var i = 0; i < $(".label-selected1").children("li").length; i++){
            val1 += $(".label-selected1").children("li").eq(i).attr("data")+',';
        }
        $("input[name='label1']").val(val1);
        $(".label-item1").find("li[data='"+id+"']").removeClass("selected1");
    });
/***************************第二个标签库*************************************/
    // 展示标签库按钮
    $(".show-labelitem2").on("click",function(){
        $(this).hide();
        $(".hide-labelitem2").show();
        $("#labelItem2").show();
    });
    // 隐藏标签库按钮
    $(".hide-labelitem2").on("click",function(){
        $(this).hide();
        $(".show-labelitem2").show();
        $("#labelItem2").hide();
    });
    // 选择标签（未选择）
    $(".label-item2").on("click","li",function(){
        var id = $(this).attr("data");
        var text = $(this).children("span").html();
        var labelHTML = "<li data='"+id+"''>x "+text+"</li>";
        if($(this).hasClass("selected2")){
            return false;
        }else if($(".label-selected2").children("li").length >= 5){
            layer.msg("最多可以选择5个哦");
            return false;
        }
        $(".label-selected2").append(labelHTML);
        val2 = '';
        for(var i = 0; i < $(".label-selected2").children("li").length; i++){
            val2 += $(".label-selected2").children("li").eq(i).attr("data")+',';
        }
        $("input[name='label2']").val(val2);
        $(this).addClass("selected2");
    });
    // 选择标签（已选择）
    var val2 = "";
    $(".label-selected2").on("click","li",function(){
        var id = $(this).attr("data");
        val2 = '';
        $(this).remove();
        for(var i = 0; i < $(".label-selected2").children("li").length; i++){
            val2 += $(".label-selected2").children("li").eq(i).attr("data")+',';
        }
        $("input[name='label2']").val(val2);
        $(".label-item2").find("li[data='"+id+"']").removeClass("selected2");
    });

})
